from stellar_base.keypair import Keypair
kp = Keypair.random()
from stellar_base.utils import StellarMnemonic

# ALICE

sm = StellarMnemonic("english") # here we use chinese, but default language is 'english'
m = sm.generate()
m = 'nose cute involve saddle excess sketch feature polar must aerobic poverty guilt'
kp = Keypair.deterministic(m, lang='english')
# publickey = kp.address().decode()
public_key = 'GD2HPONSMOTEJQUE2WIUBPMWZ7WPAE7EBLW2RRD2XH6CIJHKCZKAYMZ3'
# seed = kp.seed().decode() 
alice_seed = 'SCQUFZIFKMF3CYXAYKPKBQZRF33O3YKJMQXL63PVW7O66GBQOTVKR3A5'
# import requests
# publickey = kp.address().decode()
# r = requests.get('https://horizon-testnet.stellar.org/friendbot?addr=' + publickey)
# print(r.json())





from stellar_base.keypair import Keypair
from stellar_base.asset import Asset
from stellar_base.operation import Payment
from stellar_base.transaction import Transaction
from stellar_base.transaction_envelope import TransactionEnvelope as Te
from stellar_base.memo import TextMemo
from stellar_base.horizon import horizon_testnet, horizon_livenet

alice_seed = 'SCQUFZIFKMF3CYXAYKPKBQZRF33O3YKJMQXL63PVW7O66GBQOTVKR3A5'
bob_address = 'GCGUHPNOCTJVOC7YQUX6UG6L3S6PUPHNZJFEVSVSP56GJIC7OBCNXHUA'
# CNY_ISSUER = 'GDVDKQFP665JAO7A2LSHNLQIUNYNAAIGJ6FYJVMG4DT3YJQQJSRBLQDG'
amount = '100'

Alice = Keypair.from_seed(alice_seed)

horizon = horizon_testnet() # horizon = horizon_livenet() for LIVENET

# asset = Asset('CNY', CNY_ISSUER)
asset = Asset("XLM") 
# create op

# IMPORTANT

# MAKE SURE THE ACCOUNT (DESTINATION) EXISTS OR NOT. OTHERWISE but if the account does not exist, you will be charged
# the transaction fee when the transaction fails.. FOLLOW KIN SDK for steller

op = Payment({
    # 'source' : Alice.address().decode(),
    'destination': bob_address,
    'asset': asset,
    'amount': amount
})
# create a memo
msg = TextMemo('Buy yourself a beer !')

# get sequence of Alice
# Python 2
# sequence = horizon.account(Alice.address()).get('sequence')
# Python 3
sequence = horizon.account(Alice.address().decode('utf-8')).get('sequence')

# construct Tx
tx = Transaction(
    source = Alice.address().decode('utf-8'),
    opts = {
        'sequence': sequence,
        # 'timeBounds': [],
        'memo': msg,
        'fee': 100,
        'operations': [
            op,
        ],
    },
)
    
    
# build envelope
envelope = Te(tx=tx, opts={"network_id": "TESTNET"}) # envelope = Te(tx=tx, opts={"network_id": "PUBLIC"}) for LIVENET
# sign 
envelope.sign(Alice)
# submit
xdr = envelope.xdr()
print(xdr)
response = horizon.submit(xdr)
print(response)