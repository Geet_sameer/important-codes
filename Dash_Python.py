from cryptos import *
# priv : 89d8d898b95addf569b458fbbd25620e9c9b19c9f730d5d60102abbabcb72678 , pub : yc6xxkH4B1P4VRuviHUDBd3j4tAQpy4fzn # 40.677700 DASH
# priv : 8fb82cab05b1d02e3ea4f780d871ac0e593839347e4e2ec519acc3aa7c55e430 , pub : yXH2CqSgDtbZWR8qvPQyWbkthuUrWKsmjD
d = Dash(testnet=True)
priv = '89d8d898b95addf569b458fbbd25620e9c9b19c9f730d5d60102abbabcb72678'
my_addr = 'yc6xxkH4B1P4VRuviHUDBd3j4tAQpy4fzn'
destination = 'yXH2CqSgDtbZWR8qvPQyWbkthuUrWKsmjD'
inputs = d.unspent('yc6xxkH4B1P4VRuviHUDBd3j4tAQpy4fzn')
amount_to_send = 30262000
tx = d.preparesignedtx(privkey = priv, to = destination, value = amount_to_send , change_addr = my_addr, addr = my_addr)
transaction_id = d.pushtx(tx)
print(transaction_id)