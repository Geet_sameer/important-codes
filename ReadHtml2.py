from boilerpipe.extract import Extractor
import re
from itertools import islice,groupby,count
import itertools
import linecache
from boilerpipe.extract import Extractor
import re
# from bs4 import BeautifulSoup as soup  # HTML data structure
from urllib.request import urlopen as uReq  # Web client

#email = re.compile(r"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[(\.[a-z]{2,4})$")


def ExtractionProcess():
	with open("1.html","r",encoding = 'utf-8') as data_file:
		print(type(data_file))
		start = 0
		end = 0
		mylist = []
		start_list = []
		end_list = []
		html_lines = ''

		for index,line in enumerate(data_file):
			# print(type(line))
			if(line.startswith("<!DOCTYPE html")):
				start = index
				# print(start)
				start_list.append(start)
				# print(add)
			if(line.startswith("</html>")):
				end = index
				# print(end)
				end_list.append(end)
				# print(add)
		# print("start:",start_list)
		mylist = fileslice(start_list,end_list)
			
			
		
	# print(mylist)
	html_lines = ''.join(e for e in mylist)
	# print((html_lines))

	#Send html page to the extractor function, this function will return...
	#text data from the html pages using BOILER-PIPE
	data = extractor(html_lines)
	# print(data)
	#Sending the text data which got from Extractor() to phone_number_extraction method...
	#to extract the phone number from the text data
	phone_regex = Phone_Number_Extraction(data)
	# print(phone_regex)

	#Getting Email from the Email_Extraction Method
	Other_data = Email_Extraction(mylist)
	# print(Other_data)

	#Social Data from the html page
	SocialData = Social_Data_Extraction(mylist)
	# print(SocialData)

	return data

def fileslice(list1,list2):
	
	
	f = open("1.html","r",encoding = 'utf-8')
	lines = f.readlines()

	count = 0
	# print((lines))
	for a,b in zip(list1,list2):
		start = a
		end = b
		# print(start)
		# print(end)
		count += 1
		seclist = []
		
		for i in range(start,end):
			x = lines[i]
			seclist.append(x)

		
	return seclist

def Phone_Number_Extraction(htmlContent):
	phone_regex = re.findall(r'[\+]{1}\(?\d.*',htmlContent)
	return phone_regex

def Email_Extraction(htmlContent):
	data_lines = ''
	data_lines = ''.join(e for e in htmlContent)
	# email = re.findall(r'\w+@\w+\.\w+',data_lines)
	email = re.findall(r'^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[(\.[a-z]{2,4})$"',data_lines)
	print("email is" ,email)
	return email


def Social_Data_Extraction(data_list):
	data_lines = ''
	data_lines = ''.join(e for e in data_list)
	# print(data_lines)
	Extracted_Data = []
	# phone_regex = re.findall(r'[\+]{1}\(?\d.*',data_lines)
	# print("phone number", phone_regex)
	# Extracted_Data.append({'PhoneNumber' : phone_regex})
	# email = re.findall(r'\w+@\w+\.\w+',data_lines)
	# print(email)
	# Extracted_Data.append({'Email' : email})

	google_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?google.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_lines)
	# print(google_regex)
	Extracted_Data.append({'Google_account' : google_regex})

	twitter_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?twitter.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_lines)
	# print(twitter_regex)
	Extracted_Data.append({'Twitter_account' : twitter_regex})

	#Linkedin
	linkedin_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?linkedin.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_lines)
	# print(linkedin_regex)
	Extracted_Data.append({'Linkedin_account' : linkedin_regex})

	#FourSquare
	foursquare_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?foursquare.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_lines)
	# print(foursquare_regex)
	Extracted_Data.append({'Foursquare_account' : foursquare_regex})

	#Facebook
	Facebook_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?facebook.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_lines)
	# print(Facebook_regex)
	Extracted_Data.append({'FaceBook_account' : Facebook_regex})

	#Github
	github_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?github.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_lines)
	# print(github_regex)
	Extracted_Data.append({'GitHub_account' : github_regex})

	#Instagram
	Instagram_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?instagram.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_lines)
	# print(Instagram_regex)
	Extracted_Data.append({'Instagram_account' : Instagram_regex})

	#google_plus
	google_plus_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?plus.google.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_lines)
	# print(google_plus_regex)
	Extracted_Data.append({'GooglePlus_account' : google_plus_regex})
	# print("Extracted data" , Extracted_Data)
	return Extracted_Data

def extractor(html_lines):
	extractor1 = Extractor(extractor='KeepEverythingExtractor', html=html_lines)
	content = extractor1.getText()
	return content
	
if __name__ == "__main__":
	ExtractionProcess()

	

			