// Create and send a Testnet3 Bitcoin transaction:
//
// 1. Query for unspent outputs.
// 2. Create a transaction.
// 3. Forward the transaction to the Testnet3 Bitcoin network.
//
// Note:
// To explore the Testnet3 Bitcoin blockchain, see:
// https://live.blockcypher.com/btc-testnet/
//
// More information about the Blockcypher.com API, see: http://dev.blockcypher.com/
//
// More information:
// Example 3b
// https://www.mobilefish.com/developer/nodejs/nodejs_quickguide_bitcoinjs.html
//
// Disclaimer:
//
// Use this script at your own risk! All information on this page is provided "as is", without any warranty.
// Mobilefish.com will not be liable for any damages, loss of profits or any other kind of loss
// you may sustain by using this script.

var bitcoin = require('bitcoinjs-lib'); // Use version 2.2.0
var request = require('request');
var prompt = require('cli-prompt');

// Convert 'satoshi' to bitcoin value
var satoshiToBTC = function(value) {
	return value * 0.00000001;
};

// Broadcasts a transaction to the network via blockcypher.com
var broadcast_tx = function(tx) {

	console.log("tx in hex = ", tx.toHex());

	// var options = {
	// 	// uri: 'https://btgexplorer.com/api/tx/send'
	// 	uri: 'https://api.blockcypher.com/v1/btc/test3/txs/push',
	// 	method: 'POST',
	// 	json: {
	// 		"tx": tx.toHex()
	// 	}
	// };

	request(options, function(err, httpResponse, body) {
		if (err) {
			console.error('Request failed:', err);
		} else {
			console.log('Broadcast results:', body);
			console.log("Transaction send with hash:", tx.getId());
			// Give ajax call to the python backend with tx.getId()
		}
	});
}


// Fee to pay the miners in sathosis
var tx_fee = 10000; // 0.0001 BTC
// Transaction fee calculation  ---> size = (no.of.inputs * 148 + no.of.outputs * 34) + 10 
// Then call this api to get the various transaction fees per byte---> https://bitcoinfees.earn.com/api/v1/fees/recommended
//{ "fastestFee": 40, "halfHourFee": 20, "hourFee": 10 }
// fee = size * halfHourFee

// Prompt for the private key of the source address
prompt('Enter the private key of the source address (WIF format): ', function (private_wif) {
	// Get the source Testnet3 Bitcoin address from the private key
	var network = bitcoin.networks.testnet;
	var keyPair = bitcoin.ECPair.fromWIF(private_wif, network);
	var source_address = keyPair.getAddress()

	// Query blockcypher.com for the unspent outputs from the source address
	var url = "https://api.blockcypher.com/v1/btc/test3/addrs/"+source_address+"?unspentOnly=true";

	request(url, function (error, response, body) {
		if (!error && response.statusCode == 200) {

			// Parse the response and get the first unspent output
			var json = JSON.parse(body);
			// Get all the unspent transactions
			var data = [];
			var unspent = json["txrefs"];
			console.log(unspent)
			//Get all unspent transactions ids
			var total_unspent_value = 319340483
			var specific_amount = 6096389;
			// for (i = 0; i < unspent.length; i++){
			// 	if (unspent[i].value <= specific_amount || unspent[i].value >= specific_amount) {
			// 		data.push({'tx_hash':unspent[i].tx_hash,'tx_output_n':unspent[i].tx_output_n});
			// 		total_unspent_value = total_unspent_value + unspent[i].value
			// 		if (total_unspent_value > specific_amount){break;}
			// 	}
               
   //            }
   //           console.log(data)
   //           console.log(total_unspent_value)
             // data.forEach(input => console.log(input.tx_hash, input.tx_output_n))
			//----------------------------------------			Working
			// var tx_hash = ''
			// var tx_output_n = ''
			// var transaction_value = ''
			
			// for (var i = 0; i < unspent.length; i++){
			// 	if (unspent[i].value > specific_amount){
			// 		transaction_value = unspent[i].value;
			// 		tx_hash = unspent[i].tx_hash;
			// 		tx_output_n = unspent[i].tx_output_n;
			// 		break;
			// 	}
			// }
			// console.log(tx_hash)
			// console.log(tx_output_n)
			//-----------------------------------------
			// console.log("JSON unspent", unspent);
			var get_amount = total_unspent_value - specific_amount
			var withdraw_amount = specific_amount - tx_fee;
			console.log('withdraw', withdraw_amount)
			console.log('change',get_amount)

			// Prompt for the destination address
			console.log("Found an unspent transaction output with ", satoshiToBTC(specific_amount), " BTC.");

			prompt('Enter a destination address: ', function(dest_address) {
				// Calculate the withdraw amount minus the tx fee
				var get_amount = total_unspent_value - specific_amount
				var withdraw_amount = specific_amount - tx_fee;
				// var change = transaction_value - withdraw_amount
				console.log('withdraw', withdraw_amount)
				console.log('change',get_amount)
				console.log("Unspent value (BTC)= ", satoshiToBTC(specific_amount));
				console.log("Tx fee (BTC)= ", satoshiToBTC(tx_fee));
				console.log("Withdraw amount (BTC)= ", satoshiToBTC(withdraw_amount));

				// Build a transaction
				// console.log("TransactionBuilder input tx_hash = ", tx_hash);
				// console.log("TransactionBuilder input tx_output_n = ", tx_output_n);

				var txb = new bitcoin.TransactionBuilder(network);

				// console.log('data',data)
				// data.forEach(inputs => {
				// 	// console.log(inputs);
				// 	txb.addInput(inputs.tx_hash, inputs.tx_output_n)
				// })
				// txb.addInput(data);
				// txb.addInput('1e62edc0e75240ceeadbe21fe08923bd438adf4a4a5cc720a1befc5f6fab2151',1)
				// txb.addInput('a362e5c3e1b560ebcd8c95ac4205ce8bf4c9102634803d1e71436f95c64268cb',0)
				txb.addInput('4d1973c480a883a6f0e628ddd1a5a04e1ee3138921eb8f3042c970982f684f96',1)
				// txb.addInput('cc50bf54d8fa79995af7ecc9b614666696a92d166f51d572733eead914da871b',0)

				txb.addOutput(dest_address, withdraw_amount);
				txb.addOutput('mzCcYe6WzrEX1FpF2a18pqtCJU9zYE4xig',get_amount)

				txb.sign(0, keyPair);
				// txb.sign(1,keyPair);
				var tx = txb.build();

				console.log("tx = ", tx);

				// Prompt to confirm sending the transaction
				var confirm = "Send " + satoshiToBTC(withdraw_amount) + " plus miner fee? (y/N):";
				prompt(confirm, function(result) {
					if (result.toUpperCase() == "Y") {
						broadcast_tx(tx);
					};
				});

			});
		} else {
			console.log("Unable to find any unspent transaction outputs.");
			if (error) console.log("ERROR:", error);
		}
	});
});