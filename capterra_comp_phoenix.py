import tldextract
import uuid
import pandas as pd
import pymongo
import time
from pymongo.errors import DuplicateKeyError
import urllib.request
from datetime import datetime
from dateutil.parser import parse
import json
import ast
import yaml
import phoenixdb
import datetime
from database import *
import seolib as seo

print("Start : %s" % time.ctime())
# time.sleep(5)

dbStuff = PhoenixWrite()

'''
This is the first file to insert product,deployment and the product features
'''
with open('viewlocity_capt.json', encoding='utf-8') as data_file:
    data = json.load(data_file)

    if data.get('vendor_domain'):
        website = data.get('vendor_domain')
        ext = tldextract.extract(website)
        domain = '.'.join(ext[1:3])
    else:
        domain = ''
    # print(domain)

    if data.get('product_name'):
        product_name = data.get('product_name')
    else:
        product_name = ''

    if data.get('deployment'):
        deployment = data.get('deployment')
    else:
        deployment = ''

    if data.get('features'):
        features = data.get('features')
    else:
        features = ''
    # print(deployment)
    # print(features)

    # Get permalink from domain
    get_perma_query = "select reference_key from base_set where base_value like '%{val}%'".format(val=domain)
    perma_result = dbStuff.singleQuery(query=get_perma_query)
    if perma_result:
        permalink = perma_result.get('REFERENCE_KEY')

    if permalink:
        company_permalink = permalink
        query_dep = "select * from deployment where company_permalink = '{val}'".format(val=company_permalink)
        query_pro = "select * from product_features where company_permalink = '{val}'".format(val=company_permalink)
        result_dep = dbStuff.runQuery(query=query_dep, select=True)
        result_pro = dbStuff.runQuery(query=query_pro, select=True)
        if result_dep and result_pro:
            pass
        else:
            if deployment:
                for d in deployment:
                    deployment_key_uuid = str(uuid.uuid4())
                    deployment_dict = dict(deployment_key=deployment_key_uuid,company_permalink=permalink,deployment = d)
                    dbStuff.upsert_one(deployment_dict,tableName = 'deployment')
            else:
                pass
            if features:
                for f in features:
                    product_feature_name = f
                    features_list = data.get(f)
                    if features_list:
                        for fl in features_list:
                            features_list_key_uuid = str(uuid.uuid4())
                            features_list_dict = dict(features_key=features_list_key_uuid,company_permalink=permalink,product_name=product_name,product_feature_name=product_feature_name,
                                                        features = fl)
                            dbStuff.upsert_one(features_list_dict,tableName = 'product_features')
    else:
        pass



















