#
from __future__ import print_function
import time, requests, json
from cryptos import *
import pprint

class RPCHost(object):
    def __init__(self, url):
        self._session = requests.Session()
        self._url = url
        self._headers = {'content-type': 'application/json'}
    def call(self, rpcMethod, *params):
        payload = json.dumps({"method": rpcMethod, "params": list(params), "jsonrpc": "2.0"})
        tries = 5
        hadConnectionFailures = False
        while True:
            try:
                response = self._session.post(self._url, headers=self._headers, data=payload)
            except requests.exceptions.ConnectionError:
                tries -= 1
                if tries == 0:
                    raise Exception('Failed to connect for remote procedure call.')
                hadFailedConnections = True
                print("Couldn't connect for remote procedure call, will sleep for five seconds and then try again ({} more tries)".format(tries))
                time.sleep(10)
            else:
                if hadConnectionFailures:
                    print('Connected for remote procedure call after retry.')
                break
        if not response.status_code in (200, 500):
            raise Exception('RPC connection failure: ' + str(response.status_code) + ' ' + response.reason)
        responseJSON = response.json()
        if 'error' in responseJSON and responseJSON['error'] != None:
            raise Exception('Error in RPC call: ' + str(responseJSON['error']))
        return responseJSON['result']

# wallet_passphrase = 'fix hat proof cannon crew note typical deposit angle beef misery journey'
# priv_key = 'cRw1eUdTNFnZwAw76qsfZtbNT6JnVh6SX3rd59QNfZ8s4Q36tQeQ'
# address = 'mskVXyZoHyeCXsS7hiwZAKTH8GLDpb8nc8'
# destination = 'n2SybAfwGHDnHB3YGnkFPd54sscRW8bCsb'
segwit_private_key = 'cS2sqd9zxonT31qiDhPZDjcWw9mGMFnLkzPJv1amPZ7kDJJYxpUK'
segwit_address = '2Mtyc9sPELgp35bPWnuoc2K4372Vw3UW82z'

# Default port for the bitcoin testnet is 18332
# The port number depends on the one writtein the bitcoin.conf file
rpcPort = 8332

# The RPC username and RPC password MUST match the one in your bitcoin.conf file
rpcUser = 'bitcoin'
rpcPassword = 'local321'

#Accessing the RPC local server
# serverURL = 'http://' + rpcUser + ':' + rpcPassword + '@46.4.25.156:' + str(rpcPort)
serverURL = 'http://' + rpcUser + ':' + rpcPassword + '@167.99.90.68:' + str(rpcPort)
print(serverURL)

host = RPCHost(serverURL)

# walletpassphrase = host.call('walletpassphrase',wallet_passphrase,300)
# Generate address
# getinfo = host.call('getbestblockhash')
# print(getinfo)

# getnewaddress = host.call('getnewaddress')
# print(getnewaddress)
# dumpprivkey = host.call('dumpprivkey',getnewaddress)
# print(dumpprivkey)
# validDate = host.call('validateaddress',getnewaddress)
# print(validDate['pubkey'])

# unspent = host.call('listunspent',6,9999999,[segwit_address])
# unspent = host.call('listunspent')
# print(unspent)
# for i, o in enumerate(unspent):
#   unspent[i] = {
#                         "output": o['txid']+':'+str(o['vout']),
#                         "value": int(o['amount'] * 100000000)
#                     }
# print(unspent)

# dumpprivkey = host.call('dumpprivkey','munaRgLUs2ehpUorfrMyaneTQqNy4eUCQz')
# print(dumpprivkey)

# Get proper unspents
'''
amount_to_send = 100000
tx_fee = 50000

input_list = []
total_unspent_value = 0
for i in unspent:
	if (i.get('value') <= amount_to_send or i.get('value') >= amount_to_send):
		total_unspent_value += i.get('value')
		input_list.append(i)
		if (total_unspent_value > amount_to_send):
			break
print('unspent',total_unspent_value)
print('input_list', input_list)

c = Bitcoin(testnet=True)
tx = c.preparesignedtx(utran = input_list, privkey=priv_key,to=destination,value=amount_to_send,fee=tx_fee,change_addr=address,addr=address)
# print(tx)


sendrawtransaction = host.call('sendrawtransaction',tx)
pprint.pprint(sendrawtransaction)
'''


# Multisignartures
# btc_address = ['mheZwGoFxvCo8vcMwhHmx3cijZ1UMfYhkf','mmjhpVVVdPAyWWaeQcAhCuwnGczyxUrfHC','mgnYbxe92S7RWT2Y7i98SNvCMtAj85mxt8']
# publickeys = ['02909cb85626cb86674bba1318f6d642f8502519367e5ed6436a4901283993a898','03a073f1819b29ec01dd8cc967e1b5694b13a1a32bb1f01c2037cd8746333fb7dc','0316500ec3357ad51550ff8010dc42bc33a4280b5029a7aa91bcb0908a910d091b']
# privkey = ['cTUYXiRNXW6T1dCQUreotnDP7Hb68wpfD6R9J2SmxGqLVcRY6Wp1','cSo9dKXZwhFNL3ViXbCA8ZcVAzTxz2GoTUYUmz2uLTwQsRtASFrE','cSJF7Vmvxr5VmTjsSApvPP8mUg5sM7nAZbesKAzM5ySeThv8qp6K']

segwit_btc_address = ['2MtBXqJDvUcFR4xh3cXkGwM2ELiKD2FeX3F','2N1uWNr4dY4bzQY2sscNqq2juJ99S1itDpx','2NDxMDEfqDBdAxG5gvDcwQeS5FrXtVAjxvb']
segwit_publickeys = ['029a86df90632f8bd67b22467588d3c4e7729e5b97edf91e9c4986e26c0d66db12','03e084a9950a33279f1a342611eb75908f7b93303ca0cd381f7b81a9d9d4071fce','02378e73b4af38393ea203ec50f26115d643f2b37b57797b76b63e17f9b7fbfd1d']
segwit_priv_key = ['cV9UjrzjzisS3oxjTqvgLWJJ2TQNTjxoKAhqHpmCSRPDoLDn6iwN','cSHZtqDnVAvJU4Q89ivUsKUKBswgP6o3FKdpyiacAQZKVmWqntEW','cV96GwkiyTjnjvSF1EDoNkjAkyfdPQB5DWshsNUPmh3Eo8xEXDum']

from cryptos import *
coin = Bitcoin(testnet=True)
# publickeys = ['030583ff98646e16b954258beb28871082541692c5e0fac569f5ed59dbdd10118f','02b7fabb12437cb58add102c802a488d9f8a78e9b1c0b764ba2316913796e95d1a','02714e165dd79f319e344b89bae74188de6a8dec7b060c31975d3b01ac830bb246']
script, address = coin.mk_multsig_address(segwit_publickeys, 2)
# print(address)
# multisig_addr = host.call('addmultisigaddress',2,segwit_publickeys)
# print(multisig_addr)
# createmultisig = host.call('createmultisig',2,segwit_publickeys)
# print(createmultisig)

# print(address)
segwit_multisig_address = '2MyqZFYonim2fdEsZ36nquWhGqFwBCqmBFg'
# address = '2MvWaZXjWjb9pKyhLoGPfVLTeWLMYVrkCPZ'
unspent = host.call('listunspent',6,9999999,[segwit_multisig_address])
# unspent = host.call('listunspent')
# print(unspent)
for i, o in enumerate(unspent):
  unspent[i] = {
                        "output": o['txid']+':'+str(o['vout']),
                        "value": int(o['amount'] * 100000000)
                    }
# print(unspent)

amount_to_send = 100000
tx_fee = 50000
input_list = []
total_unspent_value = 0

for i in unspent:
	if (i.get('value') <= amount_to_send or i.get('value') >= amount_to_send):
		total_unspent_value += i.get('value')
		input_list.append(i)
		if (total_unspent_value > amount_to_send):
			break
print('unspent',total_unspent_value)
print('input_list', input_list)

# destination = 'n2SybAfwGHDnHB3YGnkFPd54sscRW8bCsb'
segwit_destination = '2Mtyc9sPELgp35bPWnuoc2K4372Vw3UW82z'
tx = coin.preparetx(input_list, address, segwit_destination, amount_to_send, tx_fee, change_addr = address)
# print(tx)
# walletpassphrase = host.call('walletpassphrase',wallet_passphrase,300)


for i in range(0, len(tx['ins'])):
	sig1 = coin.multisign(tx, i, script, "cV9UjrzjzisS3oxjTqvgLWJJ2TQNTjxoKAhqHpmCSRPDoLDn6iwN")
	sig2 = coin.multisign(tx, i, script, "cSHZtqDnVAvJU4Q89ivUsKUKBswgP6o3FKdpyiacAQZKVmWqntEW")
	# sig3 = coin.multisign(tx, i, script, "cSJF7Vmvxr5VmTjsSApvPP8mUg5sM7nAZbesKAzM5ySeThv8qp6K")
	tx = apply_multisignatures(tx, i, script, sig1, sig2)
# pprint.pprint(tx)

sendrawtransaction = host.call('sendrawtransaction',tx)
pprint.pprint(sendrawtransaction)

