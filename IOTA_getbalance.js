var request = require('request');

var command = {
    'command': 'getBalances',
    'addresses': ['ZGZFZIAOMUPXDYNXJEAGMUSZDMBJGAPKMFTVQGAZDRMT9NMTUFPHTLVGPUBXCIRD9BRXOBJUUVRMXNBTW'],
    'threshold': 100
}

var options = {
  url: 'http://eugene.iota.community:14265',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
		'X-IOTA-API-Version': '1',
    'Content-Length': Buffer.byteLength(JSON.stringify(command))
  },
  json: command
};

request(options, function (error, response, data) {
  if (!error && response.statusCode == 200) {
    console.log(data);
  }
});