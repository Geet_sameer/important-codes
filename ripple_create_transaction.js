// 'use strict';
// const RippleAPI = require('ripple-lib').RippleAPI;
//wss://s1.ripple.com:443   MAINNET
// const api = new RippleAPI({
//   server: 'wss://s.altnet.rippletest.net:51233' // testnet rippled server
// });
// api.connect().then(() => {
//   /* begin custom code ------------------------------------ */
//   api.getFee().then(fee => {console.log('fee', fee)});
//   const myAddress = 'rwV6W96h4DJ78KLcJYeze6uNq4qSiykt3h';

//   console.log('getting account info for', myAddress);
//   return api.getAccountInfo(myAddress);

// }).then(info => {
//   console.log(info);
//   console.log('getAccountInfo done');

//   /* end custom code -------------------------------------- */
// }).then(() => {
//   return api.disconnect();
// }).then(() => {
//   console.log('done and disconnected.');
// }).catch(console.error);

// var RippleWallet = require('ripple-wallet');
// console.log(RippleWallet.generate());
//rwV6W96h4DJ78KLcJYeze6uNq4qSiykt3h                 //rBoeec6SoM3HqsCYJq4FbUmiCMKKQ4xWZH
// snPxTEeSBWt5pujrJ2TvfCjLvgrFX
'use strict';
const RippleAPI = require('ripple-lib').RippleAPI;
const fetch = require('node-fetch')
var request = require('ajax-request');

const address = 'rwV6W96h4DJ78KLcJYeze6uNq4qSiykt3h';
const secret = 'snPxTEeSBWt5pujrJ2TvfCjLvgrFX';

const api = new RippleAPI({server: 'wss://s.altnet.rippletest.net:51233'});
const instructions = {maxLedgerVersionOffset: 5};

const payment = {
  source: {
    address: address,
    maxAmount: {
      value: '1',
      currency: 'XRP'
    }
  },
  destination: {
    address: 'rBoeec6SoM3HqsCYJq4FbUmiCMKKQ4xWZH',
    amount: {
      value: '1',
      currency: 'XRP'
    }
  }
};

function quit(message) {
  console.log(message);
  process.exit(0);
}

function fail(message) {
  console.error(message);
  process.exit(1);
}

api.connect().then(() => {
  console.log('Connected...');
  return api.preparePayment(address, payment, instructions).then(prepared => {
    console.log('Payment transaction prepared...');
    const {signedTransaction} = api.sign(prepared.txJSON, secret);
    var get_transaction_id = api.sign(prepared.txJSON, secret);
    
    console.log('Payment transaction signed...');
    console.log('Transaction id.....',get_transaction_id.id)
    api.submit(signedTransaction).then(quit, fail);

    var request = require("request");

    request({
      uri: "http://0.0.0.0:6543/checking",
      method: "POST",
      form: {
        transaction_id: get_transaction_id.id.toString()
      }
    }, function(error, response, body) {
      // console.log(response)
      // console.log(error);
    });
 
    
   
  });
}).catch(fail);