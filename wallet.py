# from database import *
from pyramid.view import view_config
import phoenixdb
from decimal import *
from hashlib import sha256
import blockcypher
from blockcypher import constants
from blockcypher import get_transaction_details
from blockcypher import get_address_full
# from moneywagon.tx import Transaction
import requests
from decimal import Decimal
import certifi
from cryptocurrency_wallet_generator import generate_wallet


digits58 = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'


# dbStuff = PhoenixWrite()

class Wallet:
    def __init__(self):
        # self.request = request
        database_url = 'http://devmike.trioncube.com:8765'
        self.conn = phoenixdb.connect(database_url, autocommit=True)
        self.cursor = self.conn.cursor(cursor_factory=phoenixdb.cursor.DictCursor)
        self.conn.close()

    def check_balance(self, asset, amount, user_id):
        query = "select wallet_current_balance from wallet_address where user_id = '{value}'".format(value=user_id)
        self.cursor.execute(query)
        main_balance = self.cursor.fetchone().get('WALLET_CURRENT_BALANCE')
        self.conn.close()
        print(main_balance)
        if amount < main_balance:
            return True
        else:
            return False

    def update(self):
        query = "upsert into wallet_address values ('dfasgadvaew','503','3154svvdsvdfsbfsdb','ETH','TRION','12-01-2017','1',6565.909,'yes','12-01-2017')"
        self.cursor.execute(query)
        self.conn.close()
        return

    def debit_wallet(self, user_id, asset, amount):
        query = "select wallet_current_balance,unique_id from wallet_address where user_id = '{value}'".format(
            value=user_id)
        self.cursor.execute(query)
        result = self.cursor.fetchone()

        main_balance = result.get('WALLET_CURRENT_BALANCE')
        unique_id = result.get('UNIQUE_ID')
        # self.conn.close()
        if amount < main_balance:
            main_balance = main_balance - amount
            # print(type(main_balance))
            update_query = "upsert into wallet_address (wallet_current_balance,user_id,unique_id) values ({main_balance},'{value}','{unique_id}')".format(
                main_balance=main_balance, value=user_id, unique_id=unique_id)
            print(update_query)
            self.cursor.execute(update_query)
            self.conn.close()
            return True
        return False

    def credit_wallet(self, user_id, asset, amount):
        query = "select wallet_current_balance,unique_id from wallet_address where user_id = '{value}'".format(
            value=user_id)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        main_balance = result.get('WALLET_CURRENT_BALANCE')
        unique_id = result.get('UNIQUE_ID')
        credit = main_balance + amount
        update_query = "upsert into wallet_address (wallet_current_balance,user_id,unique_id) values ({credit},'{value}','{unique_id}')".format(
            credit=credit, value=user_id, unique_id=unique_id)
        self.cursor.execute(update_query)
        self.conn.close()
        return True

    def decode_base58(self, bc, length):
        n = 0
        for char in bc:
            n = n * 58 + digits58.index(char)
        return n.to_bytes(length, 'big')

    def check_bc(self, bc):
        try:
            bcbytes = self.decode_base58(bc, 25)
            return bcbytes[-4:] == sha256(sha256(bcbytes[:-4]).digest()).digest()[:4]
        except Exception:
            return False

    def get_total_balance(self, bc):
        addr_details = get_address_full(address=bc)
        final_balance = addr_details.get('final_balance')
        balance = blockcypher.from_satoshis(final_balance, 'btc')
        print(balance)
        return

    def generate_eth_address(self):
        """
        This is the documentation for the generate_eth_address() function
        About:
            This function generates private,public and eth address
        Args:
            No Arguments
        :return:
            eth_addr_dict(dictionary) : dictionary with private,public and eth addresses
        """
        eth_addr_dict = {}
        from ecdsa import SigningKey, SECP256k1
        import sha3
        keccak = sha3.keccak_256()
        priv = SigningKey.generate(curve=SECP256k1)
        pub = priv.get_verifying_key().to_string()
        keccak.update(pub)
        address = keccak.hexdigest()[24:]
        eth_addr_dict['private_key'] = priv.to_string().hex()
        eth_addr_dict['publick_key'] = pub.hex()
        eth_addr_dict['eth_address'] = '0x' + address
        return eth_addr_dict

    def obtain_new_eth_address(self):
        """
       This is the documentation for obtain_new_eth_address() function
       About:
           This function generates new eth address for the wallet
       Args:
           No Arguments
       :return: (String) returns a new eth address

        """
        get_new_addr = self.generate_eth_address()
        new_addr = get_new_addr.get('eth_address')
        return new_addr

    def generate_new_address(self):

        """
        This is the documentation for the generate_new_address() function
        About:
            This function generates private,public and bitcoin address
        Args:
            No Arguments

        Returns:
            address(dictionary) : dictionary with private,public and bitcoin addresses
        """

        import bitcoin
        # Generate a random private key
        valid_private_key = False
        address = {}
        while not valid_private_key:
            private_key = bitcoin.random_key()
            decoded_private_key = bitcoin.decode_privkey(private_key, 'hex')
            valid_private_key = 0 < decoded_private_key < bitcoin.N
            # print( "Private Key (hex) is: ", private_key)             # USE THIS FOR PRIVATE KEY
            address['private_key'] = private_key
            # print ("Private Key (decimal) is: ", decoded_private_key)
            # Convert private key to WIF format
            wif_encoded_private_key = bitcoin.encode_privkey(decoded_private_key, 'wif')
            # print ("Private Key (WIF) is: ", wif_encoded_private_key)
            # Add suffix "01" to indicate a compressed private key
            compressed_private_key = private_key + '01'
            # print ("Private Key Compressed (hex) is: ", compressed_private_key)
            # Generate a WIF format from the compressed private key (WIF-compressed)
            wif_compressed_private_key = bitcoin.encode_privkey(
                bitcoin.decode_privkey(compressed_private_key, 'hex'), 'wif')
            # print( "Private Key (WIF-Compressed) is: ", wif_compressed_private_key)
            # Multiply the EC generator point G with the private key to get a public key point
            public_key = bitcoin.fast_multiply(bitcoin.G, decoded_private_key)
            # print ("Public Key (x,y) coordinates is:", public_key)
            # Encode as hex, prefix 04
            hex_encoded_public_key = bitcoin.encode_pubkey(public_key, 'hex')
            # print ("Public Key (hex) is:", hex_encoded_public_key)
            # Compress public key, adjust prefix depending on whether y is even or odd
            (public_key_x, public_key_y) = public_key
            if (public_key_y % 2) == 0:
                compressed_prefix = '02'
            else:
                compressed_prefix = '03'
            hex_compressed_public_key = compressed_prefix + bitcoin.encode(public_key_x, 16)
            # print ("Compressed Public Key (hex) is:", hex_compressed_public_key)   # USE THIS FOR PUBLIC KEY
            address['Public Key'] = hex_compressed_public_key
            # Generate bitcoin address from public key
            # print ("Bitcoin Address (b58check) is:", bitcoin.pubkey_to_address(public_key))
            # Generate compressed bitcoin address from compressed public key
            # print ("Compressed Bitcoin Address (b58check) is:",  bitcoin.pubkey_to_address(hex_compressed_public_key)) # USE THIS FOR BITCOIN ADDRESS
            address['Bitcoin Address'] = bitcoin.pubkey_to_address(hex_compressed_public_key)
        return address

    def bitcoin_forks_address_generator(self,coin_name):
        private_key,address = generate_wallet(coin_name)
        return private_key,address

    # def generate_multisig_address(self,script_type,pubkey_list=pubkey_list):
    #     # Iterate over the list and generate the public address for each public_key in list
    #     return

    def create_wallet(self, coin_type, user_id):
        # assert wallet_name , 'wallet name is required'
        generate_address = self.generate_new_address()
        wallet_address = generate_address.get('Bitcoin Address')  # BTC ADDRESS

        # Store the address,user_id,coin_type in the phoenix table
        return


    def list_assets(self):
        return

    @view_config(route_name="wallet_deposit", renderer='json')
    def wallet_deposit(self):
        # request = self.request
        address = request.params.get('bitcoin_address')

        import blockcypher
        import pprint

        # pprint.pprint(addr)
        while True:
            addr = blockcypher.get_address_details(address)
            if addr.get('unconfirmed_balance') > 0:
                continue
            else:
                final_balance = blockcypher.from_satoshis(addr.get('final_balance'), 'btc')
                break
        return final_balance

    def btc_withdraw(self,tx_hash):

        from transactions import Transactions
        transactions = Transactions(testnet=False)
        tx_id = transactions.push(tx_hash)

        return tx_id

    @view_config(route_name='obtain_new_btc_address', renderer="json")
    def obtain_new_btc_address(self):
        """
        This is the documentation for obtain_new_btc_address() function
        About:
            This function generates new bitcoin address for the wallet
        Args:
            No Arguments
        :return: (String) returns a new bitcoin address
        """
        request = self.request
        user_id = request.params.get('user_id')
        addr = self.generate_new_address()
        new_btc_addr = addr.get('Bitcoin Address')
        # update the new address in the ASSET table where user_id = user_id
        return new_btc_addr

    def eth_receive(self,address):
        """
        This is the documentation for eth_deposit() function
        About:
            This function returns the final balance of the eth address and stores
            the balance in the database where user_id = user_id
        :param address: eth address
        :return: (float) eth final balance
        """

        address = address
        api = 'TRKB84Y659CDNVGG4PDB4VWSFAP9292FB9'
        url = 'https://api.etherscan.io/api?module=account&action=balance&address='+address+'&tag=latest&apikey='+api
        response = requests.get(url,verify=certifi.where())

        address_content = response.json()
        result = Decimal(address_content.get("result"))/Decimal("1000000000000000000")
        # result = Decimal("200000012345")/Decimal("1000000000000000000")

        print(address_content)

        print(result)


        return result

    def get_eth_current_gasPrice(self):
        from web3 import Web3, HTTPProvider
        # print('coming')
        api = 'TRKB84Y659CDNVGG4PDB4VWSFAP9292FB9'
        url = 'https://api.etherscan.io/api?module=proxy&action=eth_gasPrice&apikey='+api
        response = requests.get(url, verify=certifi.where())
        address_content = response.json()
        result = address_content.get('result')

        gas_price = Web3.toInt(result)
        gas_price_in_eth = Decimal(gas_price) / Decimal("1000000000000000000")
        return gas_price

    def eth_sign_transaction(self):
        return

    def eth_send_transaction_to_network(self):
        # 0xF1526628Ea5c16a3F3c5e0414A9171eE89e7D95c
        private_key = 'd484c2a05c35a1ed8bdd7136e48e01b4b6aa934c569df73487d7b84985d59f3e'
        from_addr = '0x12249d584E86e91112925DcfB6573A15CE57aDa8'
        from web3 import Web3, HTTPProvider
        import rlp
        from ethereum.transactions import Transaction
        # w3 = Web3()
        w3 = Web3(HTTPProvider('https://api.myetherapi.com/eth'))
        # w3 = Web3(HTTPProvider('https://rinkeby.etherscan.io'))
        latest_gas_price = self.get_eth_current_gasPrice()
        transaction_count_url = 'https://rinkeby.etherscan.io/api?module=proxy&action=eth_getTransactionCount&address='+from_addr+'&tag=latest&apikey=ZJKSQPZUCTI9QXSY9412S72NBBJ4DKWA88'
        transaction_request = requests.get(transaction_count_url)
        get_transaction_request_json = transaction_request.json()
        get_transaction_count = w3.toInt(get_transaction_request_json.get('result'))
        # gas_price = 10, gas_limit = 50000
        tx = Transaction(
            nonce=get_transaction_count,
            gasprice=latest_gas_price,
            startgas = 50000,
            to='0x763e4fB6139a6Bb3c7F59D98E8a56C5Fed366a90',
            value=12345,
            data = ''
        )
        tx.sign(private_key)
        raw_tx = rlp.encode(tx)
        print(raw_tx)
        raw_tx_hex = w3.toHex(raw_tx)
        print(raw_tx_hex)
        url_1 = 'https://rinkeby.etherscan.io/api?module=proxy&action=eth_sendRawTransaction&hex='+raw_tx_hex+'&apikey=ZJKSQPZUCTI9QXSY9412S72NBBJ4DKWA88'

        r = requests.post(url_1)
        get_data = r.json()
        # transaction_id = w3.eth.sendRawTransaction(raw_tx_hex)




        # get the private_key of the of user to sign the transaction
        # url = 'https://api.etherscan.io/api?module=proxy&action=eth_sign&address='+private_key+'apikey=YourApiKeyToken'
        return get_data



    def validate_eth_address(self,address):
        from web3 import Web3, HTTPProvider, IPCProvider
        web3 = Web3(IPCProvider())
        if web3.isChecksumAddress(address):
            return True
        else:
            return False

    def erc20token_send(self):
        import erc20token    # supports only tokens with 18 decimals
        import json
        from_addr = '0x4921a61F2733d9Cf265e13865820d7eb435DcBB2'
        # Get contract ABI---Use the below api
        contract_address = '0xef2fcc998847db203dea15fc49d0872c7614910c'
        contract_abi_url = 'https://api.etherscan.io/api?module=contract&action=getabi&address='+contract_address+'&apikey=ZJKSQPZUCTI9QXSY9412S72NBBJ4DKWA88'
        contract_abi_request = requests.get(contract_abi_url)
        contract_abi_json = contract_abi_request.json()
        contract_abi = contract_abi_json.get('result')

        ROPSTEN_CONTRACT_ABI = json.loads(
            '[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_newOwnerCandidate","type":"address"}],"name":"requestOwnershipTransfer","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"issueTokens","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"acceptOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"newOwnerCandidate","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_by","type":"address"},{"indexed":true,"name":"_to","type":"address"}],"name":"OwnershipRequested","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"}],"name":"OwnershipTransferred","type":"event"}]')  # noqa: E501

        provider_endpoint_uri = 'https://mainnet.infura.io/12345678'
        # contract_abi_2 = '[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_newOwnerCandidate","type":"address"}],"name":"requestOwnershipTransfer","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"isMinting","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_amount","type":"uint256"}],"name":"mint","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"acceptOwnership","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"newOwnerCandidate","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_tokenAddress","type":"address"},{"name":"_amount","type":"uint256"}],"name":"transferAnyERC20Token","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"endMinting","outputs":[],"payable":false,"type":"function"},{"anonymous":false,"inputs":[],"name":"MintingEnded","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_by","type":"address"},{"indexed":true,"name":"_to","type":"address"}],"name":"OwnershipRequested","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"}],"name":"OwnershipTransferred","type":"event"}]'
        token_sdk = erc20token.SDK(provider_endpoint_uri='http://159.89.240.246:8545',
                                   private_key='dd5c6c0e12667d0563bc951e6eee5994cc786b6ce6a2192fd17b9d2bc810a25d',
                                   contract_address=contract_address,
                                   contract_abi=ROPSTEN_CONTRACT_ABI,gas_price=10, gas_limit=90000)
        tx_id = token_sdk.send_tokens('0x763e4fB6139a6Bb3c7F59D98E8a56C5Fed366a90', 1)
        # Get Ether balance of some address
        # eth_balance = token_sdk.get_address_ether_balance(from_addr)
        # total_supply = token_sdk.get_token_total_supply()
        # print(total_supply)

        # Get token balance of some address
        # token_balance = token_sdk.get_address_token_balance(from_addr)
        return tx_id


obj = Wallet()
# print(obj.eth_send_transaction_to_network())
print(obj.erc20token_send())
# print(obj.get_eth_current_gasPrice())
# print(obj.eth_receive('0xbeb6f3FBA63E462fDA138c67b4ADbAd416859b8f'))
# print(obj.create_wallet_with_name())
# obj.debit_wallet('501','BTC',123)
# print(obj.check_bc('1DcxZSS8yvErGwXSth5UUjcLV9S4ULDtMy'))
# print(check_bc('1AGNa15ZQXAZUgFiqJ3i7Z2DPU2J6hW62i'))
# print(check_bc("17NdbrSGoUotzeGCcMMCqnFkEvLymoou9j"))
# obj.eth_deposit('0xbeb6f3FBA63E462fDA138c67b4ADbAd416859b8f')
# print(obj.validate_eth_address('0x12249d584E86e91112925DcfB6573A15CE57aDa8'))
