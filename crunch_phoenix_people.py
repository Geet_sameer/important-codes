import tldextract
import uuid
import pandas as pd
import pymongo
import time
from pymongo.errors import DuplicateKeyError
import urllib.request
from datetime import datetime
from dateutil.parser import parse
import json
import ast
import yaml
import phoenixdb
import datetime
from database import *
import seolib as seo

# MAKE CURRENT JOB, PAST JOB , CURRENT COMPANY TABLES
# INSERT INVESTOR DETAILS IN PEOPLE_DATA

import boto
from boto.s3.key import Key
from boto.s3.connection import S3Connection
conn = S3Connection('AKIAJ2WSZQIHD7BQZKLA', 'qDyZ4451CEF1LlkcCPlyRfIU/Wp2K/fXYCTFu/ZV')

# cd /opt/crunchbase/crunchbase_json_mongo

print("Start : %s" % time.ctime())
# time.sleep(5)

client = pymongo.MongoClient('127.0.0.1', 39898)
db = client['CrunchbaseLinks']
people_data_jsons = list(db['Crunchbase_People_Json_Unique1'].find({'phoenix_status': {'$exists': False}},
                                                                               projection=dict(_id=False,
                                                                                               json_data=True,
                                                                                               link=True)).limit(100))
link = [i.get('link') for i in people_data_jsons]
db['Crunchbase_People_Json_Unique1'].update_many(filter={'link': {'$in': link}},
                                                            update={'$set': {'phoenix_status': 'crawling'}})
print("retrieved", len(link))
poeple_data_jsons = list(db['Crunchbase_People_Json_Unique1'].find(projection = dict(_id=False,json_data = True)).limit(1))
dbStuff = PhoenixWrite()

for data in people_data_jsons:
    col_link = data.get('link')
    # print(col_link)
    json_data = data.get('json_data')
    data = json.loads(json_data)
    # data = json.load(data_file)

    # for data in poeple_data_jsons:
    # people_key = str(uuid.uuid4()) + str(uuid.uuid4())

# with open('mark-pincus.json', encoding='utf-8') as data_file:
#     data = json.load(data_file)

    if data.get('properties'):
        if data.get('properties').get('identifier'):
            p_key = data.get('properties').get('identifier').get('uuid')
    # print(p_key)

    # FIRST BLOCK DONE I.E; PROPERTIES
    if data.get('properties'):
        # print(data.get('properties'))
        if data.get('properties').get('identifier'):
            # p_key = data.get('properties').get('identifier').get('uuid')
            people_name = data.get('properties').get('identifier').get('value').replace("\'","''")
            permalink = data.get('properties').get('identifier').get('permalink')
        # person_image = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_thumb,h_120,w_120,f_jpg,g_faces/' + str(data.get('properties').get('identifier').get('image_id'))
        else:
            people_name = ''
            # person_image = ''
            permalink = ''
        if data.get('properties').get('short_description'):
            about_info = data.get('properties').get('short_description').replace("\'","''")
        else:
            about_info = ''
    
        if data.get('properties').get('facet_ids'):
            extra_roles = data.get('properties').get('facet_ids')
        else:
            extra_roles = ''

            # if data.get('properties').get('identifier'):
            #     try:
            #         crunch_image = data.get('properties').get('identifier').get('image_id').split('/')[1]
            #         main_image = data.get('properties').get('identifier').get('image_id')
            #         image_id = str(uuid.uuid4()) + '.jpg'
            #         download_image = urllib.request.urlretrieve(
            #             'https://crunchbase-production-res.cloudinary.com/image/upload/c_lpad,h_120,w_120,f_jpg/' + main_image,
            #             image_id)
            #         our_logo = download_image[0]
            #         image_extension = logo.split('.')[1]
            #     except:
            #         crunch_image = ''
            #         main_image = ''
            #         image_id = ''
            #         download_image = ''
            #         our_logo = ''
            #         image_extension = ''
            # bucket = conn.get_bucket('peop_logo')
            # possible_key = bucket.get_key(p_key)
            # if possible_key:
            #     pass
            # else:
            #     k = Key(bucket)
            #     k.key = p_key
            #     k.set_contents_from_string(our_logo)
    # print(current_position)


    # PERSON IMAGE


    # SOCIAL ACCOUNTS
    if data.get('cards').get('overview_fields2'):
        if data.get('cards').get('overview_fields2').get('facebook'):
            facebook = data.get('cards').get('overview_fields2').get('facebook').get('value').replace("\'","''")
        # facebook_ascii = ''.join(str(ord(c.lower())) for c in facebook)
        else:
            facebook = ''
        # print(facebook)
        if data.get('cards').get('overview_fields2').get('linkedin'):
            linkedin = data.get('cards').get('overview_fields2').get('linkedin').get('value').replace("\'","''")
        # linkedin_ascii = ''.join(str(ord(c.lower())) for c in linkedin)
        # people_linkedin_key = str(uuid.uuid3(uuid.NAMESPACE_X500, str(linkedin_ascii)))
        else:
            linkedin = ''
        # print(linkedin)
        if data.get('cards').get('overview_fields2').get('twitter'):
            twitter = data.get('cards').get('overview_fields2').get('twitter').get('value').replace("\'","''")
        # twitter_ascii = ''.join(str(ord(c.lower())) for c in twitter)
        # people_twitter_key = str(uuid.uuid3(uuid.NAMESPACE_X500, str(twitter_ascii)))
        else:
            twitter = ''
        # print(twitter)
        if data.get('cards').get('overview_fields2').get('website'):
            website = data.get('cards').get('overview_fields2').get('website').get('value').replace("\'","''")
        else:
            website = ''
            # print(website)
    else:
        facebook = ''
        linkedin = ''
        twitter = ''
        website = ''

    # DEMOGRAPHICS
    if data.get('cards').get('overview_fields'):
        if data.get('cards').get('overview_fields').get('gender'):
            gender = data.get('cards').get('overview_fields').get('gender')
        else:
            gender = ''
    else:
        gender = ''
    # print(gender)

    # LOCATIONS
    if data.get('cards').get('overview_fields'):
        try:
            locations = [i.get('value') for i in data.get('cards').get('overview_fields').get('location_identifiers')]
        except:
            locations = ''
        try:
            state = locations[0].replace(' ', '_').replace("\'","''").lower()
        except:
            state = ''
        try:
            city = locations[1].replace(' ', '_').replace("\'","''").lower()
        except:
            city = ''
        try:
            country = locations[2].replace(' ', '_').replace("\'","''").lower()
        except:
            country = ''
        try:
            continent = locations[3].replace(' ', '_').replace("\'","''").lower()
        except:
            continent = ''
    else:
        state = ''
        city = ''
        country = ''
        continent = ''

    # overview_description
    if data.get('cards').get('overview_description'):
        overview_description = data.get('cards').get('overview_description').get('description').replace("\'","''")
    else:
        overview_description = ''
    # print(overview_description)

    # ADD IN PEOPLE_DATA
    if data.get('cards'):
        if data.get('cards').get('investor_overview_fields'):
            investor_stage = data.get('cards').get('investor_overview_fields').get('investor_stage')
            investor_type = data.get('cards').get('investor_overview_fields').get('investor_type')
        else:
            investor_type = ''
            investor_stage = ''
            # print(investor_stage)


            # overview_image_description
    if data.get('cards').get('overview_image_description'):

        # ROLE TABLE
        if data.get('cards').get('overview_image_description').get('primary_job_title'):
            current_position = data.get('cards').get('overview_image_description').get('primary_job_title').replace("\'","''")
        else:
            current_position = ''
        # print(primary_job_title)
        if data.get('cards').get('overview_image_description').get('primary_organization'):
            current_company = data.get('cards').get('overview_image_description').get('primary_organization').get(
                'value').replace("\'","''")
            current_company_permalink = data.get('cards').get('overview_image_description').get(
                'primary_organization').get('permalink')
        else:
            current_company = ''
            current_company_permalink = ''
         

    # NO NEED

    # current_board_and_advisory_roles
    # if data.get('cards').get('current_board_and_advisory_roles_image_list'):
    #     board_and_advisory_roles = data.get('cards').get('current_board_and_advisory_roles_image_list')
    #     # print(data.get('cards').get('current_board_and_advisory_roles_image_list'))
    #     for ba in board_and_advisory_roles:
    #         if ba.get('organization_identifier'):
    #             current_board_organization = ba.get('organization_identifier').get('value')
    #             current_board_organization_permalink = ba.get('organization_identifier').get('permalink')
    #         else:
    #             current_board_organization = ''
    #             current_board_organization_permalink = ''
    #         # print("org",organization)
    #         if ba.get('identifier'):
    #             current_board_title = ba.get('identifier').get('value')
    #         else:
    #             current_board_title = ''
    #         # print("tit",title)
    #         # if ba.get('organization_identifier'):
    #         # 	organization_image = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_lpad,h_100,w_100,f_jpg/' + str(ba.get('organization_identifier').get('image_id'))
    #         # else:
    #         # 	organization_image = ''

    #         if ba.get('started_on'):
    #             current_board_started_on = ba.get('started_on').get('value')
    #             if current_board_started_on:
    #                 current_board_started_on = datetime.datetime.strptime(current_board_started_on, '%Y-%m-%d')
    #                 current_board_started_on_date = current_board_started_on.date().isoformat()
    #             else:
    #                 current_board_started_on_date = ''

    #         else:
    #             current_board_started_on = ''
    #             current_board_started_on_date = ''
    #         # print("started_on", started_on)
    #         if ba.get('title'):
    #             current_board_primary_role = ba.get('title')
    #         else:
    #             current_board_primary_role = ''
            # print("primary",primary_role)
            # print(primary_role)

    # NO NEED

    # if data.get('cards').get('past_board_and_advisory_roles_list'):
    #     past_board_and_advisory_roles = data.get('cards').get('past_board_and_advisory_roles_list')
    #     for pba in past_board_and_advisory_roles:

    #         if pba.get('organization_identifier'):
    #             past_board_organization = pba.get('organization_identifier').get('value')
    #             past_board_organization_permalink = pba.get('organization_identifier').get('permalink')
    #         else:
    #             past_board_organization = ''
    #             past_board_organization_permalink = ''

    #         if pba.get('identifier'):
    #             past_board_title = pba.get('identifier').get('value')
    #         else:
    #             past_board_title = ''

    #         if pba.get('started_on'):
    #             past_board_started_on = pba.get('started_on').get('value')
    #             if past_board_started_on:
    #                 past_board_started_on = datetime.datetime.strptime(past_board_started_on, '%Y-%m-%d')
    #                 past_board_started_on_date = past_board_started_on.date().isoformat()
    #             else:
    #                 past_board_started_on_date = ''

    #         else:
    #             past_board_started_on = ''
    #             past_board_started_on_date = ''

    #         if pba.get('ended_on'):
    #             past_board_ended_on = str(pba.get('ended_on').get('value'))
    #             if past_board_ended_on:
    #                 past_board_ended_on = datetime.datetime.strptime(past_board_ended_on, '%Y-%m-%d')
    #                 past_board_ended_on_date = past_board_ended_on.date().isoformat()
    #             else:
    #                 past_board_ended_on_date = ''

    #         else:
    #             past_board_ended_on = ''
    #             past_board_ended_on_date = ''

    #         if ba.get('title'):
    #             past_board_primary_role = ba.get('title')
    #         else:
    #             past_board_primary_role = ''

    # ALL QUERIES
    try:
        created_date = datetime.datetime.today()
        c_date = created_date.date().isoformat()
        updated_date = datetime.datetime.today()
        u_date = updated_date.date().isoformat()

        # base_set_email_uuid = str(uuid.uuid4())
        # base_set_email = dict(base_key=base_set_email_uuid, base_type='email', base_value=contact_email,
        #                       email_head=email_head,created_date=c_date,updated_date=u_date,
        #                       username=username, hostname=hostname, data_source='CB', data_type='people',
        #                       reference_key=permalink)

        # BASE TABLE
        base_set_twitter_uuid = str(uuid.uuid4())
        base_set_twitter = dict(base_key=base_set_twitter_uuid, base_type='twitter', base_value=twitter, twitter=twitter,
                                data_source='CB', data_type='people',created_date=c_date,updated_date=u_date,
                                reference_key=permalink)

        base_set_facebook_uuid = str(uuid.uuid4())
        base_set_facebook = dict(base_key=base_set_facebook_uuid, base_type='facebook', base_value=facebook,
                                 facebook=facebook,created_date=c_date,updated_date=u_date,
                                 data_source='CB', data_type='people',
                                 reference_key=permalink)

        base_set_linkedin_uuid = str(uuid.uuid4())
        base_set_linkedin = dict(base_key=base_set_linkedin_uuid, base_type='linkedin', base_value=linkedin,
                                 linkedin=linkedin,created_date=c_date,updated_date=u_date,
                                 data_source='CB', data_type='people',
                                 reference_key=permalink)

        people_permalink = permalink
        query = "select * from base_set where reference_key = '{val}'".format(val=people_permalink)
        base_result = dbStuff.runQuery(query=query, select=True)
        if base_result:
            pass
        else:
            dbStuff.upsert_one(base_set_twitter, tableName='base_set')
            dbStuff.upsert_one(base_set_facebook, tableName='base_set')
            dbStuff.upsert_one(base_set_linkedin, tableName='base_set')


        # PEOPLE DATA
        # CHECK WHETHER THE PERMALINK IS THERE IN PEOPLE_DATA, IF NOT THEN INSERT ELSE UPDATE
        people_permalink = permalink
        query = "select people_permalink from people_data where people_permalink = '{val}'".format(val = people_permalink)
        # print(query)
        people_result = dbStuff.runQuery(query=query,select=True)
        # print('people result', people_result)
        if people_result:
            pass
        else:
            people_data_dict = dict(people_permalink=permalink,people_name=people_name,people_description=overview_description,people_facebook=facebook,people_twitter=twitter,
                                    people_linkedin=linkedin,website=website,gender=gender)
            dbStuff.upsert_one(people_data_dict, tableName='people_data')

        # DEMOGRAPHICS
        demographics_dict_uuid = str(uuid.uuid4())
        demographics_dict = dict(demographics_key = demographics_dict_uuid,state = state, city = city, country = country, continent = continent,
                                 people_permalink = people_permalink,data_type='people')
        people_permalink = permalink
        query = "select * from demographics where people_permalink = '{val}'".format(val=people_permalink)
        demographics_result = dbStuff.runQuery(query=query, select=True)
        if demographics_result:
            pass
        else:
            dbStuff.upsert_one(demographics_dict, tableName='demographics')


        # IMAGE TABLE
        # people_permalink = permalink
        # query = "select * from image_table where people_permalink = '{val}'".format(val=people_permalink)
        # images_result = dbStuff.runQuery(query=query, select=True)
        # images_dict_uuid = str(uuid.uuid4())
        # images_dict = dict(image_key=images_dict_uuid,people_permalink=permalink,data_type = 'people',extension='.jpg',source='crunchbase',
        #                    host_provider = 'S3',bucket_name = 'people_phoenix_logo',bucket_key=permalink,our_logo = our_logo)
        # if images_result:
        #     pass
        # else:
        #     pass
            # dbStuff.upsert_one(images_dict,tableName='image_table')

        # ROLE TABLE
        people_permalink = permalink
        query = "select * from role where people_permalink = '{val}'".format(val=people_permalink)
        role_result = dbStuff.runQuery(query=query, select=True)
        role_dict_uuid = str(uuid.uuid4())
        role_dict = dict(role_key = role_dict_uuid,people_permalink=permalink,current_positon=current_position,current_company=current_company_permalink,
                        current_company_name = current_company)
        if role_result:
            pass
        else:
            
            dbStuff.upsert_one(role_dict,tableName='role')

        people_permalink = permalink
        query = "select * from people_investor_type where people_permalink = '{val}'".format(val=people_permalink)
        inv_result = dbStuff.runQuery(query=query, select=True)
        if inv_result:
            pass
        else:
            if investor_type:
                for it in investor_type:
                    inv_uuid = str(uuid.uuid4())
                    inv_dict = dict(investor_type_key=inv_uuid,people_permalink=permalink,investor_type=it.replace("\'","''"))
                    dbStuff.upsert_one(inv_dict,tableName = 'people_investor_type')
            else:
                pass

        people_permalink = permalink
        query = "select * from people_investor_stage where people_permalink = '{val}'".format(val=people_permalink)
        inv_stage_result = dbStuff.runQuery(query=query, select=True)
        if inv_stage_result:
            pass
        else:
            if investor_stage:
                for i_stage in investor_stage:
                    inv_stage_uuid = str(uuid.uuid4())
                    inv_stage_dict = dict(investor_stage_key = inv_stage_uuid,people_permalink=permalink,investor_stage=i_stage.replace("\'","''"))
                    dbStuff.upsert_one(inv_stage_dict,tableName = 'people_investor_stage')
            else:
                pass

        people_permalink = permalink
        query = "select * from people_other_roles where people_permalink = '{val}'".format(val=people_permalink)
        people_other_roles_result = dbStuff.runQuery(query=query, select=True)
        if people_other_roles_result:
            pass
        else:
            if extra_roles:
                for er in extra_roles:
                    er_uuid = str(uuid.uuid4())
                    er_dict = dict(other_roles_key = er_uuid,people_permalink=permalink,other_roles=er.replace("\'","''"))
                    dbStuff.upsert_one(er_dict,tableName = 'people_other_roles')
            else:
                pass


      

        # INSERT IN CURRENT_EMPLOYEES TABLE
        if data.get('cards').get('current_jobs_image_list'):
            current_jobs_image = data.get('cards').get('current_jobs_image_list')
            people_permalink = permalink
            query = "select * from current_employees where people_permalink = '{val}'".format(val=people_permalink)
            current_employees_result = dbStuff.runQuery(query=query, select=True)
            if current_employees_result:
                pass
            else:

                for cj in current_jobs_image:
                    if cj.get('organization_identifier'):
                        current_job = cj.get('organization_identifier').get('value').replace("\'","''")
                        current_job_organization_permalink = cj.get('organization_identifier').get('permalink')
                    else:
                        current_job = ''
                        current_job_organization_permalink = ''

                    if cj.get('identifier'):
                        current_job_title = cj.get('identifier').get('value').replace("\'","''")
                    else:
                        current_job_title = ''

                    if cj.get('started_on'):
                        current_job_started_on = cj.get('started_on').get('value')
                        # print("current job", current_job_started_on)
                        if current_job_started_on:
                            current_job_started_on = datetime.datetime.strptime(current_job_started_on, '%Y-%m-%d')
                            current_job_started_on_date = current_job_started_on.date().isoformat()
                        else:
                            current_job_started_on_date = ''

                    else:
                        current_job_started_on = ''
                        current_job_started_on_date = ''

                    if cj.get('title'):
                        current_job_primary_role = cj.get('title').replace("\'","''")
                    else:
                        current_job_primary_role = ''

                    # print(type(current_job_started_on_date))
                    current_job_dict_uuid = str(uuid.uuid4())
                    # cur_emp_dict['current_employees_key'] = current_job_dict_uuid
                    # cur_emp_dict['primary_role'] = current_job_primary_role
                    current_job_dict = dict(current_employees_key=current_job_dict_uuid,company_permalink=current_job_organization_permalink,people_name=people_name,people_permalink = permalink,
                                            primary_role = current_job_primary_role,start_date=current_job_started_on_date,title=current_job_title)
                    dbStuff.upsert_one(current_job_dict,tableName='current_employees')


        # INSERT IN PAST_EMPLOYEES TABLE
        
        if data.get('cards').get('past_jobs_list'):
            past_jobs_image = data.get('cards').get('past_jobs_list')
            people_permalink = permalink
            query = "select * from past_employees where people_permalink = '{val}'".format(val=people_permalink)
            past_employees_result = dbStuff.runQuery(query=query, select=True)
            if past_employees_result:
                pass
            else:
                for pj in past_jobs_image:
                    if pj.get('organization_identifier'):
                        past_job = pj.get('organization_identifier').get('value').replace("\'","''")
                        past_job_organization_permalink = pj.get('organization_identifier').get('permalink')
                    else:
                        past_job = ''
                        past_job_organization_permalink = ''

                    if pj.get('identifier'):
                        past_job_title = pj.get('identifier').get('value').replace("\'","''")
                    else:
                        past_job_title = ''

                    if pj.get('started_on'):
                        past_job_started_on = pj.get('started_on').get('value')
                        if past_job_started_on:
                            past_job_started_on = datetime.datetime.strptime(past_job_started_on, '%Y-%m-%d')
                            past_job_started_on_date = past_job_started_on.date().isoformat()
                        else:
                            past_job_started_on_date = ''

                    else:
                        past_job_started_on = ''
                        past_job_started_on_date = ''

                    if pj.get('ended_on'):
                        past_job_ended_on = pj.get('ended_on').get('value')
                        if past_job_ended_on:
                            past_job_ended_on = datetime.datetime.strptime(past_job_ended_on, '%Y-%m-%d')
                            past_job_ended_on_date = past_job_ended_on.date().isoformat()
                        else:
                            past_job_ended_on_date = ''
                            

                    else:
                        past_job_ended_on = ''
                        past_job_ended_on_date = ''

                    if pj.get('title'):
                        past_job_primary_role = pj.get('title').replace("\'","''")
                    else:
                        past_job_primary_role = ''

                    past_job_dict_uuid = str(uuid.uuid4())
                    past_job_dict = dict(past_employees_key=past_job_dict_uuid,
                                            company_permalink=past_job_organization_permalink, people_name=people_name,
                                            people_permalink=permalink,
                                            primary_role=past_job_primary_role , start_date=past_job_started_on_date,end_date = past_job_ended_on_date,
                                            title=past_job_title)
                    dbStuff.upsert_one(past_job_dict,tableName='past_employees')

        # INSERT IN EDUCATION TABLE

        # GET FROM COMPANY_DATA...FOLLOW THE ABOVE LOGIC
        # education_image_list
        if data.get('cards').get('education_image_list'):
            education = data.get('cards').get('education_image_list')
            people_permalink = permalink
            query = "select * from education where people_permalink = '{val}'".format(val=people_permalink)
            education_result = dbStuff.runQuery(query=query, select=True)
            if education_result:
                pass
            else:
                for edu in education:
                    if edu.get('school_identifier'):
                        education_institute = edu.get('school_identifier').get('value').replace("\'","''")
                        education_permalink = edu.get('school_identifier').get('permalink')
                    else:
                        education_institute = ''
                        education_permalink = ''
        
                    if edu.get('subject'):
                        subject = edu.get('subject').replace("\'","''")
                    else:
                        subject = ''
                    # print(subject)
                    if edu.get('identifier'):
                        title = edu.get('identifier').get('value')
                        if title:
                            title = edu.get('identifier').get('value').replace("\'","''")
                        else:
                            title = ''
                    else:
                        title = ''
                    # print(title)
                    if edu.get('completed_on'):
                        completed_on = edu.get('completed_on').get('value')
                        if completed_on:
                            completed_on = datetime.datetime.strptime(completed_on, '%Y-%m-%d')
                            completed_on_date = completed_on.date().isoformat()
                        else:
                            completed_on_date = ''
                           
                            # completed_on_date = [d.to_datetime() for d in pd.to_datetime(completed_on)]
                    else:
                        completed_on = ''
                        completed_on_date = ''
                    if edu.get('type_name'):
                        degree = edu.get('type_name')
                        if degree:
                            degree = edu.get('type_name').replace("\'","''")
                        else:
                            degree = ''
                    else:
                        degree = ''

                    edu_dict_uuid = str(uuid.uuid4())
                    edu_dict = dict(education_key=edu_dict_uuid,people_permalink=permalink,education_institute=education_institute,
                                    education_institute_permalink=education_permalink,degree=degree,subject=subject,title=title.replace('@','at'),completed_on=completed_on_date)
                    dbStuff.upsert_one(edu_dict,tableName='education')

        db['Crunchbase_People_Json_Unique1'].update_one(filter={'link': col_link},update={'$set': {'phoenix_status': 'inserted'}})
    except:
        # print('exception')
        print('GOT EXCEPTION', col_link)
        db['Crunchbase_organization_Json_Unique_26Nov'].update_one(filter={'link': col_link},update={'$set': {'phoenix_status': 'exception'}})

print("End : %s" % time.ctime())
