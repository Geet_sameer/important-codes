'use strict';
const RippleAPI = require('ripple-lib').RippleAPI; // require('ripple-lib')

const api = new RippleAPI({server: 'wss://s.altnet.rippletest.net:51233'});
const address = 'rBoeec6SoM3HqsCYJq4FbUmiCMKKQ4xWZH';

api.connect().then(() => {
  api.getBalances(address).then(balances => {
    console.log(JSON.stringify(balances, null, 2));
    process.exit();
  });
});
