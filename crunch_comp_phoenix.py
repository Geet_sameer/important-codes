import tldextract
import uuid
import pandas as pd
import pymongo
import time
from pymongo.errors import DuplicateKeyError
import urllib.request
from datetime import datetime
from dateutil.parser import parse
import json
import ast
import yaml
import phoenixdb
import datetime
from database import *
import seolib as seo


import boto
from boto.s3.key import Key
from boto.s3.connection import S3Connection

conn = S3Connection('AKIAJ2WSZQIHD7BQZKLA', 'qDyZ4451CEF1LlkcCPlyRfIU/Wp2K/fXYCTFu/ZV')
# client = pymongo.MongoClient('127.0.0.1', 39898)
# dbinhouse = client['inhouse']
# db = client['CrunchbaseLinks']
print("Start : %s" % time.ctime())
time.sleep(5)
# cd /opt/crunchbase/crunchbase_json_mongo

client = pymongo.MongoClient('127.0.0.1', 39898)
db = client['CrunchbaseLinks']


company_data_jsons = list(db['Crunchbase_organization_Json_Unique_26Nov'].find({'phoenix_status': {'$exists': False}},
                                                                               projection=dict(_id=False,
                                                                                               json_data=True,
                                                                                               link=True)).limit(1000))
link = [i.get('link') for i in company_data_jsons]
db['Crunchbase_organization_Json_Unique_26Nov'].update_many(filter={'link': {'$in': link}},
                                                            update={'$set': {'phoenix_status': 'crawling'}})




print("retrieved", len(link))

# print(data)

count = 0
dbStuff = PhoenixWrite()
# for data in company_data_jsons:

# with open('bluestem-brands.json', encoding='utf-8') as data_file:
#     data = json.load(data_file)

    col_link = data.get('link')
    # print(col_link)
    json_data = data.get('json_data')
    data = json.loads(json_data)
    # print(data.get('cards'))

    # base_key = str(uuid.uuid4())

    if data.get('properties'):
        if data.get('properties').get('identifier'):
            c_key = data.get('properties').get('identifier').get('uuid')

    if data.get('cards').get('overview_fields2'):
        if data.get('cards').get('overview_fields2').get('website'):
            if data.get('cards').get('overview_fields2').get('website').get('value'):
                website = data.get('cards').get('overview_fields2').get('website').get('value')
                ext = tldextract.extract(website)
                domain = '.'.join(ext[1:3])
                company_key_ascii = ''.join(str(ord(c.lower())) for c in domain)
                # c_key = data.get('properties').get('identifier').get('uuid')
                company_domain_key = str(uuid.uuid3(uuid.NAMESPACE_X500, str(company_key_ascii)))
        else:
            website = ''
            ext = ''
            domain = ''
            # c_key = ''
            company_key_ascii = ''
            company_domain_key = ''
    else:
        website = ''
        ext = ''
        domain = ''
        # c_key = ''
        company_key_ascii = ''
        company_domain_key = ''

    # FIRST BLOCK DONE I.E; PROPERTIES
    if data.get('properties'):
        # print(data.get('properties'))
        if data.get('properties').get('identifier'):
            try:
                company_name = data.get('properties').get('identifier').get('value')
                
            except:
                company_name = ''
            try:
                permalink = data.get('properties').get('identifier').get('permalink')
            except:
                permalink = ''
            company_logo = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_thumb,h_120,w_120,f_jpg,g_faces/' + str(data.get('properties').get('identifier').get('image_id'))
            try:
                logo = data.get('properties').get('identifier').get('image_id').rsplit('/',1)[-1]
                # print('logo',logo)
                # main_logo = data.get('properties').get('identifier').get('image_id')
                image_id = str(permalink) +'tdc1000' + str(uuid.uuid4()) + '.jpg'
                download_image = urllib.request.urlretrieve(
                    'https://crunchbase-production-res.cloudinary.com/image/upload/c_lpad,h_120,w_120,f_jpg/' + logo,
                    image_id)
                our_logo = download_image[0]
                comp_logo = str(uuid.uuid4()) + '.' + logo.split('.')[1]
                logo_extension = logo.split('.')[1]
            except:
                logo_extension = ''
                logo = ''
                comp_logo = ''
                download_image = ''
                our_logo = ''

            bucket = conn.get_bucket('comp_phoenix_logo')
            possible_key = bucket.get_key(permalink)
            # k = Key(bucket)
            # k.key = permalink
            # k.set_contents_from_string(our_logo)
            if possible_key:
                pass
            else:
                k = Key(bucket)
                k.key = permalink
                k.set_contents_from_string(our_logo)
        else:
            company_name = ''
            permalink = ''
            logo_extension = ''
            comp_logo = ''
            logo = ''
            comp_log = ''
            download_image = ''

        if data.get('properties').get('short_description'):
            about_info = data.get('properties').get('short_description')
        else:
            about_info = ''

    # COMPANY LOCATIONS                         
    if data.get('cards').get('overview_image_description'):
        try:
            company_locations = [i.get('value') for i in
                                 data.get('cards').get('overview_image_description').get('location_identifiers')]
            # db['company'].insert_one(document = {'company_locations':company_locations})
            try:
                state = company_locations[0].replace(' ', '_').lower()
            except:
                state = ''
            try:
                city = company_locations[1].replace(' ', '_').lower()
            except:
                city = ''
            try:
                country = company_locations[2].replace(' ', '_').lower()
            except:
                country = ''
            try:
                continent = company_locations[3].replace(' ', '_').lower()
            except:
                continent = ''
                # print(city)
        except:
            state = ''
            city = ''
            country = ''
            continent = ''
            # QUERY HERE

    # Company company_categories
    # category_dict_uuid = str(uuid.uuid4())
    cat_list = []
    if data.get('cards').get('overview_fields').get('categories'):
        categories = data.get('cards').get('overview_fields').get('categories')
        company_permalink = permalink
        query = "select * from categories where company_permalink = '{val}'".format(val=company_permalink)
        categories_result = dbStuff.runQuery(query=query, select=True)
        if categories_result:
            pass
        else:
            for cat in categories:
                # categories = cat.get('value')
                cat_list.append(cat.get('value'))
                # category_dict_uuid = str(uuid.uuid4())
                # categories_dict = dict(category_key=category_dict_uuid,company_permalink=permalink,categories=categories)
                # dbStuff.upsert_one(categories_dict,tableName='categories')


    # print('cat',cat_list)

    # FOUNDERS

    founders_list = []
    founders_dict_permalink = {}
    if data.get('cards').get('overview_fields').get('founder_identifiers'):
        founders = data.get('cards').get('overview_fields').get('founder_identifiers')
        company_permalink = permalink
        query = "select * from founders where company_permalink = '{val}'".format(val=company_permalink)
        founders_result = dbStuff.runQuery(query=query, select=True)
        if founders_result:
            pass
        else:
            for fn in founders:
                # founder_dict_uuid = str(uuid.uuid4())
                founders_dict_permalink[fn.get('permalink')] = fn.get('value')
                # company_founders = founders_list.append(fn.get('permalink'))
                # company_founders_name = founders_list.append(fn.get('value'))
                # founders_dict = dict(founders_key=founder_dict_uuid,company_permalink=permalink,people_permalink=fn.get('permalink'),people_name=fn.get('value'))
                # dbStuff.upsert_one(founders_dict,tableName='founders')
    # print('foun',founders_dict_permalink)

    # SOCIAL ACCOUNTS
    if data.get('cards').get('overview_fields2'):
        if data.get('cards').get('overview_fields2').get('facebook'):
            facebook = data.get('cards').get('overview_fields2').get('facebook').get('value')
        else:
            facebook = ''
        # print(facebook)
        if data.get('cards').get('overview_fields2').get('linkedin'):
            linkedin = data.get('cards').get('overview_fields2').get('linkedin').get('value')
        else:
            linkedin = ''
        # print(linkedin)
        if data.get('cards').get('overview_fields2').get('twitter'):
            twitter = data.get('cards').get('overview_fields2').get('twitter').get('value')
        else:
            twitter = ''
        # print(twitter)
        if data.get('cards').get('overview_fields2').get('website'):
            website = data.get('cards').get('overview_fields2').get('website').get('value')
        else:
            website = ''
        # print(website)

        if data.get('cards').get('overview_fields2').get('phone_number'):
            phone_number = data.get('cards').get('overview_fields2').get('phone_number')
        else:
            phone_number = ''

        if data.get('cards').get('overview_fields2').get('contact_email'):
            contact_email = data.get('cards').get('overview_fields2').get('contact_email')
            # print('email',contact_email)
        else:
            contact_email = ''
    else:
        facebook = ''
        linkedin = ''
        twitter = ''
        phone_number = ''
        contact_email = ''

    # print(contact_email)

    # overview_fields 
    if data.get('cards').get('overview_fields'):
        if data.get('cards').get('overview_fields').get('founded_on'):
            founded_on = data.get('cards').get('overview_fields').get('founded_on').get('value')
            # founded_on_date = [d.to_pydatetime() for d in pd.to_datetime(founded_on)]
            if founded_on:
                founded_on = datetime.datetime.strptime(founded_on,'%Y-%m-%d')
                founded_on_date = founded_on.date().isoformat()
                # founded_on_date = datetime.strptime(founded_on, '%Y-%m-%d')
                # print(type(founded_on_date))
            else:
                founded_on_date = ''
                # founded_on = '1970-01-01'
                # founded_on = datetime.datetime.strptime(founded_on,'%Y-%m-%d')
                # founded_on_date = founded_on.date().isoformat()

            # print(type(founded_on_date))
        else:
            founded_on_date = ''
            # founded_on = '1970-01-01'
            # founded_on = datetime.datetime.strptime(founded_on, '%Y-%m-%d')
            # founded_on_date = founded_on.date().isoformat()
        # print(founded_on_date)
        if data.get('cards').get('overview_fields').get('operating_status'):
            operating_status = data.get('cards').get('overview_fields').get('operating_status')
        else:
            operating_status = ''
        # print(operating_status)
        if data.get('cards').get('overview_fields').get('num_employees_enum'):
            total_employees = data.get('cards').get('overview_fields').get('num_employees_enum').replace('c_0',
                                                                                                         '').replace(
                '_0', '-').replace('c_', '').replace('_max', '+')
        else:
            total_employees = ''
        # print(total_employees)
        if data.get('cards').get('overview_fields').get('last_funding_type'):
            last_funding_type = data.get('cards').get('overview_fields').get('last_funding_type')
        else:
            last_funding_type = ''
        # print(last_funding_type)
        if data.get('cards').get('overview_fields').get('funding_stage'):
            funding_stage = data.get('cards').get('overview_fields').get('funding_stage')
        else:
            funding_stage = ''
    else:
        funding_stage = ''
        last_funding_type = ''
        total_employees = ''
        operating_status = ''
        founded_on = ''
        founded_on_date = ''

    # IPO FIELDS
    if data.get('cards').get('ipo_fields'):
        if data.get('cards').get('ipo_fields').get('ipo_amount_raised'):
            ipo_amount_raised_usd = data.get('cards').get('ipo_fields').get('ipo_amount_raised').get('value_usd')
            currency = data.get('cards').get('ipo_fields').get('ipo_amount_raised').get('currency')
        else:
            ipo_amount_raised_usd = 0
            currency = ''
        # print("ipo",ipo_amount_raised_usd)
        # print(currency)
        if data.get('cards').get('ipo_fields').get('went_public_on'):
            went_public_on = data.get('cards').get('ipo_fields').get('went_public_on')
            # went_public_on_date = [d.to_pydatetime() for d in pd.to_datetime(went_public_on)]
            if went_public_on:
                went_public_on = datetime.datetime.strptime(went_public_on, '%Y-%m-%d')
                went_public_on_date = went_public_on.date().isoformat()
                
            else:
                went_public_on_date = ''
                # went_public_on = '1970-01-01'
                # went_public_on = datetime.datetime.strptime(went_public_on, '%Y-%m-%d')
                # went_public_on_date = went_public_on.date().isoformat()
        else:
            went_public_on_date = ''
            # went_public_on = '1970-01-01'
            # went_public_on = datetime.datetime.strptime(went_public_on, '%Y-%m-%d')
            # went_public_on_date = went_public_on.date().isoformat()
        # print(went_public_on)
        # print("date",went_public_on_date)
        if data.get('cards').get('ipo_fields').get('stock_link'):
            stock_link_label = data.get('cards').get('ipo_fields').get('stock_link').get('label')
            stock_link_value = data.get('cards').get('ipo_fields').get('stock_link').get('value')
        else:
            stock_link_label = ''
            stock_link_value = ''
        # print(stock_link_label)
        # print(value)

        if data.get('cards').get('ipo_fields').get('ipo_valuation'):
            ipo_valuation_usd = data.get('cards').get('ipo_fields').get('ipo_valuation').get('value_usd')
            currency = data.get('cards').get('ipo_fields').get('ipo_valuation').get('currency')
        else:
            ipo_valuation_usd = 0
            currency = ''
        # print(ipo_valuation)
        # print(currency)

        if data.get('cards').get('ipo_fields').get('ipo_share_price'):
            ipo_share_price_usd = data.get('cards').get('ipo_fields').get('ipo_share_price').get('value_usd')
            currency = data.get('cards').get('ipo_fields').get('ipo_share_price').get('currency')
        else:
            ipo_share_price_usd = 0
            currency = ''
    else:
        ipo_amount_raised_usd = 0
        ipo_share_price_usd = 0
        ipo_valuation_usd = 0
        stock_link_label = ''
        currency = ''
        value = ''
        went_public_on_date = ''
        went_public_on = ''
        stock_link_value = ''

    # IF DOMAIN NOT PRESENT , CHECK FROM CONTACT_EMAIL
    try:
        if company_domain_key:
            pass
        else:
            domain = contact_email.split('@')[1]
            company_key_ascii = ''.join(str(ord(c.lower())) for c in domain)
            company_domain_key = str(uuid.uuid3(uuid.NAMESPACE_X500, str(company_key_ascii)))
    except:
        pass


    if data.get('cards').get('overview_investor_fields'):
        investor_type = data.get('cards').get('overview_investor_fields').get('investor_type')
        investor_stage = data.get('cards').get('overview_investor_fields').get('investor_stage')
    else:
        investor_type = ''
        investor_stage = ''

    if data.get('properties'):
        company_other_roles = data.get('properties').get('facet_ids')
    else:
        company_other_roles = ''

    sub_orgs_list = []
    sub_orgs_permalink = {}
    if data.get('cards').get('sub_organizations_image_list'):
        orgs = data.get('cards').get('sub_organizations_image_list')
        company_permalink = permalink
        query = "select * from company_sub_organizations where company_permalink = '{val}'".format(val=company_permalink)
        sub_orgs_result = dbStuff.runQuery(query=query, select=True)
        if sub_orgs_result:
            pass
        else:
            for org in orgs:
                # sub_orgs_list.append(org.get('ownee_identifier').get('permalink'))
                sub_orgs_permalink[org.get('ownee_identifier').get('permalink')] = org.get('ownee_identifier').get('value')
                # comp_sub_uuid = str(uuid.uuid4())
                # comp_sub_dict = dict(company_permalink=permalink,sub_orgs = org.get('ownee_identifier').get('permalink'))
                # dbStuff.upsert_one(comp_sub_dict,tableName='company_sub_organizations')

    # print('sub_orgs',sub_orgs_list)

    exits_list = []
    if data.get('cards').get('exits_image_list'):
        exits = data.get('cards').get('exits_image_list')
        for ex in exits:
            comp_exits = exits_list.append(ex.get('identifier').get('permalink'))

    if data.get('cards').get('owner_identifier'):
        company_owned_by = data.get('cards').get('owner_identifier').get('permalink')
    else:
        company_owned_by = ''

    if data.get('cards').get('overview_company_fields'):
        company_type = data.get('cards').get('overview_company_fields').get('company_type')
    else:
        company_type = ''

    # INCLUDE THIS
    # db['company_domains'].insert_one(document = {'company_name':company_name,'company_permalink':permalink,'domain':domain,'company_key':company_key,'company_logo':company_logo})
    created_date = datetime.datetime.today()
    c_date = created_date.date().isoformat()
    updated_date = datetime.datetime.today()
    u_date = updated_date.date().isoformat()

    if contact_email:
        username = contact_email.split('@')[0]
        hostname = domain
        if 'jobs' in contact_email:
            email_head = 'jobs'
        elif 'support' in contact_email:
            email_head = 'support'
        else:
            email_head = 'contact'
    else:
        email_head = ''
        username = ''
        hostname = ''
    

    # ALL QUERIES

    try:
        base_set_domain_uuid = str(uuid.uuid4())
        base_set_domain = dict(base_key=base_set_domain_uuid, base_type='domain', base_value=domain, created_date=c_date,
                               updated_date=u_date,
                               data_source='CB', data_type='company', reference_key=permalink)

        base_set_email_uuid = str(uuid.uuid4())
        base_set_email = dict(base_key=base_set_email_uuid, base_type='email', base_value=contact_email,
                              email_head=email_head,created_date=c_date,updated_date=u_date,
                              username=username, hostname=hostname, data_source='CB', data_type='company',
                              reference_key=permalink)

        base_set_twitter_uuid = str(uuid.uuid4())
        base_set_twitter = dict(base_key=base_set_twitter_uuid, base_type='twitter', base_value=twitter.replace("\'","''"), twitter=twitter.replace("\'","''"),
                                data_source='CB', data_type='company',created_date=c_date,updated_date=u_date,
                                reference_key=permalink)

        base_set_facebook_uuid = str(uuid.uuid4())
        base_set_facebook = dict(base_key=base_set_facebook_uuid, base_type='facebook', base_value=facebook.replace("\'","''"),
                                 facebook=facebook.replace("\'","''"),created_date=c_date,updated_date=u_date,
                                 data_source='CB', data_type='company',
                                 reference_key=permalink)

        base_set_linkedin_uuid = str(uuid.uuid4())
        base_set_linkedin = dict(base_key=base_set_linkedin_uuid, base_type='linkedin', base_value=linkedin.replace("\'","''"),
                                 linkedin=linkedin.replace("\'","''"),created_date=c_date,updated_date=u_date,
                                 data_source='CB', data_type='company',
                                 reference_key=permalink)

        base_set_phone_uuid = str(uuid.uuid4())
        base_set_phone = dict(base_key=base_set_phone_uuid, base_type='phone', base_value=phone_number,
                              phone_no=phone_number,created_date=c_date,updated_date=u_date,
                              data_source='CB', data_type='company',
                              reference_key=permalink)

        company_permalink = permalink
        query = "select * from base_set where reference_key = '{val}'".format(val=company_permalink)
        base_result = dbStuff.runQuery(query=query, select=True)
        if base_result:
            pass
        else:

            dbStuff.upsert_one(base_set_domain, tableName='base_set')
            dbStuff.upsert_one(base_set_email, tableName='base_set')
            dbStuff.upsert_one(base_set_twitter, tableName='base_set')
            dbStuff.upsert_one(base_set_facebook, tableName='base_set')
            dbStuff.upsert_one(base_set_linkedin, tableName='base_set')
            dbStuff.upsert_one(base_set_phone, tableName='base_set')



        # CHECK WHETHER THE PERMALINK IS THERE IN COMPANY_DATA, IF NOT THEN INSERT ELSE UPDATE
        company_permalink = permalink
        query = "select * from company_data where company_permalink = '{val}'".format(val =company_permalink)
        result = dbStuff.runQuery(query=query,select=True)
        # print('company',result)
        if result:
            pass
            # print('there')
        else:
            # print('came to else')
            # company_uuid = str(uuid.uuid4())
            url = 'http://www.' + str(domain)
            alexa_rank = seo.get_alexa(url)
            if alexa_rank:
                pass
            else:
                alexa_rank = 0
            # print('alexa',alexa_rank)
            # print('ipo_amount_raised_usd',ipo_amount_raised_usd)
            # print('ipo_valuation_usd',ipo_valuation_usd)

            # CHANGE COLUMN DATA TYPE AS INTEGER WHERE THERE IS STRING
            # print(company_name.replace("'",'"'))
            # print(about_info.replace("'",'"'))
            company_data_dict = dict(company_key = company_domain_key,company_permalink = permalink,company_name=company_name.replace("\'","''"),domain =domain,
                                     company_description=about_info.replace("\'","''"),website = website,company_email=contact_email,founded_on =founded_on_date,total_employees = str(total_employees),
                                     operating_status = operating_status,company_type = company_type,last_funding_type =last_funding_type,company_twitter=twitter.replace("\'","''"),company_facebook=facebook.replace("\'","''"),company_linkedin=linkedin.replace("\'","''"),
                                     phone_no = phone_number,funding_stage = funding_stage,ipo_amount_raised_usd = int(ipo_amount_raised_usd),went_public_on=went_public_on_date,
                                     stock_symbol = stock_link_label,stock_link_value = stock_link_value,ipo_valuation_usd = int(ipo_valuation_usd),alexa_ranking = int(alexa_rank))

            dbStuff.upsert_one(company_data_dict, tableName='company_data')

        demographics_dict_uuid = str(uuid.uuid4())
        demographics_dict = dict(demographics_key = demographics_dict_uuid,state = state.replace("\'","''"), city = city.replace("\'","''"), country = country.replace("\'","''"), continent = continent.replace("\'","''"),
                                 company_permalink = company_permalink,data_type='company')
        company_permalink = permalink
        query = "select * from demographics where company_permalink = '{val}'".format(val=company_permalink)
        demographics_result = dbStuff.runQuery(query=query, select=True)
        if demographics_result:
            pass
        else:
            dbStuff.upsert_one(demographics_dict, tableName='demographics')


        company_permalink = permalink
        query = "select * from image_table where company_permalink = '{val}'".format(val=company_permalink)
        images_result = dbStuff.runQuery(query=query, select=True)
        images_dict_uuid = str(uuid.uuid4())
        images_dict = dict(image_key=images_dict_uuid,company_permalink=permalink,data_type = 'company',extension='.jpg',source='crunchbase',
                           host_provider = 'S3',bucket_name = 'comp_phoenix_logo',bucket_key=permalink,our_logo = our_logo)
        if images_result:
            pass
        else:
            dbStuff.upsert_one(images_dict,tableName='image_table')


        # company_permalink = permalink
        # query = "select * from founders where company_permalink = '{val}'".format(val=company_permalink)
        # founder_result = dbStuff.runQuery(query=query, select=True)
        # if foun
        if cat_list:
            for c in cat_list:
                category_dict_uuid = str(uuid.uuid4())
                categories_dict = dict(category_key=category_dict_uuid,company_permalink=permalink,categories=c.replace("\'","''"))
                dbStuff.upsert_one(categories_dict,tableName='categories')
        else:
            pass

        # print("founders",founders_dict_permalink)
        if founders_dict_permalink:
            for key in founders_dict_permalink:
                founders_key = str(uuid.uuid4())
                founders_dict = dict(founders_key=founders_key,company_permalink=permalink.replace("\'","''"),people_permalink=key.replace("\'","''"),people_name=founders_dict_permalink[key].replace("\'","''"))
                dbStuff.upsert_one(founders_dict,tableName='founders')
        else:
            pass

        if sub_orgs_permalink:
            for s in sub_orgs_permalink:
                comp_sub_uuid = str(uuid.uuid4())
                comp_sub_dict = dict(sub_org_key=comp_sub_uuid,company_permalink=permalink.replace("\'","''"),company_name = sub_orgs_permalink[s].replace("\'","''"),sub_orgs_permalink=s.replace("\'","''"))
                dbStuff.upsert_one(comp_sub_dict,tableName='company_sub_organizations')
        else:
            pass

        company_permalink = permalink
        query = "select * from company_investor_type where company_permalink = '{val}'".format(val=company_permalink)
        inv_result = dbStuff.runQuery(query=query, select=True)
        if inv_result:
            pass
        else:
            if investor_type:
                for it in investor_type:
                    inv_uuid = str(uuid.uuid4())
                    inv_dict = dict(investor_type_key=inv_uuid,company_permalink=permalink,investor_type=it.replace("\'","''"))
                    dbStuff.upsert_one(inv_dict,tableName = 'company_investor_type')
            else:
                pass


        company_permalink = permalink
        query = "select * from company_investor_stage where company_permalink = '{val}'".format(val=company_permalink)
        inv_stage_result = dbStuff.runQuery(query=query, select=True)
        if inv_stage_result:
            pass
        else:
            if investor_stage:
                for i_stage in investor_stage:
                    inv_stage_uuid = str(uuid.uuid4())
                    inv_stage_dict = dict(investor_stage_key = inv_stage_uuid,company_permalink=permalink,investor_stage=i_stage.replace("\'","''"))
                    dbStuff.upsert_one(inv_stage_dict,tableName = 'company_investor_stage')
            else:
                pass

        company_permalink = permalink
        query = "select * from company_other_roles where company_permalink = '{val}'".format(val=company_permalink)
        comp_other_result = dbStuff.runQuery(query=query, select=True)
        if comp_other_result:
            pass
        else:
            if company_other_roles:
                for c_other in company_other_roles:
                    comp_other_uuid = str(uuid.uuid4())
                    comp_other_dict = dict(other_roles_key = comp_other_uuid,company_permalink=permalink,other_roles=c_other.replace("\'","''"))
                    dbStuff.upsert_one(comp_other_dict,tableName = 'company_other_roles')
            else:
                pass



        db['Crunchbase_organization_Json_Unique_26Nov'].update_one(filter={'link': col_link},update={'$set': {'phoenix_status': 'inserted'}})
        # print('SUCCESSFULLY COMPLETED')

    except:
        # print('exception')
        print('GOT EXCEPTION', col_link)
        
        db['Crunchbase_organization_Json_Unique_26Nov'].update_one(filter={'link': col_link},update={'$set': {'phoenix_status': 'exception'}})
        db['comp_exceptions'].insert_one(document={'link': col_link})





    # query = 'UPSERT INTO COMPANY_DATA ({column1},{column2}) VALUES {value1,value2} ON DUPLICATE KEY UPDATE {column} = {value}'.format(column1 ='company_facebook',value = facebook,column2 = )
    # dbStuff.singleQuery(query = query)

    # INCLUDE THIS
    # try:
    #     dbinhouse['company_data'].insert_one(
    #         document={'c_key': c_key, 'company_domain_key': company_domain_key, 'company_name': company_name,
    #                   'company_founders': founders_list, 'permalink': permalink, 'domain': domain, 'crunch_logo': logo,
    #                   'company_logo': comp_logo, 'our_logo': our_logo, 'about_company': about_info,
    #                   'company_categories': cat_list, 'state': state, 'city': city, 'country': country,
    #                   'continent': continent, 'company_facebook_link': facebook, 'company_linkedin_link': linkedin,
    #                   'company_twitter_link': twitter, 'company_website': website, 'company_email': contact_email,
    #                   'company_phone_number': phone_number, 'founded_on': founded_on_date,
    #                   'operating_status': operating_status, 'total_employees': total_employees,
    #                   'last_funding_type': last_funding_type, 'funding_stage': funding_stage,
    #                   'ipo_amount_raised_usd': ipo_amount_raised_usd, 'went_public_on': went_public_on_date,
    #                   'stock_symbol': stock_link_label, 'stock_link_value': stock_link_value,
    #                   'ipo_valuation_usd': ipo_valuation_usd,
    #                   'ipo_share_price_usd': ipo_share_price_usd, 'investor_type': investor_type,
    #                   'company_other_roles': company_other_roles, 'sub_organizations': sub_orgs, 'exits': exits_list,
    #                   'company_owned_by': company_owned_by, 'company_type': company_type})
    #     dbinhouse['img_table'].insert_one(
    #         document={'c_key': c_key, 'unique': comp_logo, 'crunch_logo': logo, 'ext': logo_extension,
    #                   'source': 'crunchbase', 'status': 'enable', 'host_provider': 'S3', 'bucket_name': 'comp_logo',
    #                   'bucket_key': c_key, 'company_name': company_name, 'our_logo': our_logo})
    #     db['Crunchbase_organization_Json_Unique_26Nov'].update_one(filter={'link': col_link},
    #                                                                update={'$set': {'db_status': 'inserted'}})
    #
    # except Exception as e:
    #     print('link got Exception', col_link)
    #     dbinhouse['comp_exceptions'].insert_one(document={'link': col_link})
    #     db['Crunchbase_organization_Json_Unique_26Nov'].update_one(filter={'link': col_link},update={'$unset': {'db_status': ''}})




    # count = count + 1
    # print(count)






print("End : %s" % time.ctime())
