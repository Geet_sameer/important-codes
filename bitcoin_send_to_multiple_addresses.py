from cryptos import *
# Have to figure out the transaction size and also the fee based on transaction size
# Transaction fee calculation  ---> size = (no.of.inputs * 148 + no.of.outputs * 34) + 10 
# Then call this api to get the various transaction fees per byte---> https://bitcoinfees.earn.com/api/v1/fees/recommended
# { "fastestFee": 40, "halfHourFee": 20, "hourFee": 10 }
# fee = size * halfHourFee
c = Bitcoin(testnet=True)
priv = 'cVRwZm4S2p6yFvYtas7KaUqGnBUP5gVBrVDVm21idkrddeMRpSEV'
btc_addr = 'muiPT6YFkAALUATXR5b9dLPgh9VBnqUy23'
destination_1 = 'mzCcYe6WzrEX1FpF2a18pqtCJU9zYE4xig'
destination_2 = 'mnAJWx7kX4kAbjiiSVs73kDbzdAF9CapBY'
# print(c.unspent(btc_addr))
amount_to_send_1 = 100000
amount_to_send_2 = 100000 
inputs = c.unspent(btc_addr)
tx_fee = 10000
# Make a list for the multiple addresses and amounts like this
# lis = []
# def make(address,amount):
# 	lis.append(address + ":" + amount)
# 	print(lis)
send = ['mzCcYe6WzrEX1FpF2a18pqtCJU9zYE4xig:100000', 'mnAJWx7kX4kAbjiiSVs73kDbzdAF9CapBY:100000']  # hard coded list
tx = c.preparesignedmultitx(priv,*send,str(tx_fee),change_addr = btc_addr,addr =btc_addr)
transaction_id = c.pushtx(tx)
print(transaction_id)