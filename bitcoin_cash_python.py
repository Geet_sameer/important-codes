from cryptos import *

c = BitcoinCash(testnet=True)

# 1. priv: 89d8d898b95addf569b458fbbd25620e9c9b19c9f730d5d60102abbabcb72678  pub: mwJUQbdhamwemrsR17oy7z9upFh4JtNxm1
# 2. priv: 107e9aaba95b1bcba732cb269588fb8762ba4f6018e0526818832bbe139b1317  pub: msjK2Az65fzqzUDA6yB6QeNi42ssUAxG93 13 btc
priv = '89d8d898b95addf569b458fbbd25620e9c9b19c9f730d5d60102abbabcb72678'
my_addr = 'mwJUQbdhamwemrsR17oy7z9upFh4JtNxm1'
destination = 'msjK2Az65fzqzUDA6yB6QeNi42ssUAxG93'
amount_to_send = 1000000
inputs = c.unspent('mwJUQbdhamwemrsR17oy7z9upFh4JtNxm1')
input_list = []
total_unspent_value = 0
tx_fee = 10000
tx = c.preparesignedtx(privkey=priv,to=destination,value=amount_to_send,change_addr=my_addr,addr=my_addr)
# print(tx)
transaction_id = c.pushtx(tx)
print(transaction_id)

