from warcreader import WarcFile
from gzip import GzipFile
import re
# from boilerpipe.extract import Extractor
# import usaddress
# from geotext import GeoText
import unicodedata
# import nltk
# from nltk.corpus import stopwords
# from nltk import word_tokenize, pos_tag, ne_chunk
import time
# import numpy as np
import multiprocessing
from multiprocessing import Pool
from multiprocessing import Process
import glob, os
import phoenixdb


def store_in_S3(filename):
	print('coming into store_in_S3 method')
	from boto.s3.connection import S3Connection
	conn = S3Connection('AKIAJ2WSZQIHD7BQZKLA', 'qDyZ4451CEF1LlkcCPlyRfIU/Wp2K/fXYCTFu/ZV')
	# print(conn)

	# bucket = conn.create_bucket('trioncube_test_for_loop')
	# # print(bucket)
	# # bucket = 'trioncube1'
	# for i in range(1,100):
	# 	from boto.s3.key import Key
	# 	k = Key(bucket)
	# 	k.key = 'outbeast1'
	# 	k.set_contents_from_string(str(i))



	# import boto
	# # c = boto.connect_s3()
	# b = conn.get_bucket('trioncube1') # substitute your bucket name here
	# from boto.s3.key import Key
	# k = Key(b)
	# k.key = '4802.txt'
	# print(k.get_contents_as_string())


	import math, os
	import boto
	from filechunkio import FileChunkIO

	# Connect to S3
	# c = boto.connect_s3()
	b = conn.get_bucket('trioncube1')

	# Get file info
	source_path = str(filename)
	source_size = os.stat(source_path).st_size
	print(source_size)

	# Create a multipart upload request
	mp = b.initiate_multipart_upload(os.path.basename(source_path))

	# Use a chunk size of 50 MiB (feel free to change this)
	chunk_size = 52428800
	chunk_count = int(math.ceil(source_size / float(chunk_size)))

	# Send the file parts, using FileChunkIO to create a file-like object
	# that points to a certain byte range within the original file. We
	# set bytes to never exceed the original file size.
	for i in range(chunk_count):
		offset = chunk_size * i
		bytes = min(chunk_size, source_size - offset)
		with FileChunkIO(source_path, 'r', offset=offset,bytes=bytes) as fp:
			mp.upload_part_from_file(fp, part_num=i + 1)

	# Finish the upload
	print(mp.complete_upload())


def Extraction(filename):

	# store the filenames in table...before starting check the filename is present in the table or not...
	# if the filename is present in the table then escape otherwise continue the process

	# status = checkDB(filename)
	# if status:
		# insert(filename)
	warc_gzip = GzipFile(filename, 'rb')
	warc_file = WarcFile(warc_gzip)
	count = 0
	# Data = {}
	Email_collection = {}
	Phone_collection = {}
	Social_collection = {}
	facebook = {}
	twitter = {}
	linkedin = {}
	foursquare = {}
	github = {}
	Instagram = {}
	Total_Data = {}
	process_id = os.getpid()
	# print("Number of Processes running\n")
	print("process id", os.getpid())
	file = str(filename) + '.txt'
	f = open(file, 'a')

	for webpage in warc_file:
	    body = webpage.payload          #ENTIRE HTML DATA


	    # print(type(body))
	    # #-----------------------_Email and Domain -------------------------------#    '''Working'''
	    email = re.findall(r'([\w\-\.]+@(\w[\w\-]+\.[\w\-]+))+',body)
	    Email_collection['email_Dom'] = email 
	   
	    #-----------------------    END   ----------------------------------------#
	    
	    #------------------------ URI -------------------------------------------#       '''Working'''
	    # print("uri",webpage.uri)
	    uri = str(webpage.uri).decode('unicode_escape').encode('ascii','ignore')      
	    # # # print(uri)
	    Email_collection['SourceUrl'] = uri
	    # Phone_collection['SourceUrl'] = webpage.uri
	    Social_collection['SourceUrl'] = webpage.uri
	    #--------------------------- END -----------------------------------#

	    #----------------------------PHONE_NUMBER----------------------------------------#


	    #Matches the following
	 	# 123-456-7890
		# (123) 456-7890
		# 123 456 7890
		# 123.456.7890
		# +91 (123) 456-7890
		# 555.123.4565
		# +1-(800)-545-2468
		# 2-(800)-545-2468
		# 3-800-545-2468
		# 555-123-3456
		# 555 222 3342
		# (234) 234 2442
		# (243)-234-2342
		# 1234567890
		# 123.456.7890
		# 123.4567
		# 123-4567
		# 1234567900
		# 12345678900
		# print(phone)
	    # phone_regex = re.findall(r'\+[-\(\s\d]+?(?=\s*[+<])', body)
	    # phone_regex = re.findall(r'([\+\(]?\d+[\s\-\.]\(?\d{3}\)?[\s\-\.]\d{3}[\s\-\.]?\d{4})', body)
	     
	    
	    # Phone_collection['phone'] = phone_regex
	    # print(phone_regex)
	    # print(len(phone_regex))
	    
	    # Phoone_collection['Phone'] = phone_regex

	    # print("phone ", phone_regex)
	    #---------------------------  TITLE --------------------------------------#         '''Working'''
	    Title = re.findall(r'<title>(.*?)</title>',body)
	    Title = str(Title).decode('unicode_escape').encode('ascii','ignore')
	    Email_collection['Title'] = Title
	    # Phone_collection['Title'] = Title
	    Social_collection['Title'] = Title
	    # print("Title is",Title)
	    #---------------------------- END ----------------------------------------#
			
		#----------------------------- Socail_Data --------------------------------------------------------#	 
		
		
	    Facebook_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?facebook.(?:com|in)\/([^v2.3,sharer.php,\d+,pages,plugins,share,tr,badges,feeds][A-z 0-9 \. ^=:]+))+',body)
	    # Total_Data.append(Facebook_regex)
	    facebook['facebook_link'] = Facebook_regex
	    twitter_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?twitter.(?:com|in)\/([^share,intent,javascripts][^widgets.js][A-z 0-9 \/ \. ^=:]+))+', body)
	    # Total_Data.append(twitter_regex)
	    twitter['twitter_link'] = twitter_regex
	    linkedin_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?linkedin.(?:com|in)\/([^shareArticle][A-z 0-9 \/ \. ^=:]+))+',body)
	    # Total_Data.append(linkedin_regex)
	    linkedin['linkedin_link'] = linkedin_regex
	    foursquare_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?foursquare.(?:com|in)\/([A-z 0-9 \/ \. ^=:]+))+',body)
	    # Total_Data.append(foursquare_regex)
	    foursquare['foursquare_link'] = foursquare_regex
	    github_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?github.(?:com|in)\/([A-z 0-9 \/ \. ^=:]+))+',body)
	    # Total_Data.append(github_regex)
	    github['github_link'] = github_regex
	    Instagram_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?instagram.(?:com|in)\/([A-z 0-9 \/ \. ^=:]+))+',body)
	    # Total_Data.append(Instagram_regex)
	    Instagram['Insta_link'] = Instagram_regex

	    Social_collection['Facebook'] = Facebook_regex
	    Social_collection['Twitter'] = twitter_regex
	    Social_collection['linkedin'] = linkedin_regex
	    Social_collection['foursquare'] = foursquare_regex
	    Social_collection['Github'] = github_regex
	    Social_collection['Instagram'] = Instagram_regex
	    # print("Fb", len(Facebook_regex))
	    # print("Fb", Facebook_regex)
	    #---------------------------- END ----------------------------------------#

	    #Using this try-catch for extraction of phone numbers

	    # try:
	    # 	data = Text_from_HTML(body)
	    # 	# print(data)
	    # 	phone_regex = re.findall(r'([\+\(]?\d+?[\s\-\.]?\(?\d+\)?[\s\-\.]\d{3}[\s\-\.]?\d{4})', data)
	    # 	# print(phone_regex)
	    # 	Phone_collection['Phone_Number'] = phone_regex
	    # 	print(Phone_collection)
	    # 	# Get_Data = Extract_Address(data)
	    # 	# pure_data = str(Get_Data).decode('unicode_escape').encode('ascii','ignore')
	    # 	# print("Address is ", pure_data)
	    # 	# print(type(pure_data))
	    # except Exception:
	    # 	# print("came into exception")
	    # 	continue

	    count = count + 1
	    # Email_collection['count'] = count
	    print("count",count)

	    Total_Data['Email'] = Email_collection
	    Total_Data['Social'] = Social_collection
	    # Total_Data['Phone'] = Phone_collection
	    
	    # print(Email_collection)
	    f.write(str(Total_Data) + '\n')
	    
	    # print(Social_collection)
	    print(Total_Data)
	store_in_S3(file)
	return


if __name__ == "__main__":
	t = time.time()

	
	lis = []
	for file in glob.glob("*.gz"):
		lis.append(file)
	Num_of_workers = len(lis)
	print("Total Number of running workers are " , format(Num_of_workers))
	
	# Extraction('index.warc.gz')
	# first = ['index.warc.gz']
	p = Pool(processes =Num_of_workers)
	p.map(Extraction,lis)
	p.close()
	p.join()
	# os.chdir("/opt/datac/cw")
	
	# lis = []
	# for file in glob.glob("*.gz"):
	# 	lis.append(file)
	# first = lis[0]
	# second = lis[1]
	# third = lis[2]
	# fourth = lis[3]
	# # # print(first)
	# # print(second)
	# p1 = Process(target = Extraction, args=(first,))
	# p2 = Process(target = Extraction, args=(second,))
	# p3 = Process(target = Extraction, args=(third,))
	# p4 = Process(target = Extraction, args=(fourth,))

	# p1.start()
	# p2.start()
	# p3.start()
	# p4.start()

	# p1.join()
	# p2.join()
	# p3.join()
	# p4.join()

	

	print("Time taken for all the Workers: ", time.time() - t)
	
	#extract_names(string)
