# from .methods import *
# from .send_data import *
# from .update_data import *
import time
import tldextract as ext
from .database import *
import uuid
import  datetime



class CompanyHbase:
    def __init__(self):
        super (CompanyHbase, self).__init__ ()
        # self.connection = Connection('http://207.154.207.236:8529', username='root', password='XrXT4uBknPZapu')
        # self.database = self.connection['_system']
        # self.common = Common()
        # self.objupdate = update_data()
        # self.dataToSEO = {}
        self.phoenixWrite = PhoenixWrite ()
        return

    def generateCompanyKey(self, website):
        return ''.join (str (ord (c.lower ())) for c in website)

    def companyData(self, data, nData, mode, companyKey, dataSource):

        companyData = {}
        dbObj = PhoenixWrite()

        if mode == "update":
            companyData[ 'companyKey' ] = companyKey
            print(companyData.get('companyKey'))

            companyData[ 'companyName' ] = nData.get ('organization').get ('name')

            if not data.get ('companyName'.upper()) and nData.get ('organization'):
                companyData[ 'companyName' ] = nData.get ('organization').get ('name')

            # companyData[ 'approx-employees' ] = data.get ('approxEmployees')
            # if not data.get ('approx-employees') and nData.get ('organization'):
            #     companyData[ 'approx-employees' ] = nData.get ('organization').get ('approxEmployees')

            # companyData[ 'founded' ] = data.get ('founded')
            # if not data.get ('founded') and nData.get ('organization'):
            #     companyData[ 'founded' ] = nData.get ('organization').get ('founded')
            #
            # companyData[ 'website' ] = data.get ('website', None)
            # if not data.get ('website'):
            #     companyData[ 'domain' ] = nData.get ('website')

            companyData[ 'logoUrl' ] = data.get ('logoUrl'.upper())
            if not data.get ('logoUrl'.upper()):
                companyData[ 'logoUrl' ] = nData.get ('logo')

            companyData[ 'aboutNote' ] = data.get ('aboutNote'.upper()).replace("\'", '')
            if not data.get ('aboutNote'.upper()) and nData.get ('organization'):
                companyData[ 'aboutNote' ] = nData.get ('organization').get ('overview').replace("\'", '')

            # companyData[ 'eMail' ] = data.get ('eMail')
            # if not data.get ('eMail') and nData.get ('organization').get ('contactInfo'):
            #     companyData[ 'email' ] = nData.get ('organization').get ('contactInfo').get ('emailAddresses')
            if bool(nData.get ('organization').get ('contactInfo')):
                # companyData['eMail'] = data.get('organization').get('contactInfo').get('emailAddresses')
                if nData.get('organization').get ('contactInfo').get('emailAddresses'):
                    # companyData[ 'eMail' ] = str (uuid.uuid3 (uuid.NAMESPACE_X500, str (data.get ('companyDataKey'))))
                    emails = nData.get('organization').get ('contactInfo').get ('emailAddresses')
                    for i in emails:

                        if i.get ('value'):
                            emailRow = {
                                'companyKey': str(uuid.uuid3(uuid.NAMESPACE_X500, companyKey)),
                                'uuid': str(uuid.uuid3(uuid.NAMESPACE_X500, str(i.get('value')))),
                                'email': i.get('value'), 'domain': i.get('value').split('@')[-1],
                                'email_extension': ''.join(
                                    i.get('email').split('@')[-1].split('.')[1:]),'type': dbObj.getEmailType(i.get('value')) }
                            # emailRow[ 'type' ] = self.publicOrPrivate (i.get ('email').split ('@')[ -1 ])
                            self.phoenixWrite.upsert_one (columnsValuesDict=emailRow, tableName='emailCompany')
                            # TODO add publicOrPrivate function
                            # TODO upsert emailRow


            # companyData[ 'Phone' ] = data.get ('Phone')
            # if not data.get ('Phone') and nData.get ('organization').get ('contactInfo'):
            #     companyData[ 'Phone' ] = nData.get ('organization').get ('contactInfo').get ('phoneNumbers')
            #
            # companyData[ 'Address' ] = data.get ('Address')
            # if not data.get ('Address') and nData.get ('organization').get ('contactInfo'):
            #     companyData[ 'Address' ] = nData.get ('organization').get ('contactInfo').get ('addresses')

            # companyData['Phone'] = data.get('organization').get('contactInfo').get('phoneNumbers')
            if nData.get ('organization').get ('contactInfo').get ('phoneNumbers'):
                # companyData[ 'Phone' ] = str (uuid.uuid3 (uuid.NAMESPACE_X500, str (data.get ('companyDataKey'))))
                phones = nData.get ('organization').get ('contactInfo').get ('phoneNumbers')
                for i in phones:
                    phone = {'phone': i.get ('number'), 'phone_extension': i.get ('phoneExtension'),
                             'companyKey': str (
                                 uuid.uuid3 (uuid.NAMESPACE_X500, companyKey)),
                             'uuid': str (
                                 uuid.uuid3 (uuid.NAMESPACE_X500, str (i.get('number'))))}
                    self.phoenixWrite.upsert_one(columnsValuesDict=phone, tableName='phoneCompany')
                    # TODO upsert phone
            # companyData['Address'] = data.get('organization').get('contactInfo').get('addresses')
            if nData.get ('organization').get ('contactInfo').get ('addresses'):
                # companyData[ 'Address' ] = str (uuid.uuid3 (uuid.NAMESPACE_X500, str (data.get ('companyDataKey'))))
                addresses = nData.get ('organization').get ('contactInfo').get ('addresses')
                for i in addresses:
                    address = {}
                    address[ 'street' ] = i.get ('addressLine1')
                    address[ 'city' ] = i.get ('locality')
                    if i.get ('region'):
                        address[ 'state' ] = i.get ('region').get ('name')
                    if i.get ('country'):
                        address[ 'country' ] = i.get ('country').get ('name')
                    address[ 'zipcode' ] = i.get ('postal-code')
                    address[ 'companyKey' ] = str (
                        uuid.uuid3 (uuid.NAMESPACE_X500, companyKey))
                    if i.get('region') and address.get('state') and i.get('region').get('name'):
                        address['uuid'] = str(
                            uuid.uuid3(uuid.NAMESPACE_X500, str(i.get('region').get('name') + address.get('state'))))
                    self.phoenixWrite.upsert_one(columnsValuesDict=address, tableName='addressCompany')
                    # TODO upsert address
                    # companyData['baseLocation'] = data.get('organization').get('contactInfo').get('baseLocation')
                    # companyData[ 'baseLocation' ] = str (
                    # uuid.uuid3 (uuid.NAMESPACE_X500, str (data.get ('companyDataKey'))))

            # companyData[ 'dataSource' ] = dataSource
            #
            # companyData[ 'baseLocation' ] = data.get ('baseLocation')
            # if not data.get ('baseLocation') and nData.get ('contactInfo'):
            #     companyData[ 'baseLocation' ] = nData.get ('contactInfo').get ('baseLocation')

            companyData[ 'googleBusinessID' ] = data.get ('googleBusinessID'.upper()) if data.get (
                'googleBusinessID'.upper()) else nData.get ('googleBusinessID')

            companyData[ 'bingBusinessID' ] = data.get ('bingBusinessID'.upper()) if data.get (
                'bingBusinessID'.upper()) else nData.get (
                'bingBusinessID')

            if data.get ('companyType'.upper()):
                companyData[ 'companyType' ] = data.get ('companyType'.upper())

            elif nData.get ('companyType'):
                companyData[ 'companyType' ] = nData.get ('companyType')

            companyData[ 'fortune500' ] = data.get ('fortune500'.upper())
            if not data.get ('fortune500'.upper()):
                companyData[ 'fortune500' ] = nData.get ('fortune500')

            companyData[ 'fortune5000' ] = data.get ('fortune5000'.upper())
            if not data.get ('fortune5000'.upper()):
                companyData[ 'fortune5000' ] = nData.get ('fortune5000')

            companyData[ 'inc5000' ] = data.get ('inc5000'.upper())
            if not data.get ('inc5000'.upper()):
                companyData[ 'inc5000' ] = nData.get ('inc5000')

            companyData[ 'inc2000' ] = data.get ('inc2000'.upper())
            if not data.get ('inc2000'.upper()):
                companyData[ 'inc2000' ] = nData.get ('inc2000')

            companyData[ 'mainCategory' ] = data.get ('mainCategory'.upper())
            if not data.get ('mainCategory'.upper()):
                companyData[ 'mainCategory' ] = nData.get ('mainCategory')

            companyData[ 'subCategory' ] = data.get ('subCategory'.upper())
            if not data.get ('subCategory'.upper()):
                companyData[ 'subCategory' ] = nData.get ('subCategory')



        else:

            if data.get ('website'):
                # companyData[ 'domain' ] = data.get ('website')
                companyData[ 'companyKey' ] = companyKey

            # dataOrg = data.get('organization')

            if data.get ('organization'):
                companyData[ 'companyName' ] = nData.get ('organization').get ('name')

                # companyData[ 'approx_employees' ] = data.get ('organization').get ('approxEmployees')

                # companyData[ 'founded' ] = data.get ('organization').get ('founded')
                if data.get('organization').get('overview'):
                    companyData[ 'aboutNote' ] = data.get ('organization').get ('overview').replace("\'", '')

                companyData[ 'googleBusinessID' ] = nData.get ('googleBusinessID') if data.get (
                    'googleBusinessID') else nData.get ("organization").get ('googleBusinessID')

                companyData[ 'bingBusinessID' ] = data.get ('bingBusinessID') if nData.get (
                    'bingBusinessID') else data.get (
                    'organization').get ('bingBusinessID')

            #
            #  dataContact = dataOrg.get('contactInfo')
            # companyData[ 'dataSource' ] = dataSource

            if bool(data.get ('organization').get ('contactInfo')):
                # companyData['eMail'] = data.get('organization').get('contactInfo').get('emailAddresses')
                if data.get('organization').get ('contactInfo').get ('emailAddresses'):
                    # companyData[ 'eMail' ] = str (uuid.uuid3 (uuid.NAMESPACE_X500, str (data.get ('companyDataKey'))))
                    emails = data.get('organization').get ('contactInfo').get ('emailAddresses')
                    for i in emails:

                        if i.get ('value'):
                            emailRow = {'companyKey': str(uuid.uuid3(uuid.NAMESPACE_X500, companyKey)),
                                        'uuid': str(uuid.uuid3(uuid.NAMESPACE_X500, str(i.get('value')))),
                                        'email': i.get('value'),
                                        'domain': i.get('value').split('@')[-1],
                                        'email_extension': ''.join(i.get('value').split('@')[-1].split('.')[1:]),
                                        'type': dbObj.getEmailType(i.get('value'))}
                            # emailRow[ 'type' ] = self.publicOrPrivate (i.get ('email').split ('@')[ -1 ])
                            self.phoenixWrite.upsert_one (columnsValuesDict=emailRow, tableName='emailCompany')
                            # TODO add publicOrPrivate function
                            # TODO upsert emailRow

                # companyData['Phone'] = data.get('organization').get('contactInfo').get('phoneNumbers')
                if data.get ('organization').get ('contactInfo').get ('phoneNumbers'):
                    # companyData[ 'Phone' ] = str (uuid.uuid3 (uuid.NAMESPACE_X500, str (data.get ('companyDataKey'))))
                    phones = data.get ('organization').get ('contactInfo').get ('phoneNumbers')
                    for i in phones:
                        phone = {'phone': i.get ('number'), 'phone_extension': i.get ('phoneExtension'),
                                 'companyKey': str (
                                     uuid.uuid3 (uuid.NAMESPACE_X500, companyKey)),
                                 'uuid': str (
                                 uuid.uuid3 (uuid.NAMESPACE_X500, str (i.get('number'))))}
                        self.phoenixWrite.upsert_one(columnsValuesDict=phone, tableName='phoneCompany')
                        # TODO upsert phone
                # companyData['Address'] = data.get('organization').get('contactInfo').get('addresses')
                if data.get ('organization').get ('contactInfo').get ('addresses'):
                    # companyData[ 'Address' ] = str (uuid.uuid3 (uuid.NAMESPACE_X500, str (data.get ('companyDataKey'))))
                    addresses = data.get ('organization').get ('contactInfo').get ('addresses')
                    for i in addresses:
                        address = {}
                        address[ 'street' ] = i.get ('addressLine1')
                        address[ 'city' ] = i.get ('locality')
                        if i.get ('region'):
                            address[ 'state' ] = i.get ('region').get ('name')
                        if i.get ('country'):
                            address[ 'country' ] = i.get ('country').get ('name')
                        address[ 'zipcode' ] = i.get ('postal-code')
                        address[ 'companyKey' ] = str (
                            uuid.uuid3 (uuid.NAMESPACE_X500, companyKey))
                        if i.get('region') and address.get('state') and i.get('region').get('name'):
                            address[ 'uuid' ] = str (
                            uuid.uuid3 (uuid.NAMESPACE_X500, str (i.get('region').get('name')+address.get('state'))))
                        self.phoenixWrite.upsert_one(columnsValuesDict=address, tableName='addressCompany')
                        # TODO upsert address
                        # companyData['baseLocation'] = data.get('organization').get('contactInfo').get('baseLocation')
                        # companyData[ 'baseLocation' ] = str (
                        # uuid.uuid3 (uuid.NAMESPACE_X500, str (data.get ('companyDataKey'))))

            if data.get ('companyType'):
                companyData[ 'companyType' ] = data.get ('companyType')

            elif data.get ('organization').get ('companyType'):
                companyData[ 'companyType' ] = data.get ('companyType')

            companyData[ 'fortune500' ] = data.get ('fortune500')

            companyData[ 'fortune5000' ] = data.get ('fortune5000')

            companyData[ 'inc5000' ] = data.get ('inc5000')

            companyData[ 'inc2000' ] = data.get ('inc2000')

            companyData[ 'mainCategory' ] = data.get ('mainCategory')

            companyData[ 'subCategory' ] = data.get ('subCategory')

            # companyData['blog'] = [a.get('label') for a in data.get('links') if data.get('links') is not None]
            companyData[ 'logoUrl' ] = data.get ('logo')
        #
        # if mode == "update":
        #
        #     companyData[ 'SEOData' ] = data.get ('SEOData')
        #
        #     companyData[ 'SEODataForPeopleList' ] = data.get ('SEODataForPeopleList')
        #
        # else:
        #     # companyData[ 'SEOData' ] = 0
        #
        #     #         companyData[ 'SEODataForPeopleList' ] = 0
        #     pass

        return companyData

    def revenueData(self, data, mode, nData, companyKey, dataSource):

        revenueData = {}

        if mode == 'update':

            revenueData[ 'companyKey' ] = data.get ('companyKey'.upper())
            # revenueData[ 'sourceKey' ] = data.get ('sourceKey')
            # revenueData[ 'addedTime' ] = data.get ('addedTime')
            #
            # revenueData[ 'addedBy' ] = data.get ('addedBy')


        else:

            revenueData[ 'companyKey' ] = companyKey
            # revenueData[ "sourceKey" ] = str (uuid.uuid3 (uuid.NAMESPACE_X500, str (companyKey)))
            f = '%Y-%m-%d %H:%M:%S'
            revenueData[ 'addedTime' ] = str (datetime.datetime.now().strftime(f))

            revenueData[ 'addedBy' ] = dataSource

        revenueData[ 'FY' ] = data.get ('FY')
        if not data.get ('FY'):
            revenueData[ 'FY' ] = nData.get ('FY')

        # revenueData[ 'companyKey' ] = companyKey

        revenueData[ 'amount' ] = data.get ('amount'.upper())
        if not data.get ('amount'):
            revenueData[ 'amount' ] = nData.get ('amount')

        revenueData[ 'currentCompany' ] = data.get ('currentCompany'.upper())
        if not data.get ('currentCompany'):
            revenueData[ 'currentCompany' ] = nData.get ('currentCompany'.upper())

        revenueData[ 'revenueImprovement' ] = data.get ('revenueImprovement'.upper())
        if not data.get ('revenueImprovement'):
            revenueData[ 'revenueImprovement' ] = nData.get ('revenueImprovement'.upper())

        return revenueData

    def fundingData(self, data, nData, mode, companyKey, dataSource):

        fundingData = {}
        f = '%Y-%m-%d %H:%M:%S'

        if mode == 'update':

            fundingData[ 'companyKey' ] = data.get ('companyKey'.upper())
            fundingData[ 'addedTime' ] = str(data.get ('addedTime'.upper()))
            fundingData[ 'addedBy' ] = data.get ('addedBy'.upper())



        else:
            fundingData[ 'companyKey' ] = companyKey
            fundingData[ 'addedTime' ] = str (datetime.datetime.now().strftime(f))

            fundingData[ 'addedBy' ] = dataSource

        fundingData[ 'gotTime' ] = data.get ('gotTime'.upper())
        if not data.get ('gotTime'.upper()):
            fundingData[ 'gotTime' ] = nData.get ('gotTime')

        # fundingData[ 'companyKey' ] = companyKey

        fundingData[ 'fundingstage' ] = data.get ('fundingstage'.upper())
        if not data.get ('fundingstage'):
            fundingData[ 'fundingstage' ] = nData.get ('fundingstage')

        fundingData[ 'leadFunder' ] = data.get ('leadFunder'.upper())
        if not data.get ('leadFunder'.upper()):
            fundingData[ 'leadFunder' ] = nData.get ('leadFunder')

        fundingData[ 'amount' ] = data.get ('amount'.upper())
        if not data.get ('amount'.upper()):
            fundingData[ 'amount' ] = nData.get ('amount')

        fundingData[ 'valuation' ] = data.get ('valuation'.upper())
        if not data.get ('valuation'.upper()):
            fundingData[ 'valuation' ] = nData.get ('valuation')

        fundingData[ 'Industry' ] = data.get ('Industry'.upper())
        if not data.get ('Industry'.upper()):
            fundingData[ 'Industry' ] = nData.get ('Industry')

        return fundingData

    def fundedByData(self, data, nData, mode, companyKey, dataSource):

        # ['companyKey', 'fundKey', 'companykey', 'peopleKey', 'positionKey',
        #  'addedTime', 'addedBy'])
        fundedByData = {}
        f = '%Y-%m-%d %H:%M:%S'

        if mode == 'update':

            fundedByData[ 'companyKey' ] = data.get ('companyKey'.upper())
            fundedByData[ 'positionKey' ] = data.get ('positionKey'.upper())

            fundedByData[ 'peopleKey' ] = data.get ('peopleKey'.upper())

            fundedByData[ 'addedTime' ] = str(data.get ('addedTime'.upper()))

            fundedByData[ 'addedBy' ] = data.get ('addedBy'.upper())

        else:
            fundedByData[ 'companyKey' ] = companyKey
            fundedByData[ 'addedTime' ] = str (datetime.datetime.now().strftime(f))

            fundedByData[ 'addedBy' ] = dataSource

        # fundedByData[ 'companyKey' ] = companyKey

        return fundedByData

    def sourceAndOther(self, data, nData, mode, companyKey):

        # ['companyKey', 'type', 'typeName', 'dataType', 'companyKey',
        #  'officeNumber', 'socialProfiles']
        f = '%Y-%m-%d %H:%M:%S'

        socialData = {}
        allSocial = [ "facebook", "twitter", "instagram", "foursquare", "github",
                      "pinterest", "angellist", "gravatar", "vimeo", "aboutme",
                      "flickr", "klout", "googleplus" ]

        if mode == 'update':

            socialData[ 'socialID' ] = data.get ('socialID'.upper())

        else:
            socialData[ 'socialID' ] = str (uuid.uuid3 (uuid.NAMESPACE_X500, str (companyKey)))

        socialData[ 'companyKey' ] = companyKey

        socialDataAllList = data.get ('socialProfiles'.upper())
        if not data.get ('socialProfiles'.upper()):
            socialDataAllList = nData.get('socialProfiles')

        # socialUUID = str(uuid.uuid4 ())
        for i in socialDataAllList:
            for j in allSocial:
                if i.get ('typeName').lower () == j:
                    socialData[ j.lower () ] = j.lower () + "-" + str(companyKey)
                    tableName = j
                    tableDict = {}
                    tableDict[ 'snsUUID' ] = socialData[ j.lower () ]  # j+"-"+str(socialUUID)
                    # tableDict[ 'uuid' ] = socialUUID
                    tableDict[ 'snsid' ] = i.get ('id')
                    tableDict[ 'typeId' ] = i.get ('typeId')
                    tableDict[ 'typeName' ] = i.get ('typeName')
                    tableDict[ 'url' ] = i.get ('url')
                    tableDict[ 'username' ] = i.get ('username')
                    if i.get('bio'):
                        tableDict[ 'bio' ] = i.get ('bio').replace('\r\n', '').replace('\'','')
                    else:
                        tableDict['bio'] = None
                    print(i.get('typeId'))
                    # tableDict[ 'followers' ] = i.get ('followers')
                    self.phoenixWrite.upsert_one(columnsValuesDict=tableDict, tableName=tableName.lower())
                    # TODO upsert tableDict -> corresponding table
        return socialData

    def soaTypeData(self, data, nData, mode, companyKey):

        soaType = {}
        f = '%Y-%m-%d %H:%M:%S'

        if mode == 'update':

            soaType[ 'companyKey' ] = data.get ('companyKey'.upper())

        else:
            soaType[ 'companyKey' ] = companyKey

        # ['companyKey', 'type', 'name', 'urlpattern', 'iconUrl'])

        soaType[ 'type' ] = data.get ('type'.upper())
        if not data.get ('type'.upper()):
            soaType[ 'type' ] = nData.get ('type')

        soaType[ 'name' ] = data.get ('name'.upper())
        if not data.get ('type'.upper()):
            soaType[ 'name' ] = nData.get ('name')

            # soaType[ 'urlpattern' ] = data.get ('urlpattern')
            # if not data.get ('urlpattern'):
            # soaType[ 'urlpattern' ] = self.extractFullDomain (nData.get ('website'))

        soaType[ 'iconUrl' ] = data.get ('iconUrl'.upper())
        if not data.get ('iconUrl'.upper()):
            soaType[ 'iconUrl' ] = nData.get ('logo')

        return soaType

    def product(self, data, nData, companyKey, mode, dataSource):

        product = {}
        f = '%Y-%m-%d %H:%M:%S'

        if mode == 'update':

            product[ 'companyKey' ] = data.get ('companyKey'.upper())
            dataProd = data
            ndataProd = nData
            product[ 'addedBy' ] = dataProd.get ('addedBy'.upper())

            product[ 'addedTime' ] = str(dataProd.get ('addedTime'.upper()))

        else:
            product[ 'companyKey' ] = companyKey
            dataProd = data.get ("products")

            if dataProd is None:
                dataProd = data
                ndataProd = nData

            product[ 'addedBy' ] = dataSource

            product[ 'addedTime' ] = str (datetime.datetime.now().strftime(f))

        # ['companyKey', 'productName', 'addedBy', 'addedTime', 'companyKey',
        #  'domain', 'iconUrl', 'oneLinerNote', 'aboutNote', 'blog',
        #  'blogUrl', 'mainCategoryKey', 'subCategoryKey'])

        # product[ 'companyKey' ] = companyKey

        product[ 'productName' ] = dataProd.get ('name'.upper())
        if not dataProd.get ('name'.upper()):
            product[ 'productName' ] = ndataProd.get ('productName')

        product[ 'domain' ] = dataProd.get ('domain'.upper())
        if not dataProd.get ('website'.upper()):
            product[ 'domain' ] = ndataProd.get ('website')

        product[ 'iconUrl' ] = dataProd.get ('iconUrl'.upper())
        if not dataProd.get ('iconUrl'.upper()):
            product[ 'iconUrl' ] = ndataProd.get ('icon')

        product[ 'oneLinerNote' ] = dataProd.get ('oneLinerNote'.upper())
        if not dataProd.get ('oneLinerNote'.upper()):
            product[ 'oneLinerNote' ] = ndataProd.get ('oneLinerNote')

        product[ 'aboutNote' ] = dataProd.get ('aboutNote'.upper())
        if not dataProd.get ('aboutNote'.upper()):
            product[ 'aboutNote' ] = ndataProd.get ('aboutNote')

        # product[ 'blog' ] = dataProd.get ('blog')
        # if not dataProd.get ('blog'):
        #     product[ 'blog' ] = ndataProd.get ('blog')

        product[ 'blogUrl' ] = dataProd.get ('blogUrl'.upper())
        if not dataProd.get ('blogUrl'.upper()):
            product[ 'blogUrl' ] = ndataProd.get ('blogUrl')

        product[ 'mainCategoryKey' ] = dataProd.get ('mainCategoryKey'.upper())
        if not dataProd.get ('mainCategoryKey'.upper()):
            product[ 'mainCategoryKey' ] = ndataProd.get ('mainCategoryKey')

        product[ 'subCategoryKey' ] = dataProd.get ('subCategoryKey'.upper())
        if not dataProd.get ('subCategoryKey'.upper()):
            product[ 'subCategoryKey' ] = ndataProd.get ('subCategoryKey')

        return product

    def domain(self, data, nData, companyKey, mode, dataSource):
        domain = {}
        f = '%Y-%m-%d %H:%M:%S'

        if mode == 'update':
            domain[ 'companyKey' ] = companyKey
            domain[ 'domain' ] = nData.get ('website')
            domain[ 'extension' ] = ext.extract (str (data.get ('website'.upper()))).suffix
            if not data.get ('extension'.upper()):
                domain[ 'extension' ] = ext.extract (str (nData.get ('website'.upper()))).suffix
            domain[ 'addedTime' ] = str(data.get ('addedTime'.upper()))
            # domain[ 'companyKey' ] = companyKey
            # domain[ 'dataSource' ] = dataSource
            domain[ 'webmail' ] = data.get ('webmail'.upper())
            if not data.get ('webmail'.upper()):
                domain[ 'webmail' ] = nData.get ('webmail')
            domain[ 'verifiable' ] = data.get ('verifiable'.upper())
            if not data.get ('verifiable'.upper()):
                domain[ 'verifiable' ] = nData.get ('verifiable')
        else:
            domain[ 'companyKey' ] = companyKey
            domain[ 'domain' ] = nData.get ('website')
            domain[ 'extension' ] = ext.extract (str (nData.get ('website'))).suffix
            domain[ 'addedTime' ] = str (datetime.datetime.now().strftime(f))
            # domain[ 'companyKey' ] = companyKey
            # domain[ 'dataSource' ] = dataSource
            domain[ 'webmail' ] = nData.get ('webmail')
            domain[ 'verifiable' ] = nData.get ('verifiable')

        return domain

    def dataSourceData(self, data, companyKey, mode, dataSource1):

        dataSource = {}

        if mode == 'update':

            dataSource[ 'dataSourceKey' ] = companyKey
            dataSource[ 'sourceName' ] = data.get ('sourceName')


        else:
            dataSource[ 'dataSourceKey' ] = companyKey
            dataSource[ 'sourceName' ] = dataSource1

        return dataSource

    def dataSourceStat(self, data, companyKey, mode):

        # ['companyKey', 'dailyKey', 'dataSourceKey', 'count', 'lastUpdated',
        #  'day', 'month', 'year']
        f = '%Y-%m-%d %H:%M:%S'

        dStat = {}

        if mode == 'update':

            dStat[ 'companyKey' ] = companyKey
            dStat[ 'count' ] = data.get ('count'.upper()) + 1
            dStat[ 'dataSourceKey' ] = str (uuid.uuid3 (uuid.NAMESPACE_X500, str (companyKey)))

        else:
            dStat[ 'companyKey' ] = companyKey
            dStat[ 'count' ] = 1
            dStat[ 'dataSourceKey' ] = str (uuid.uuid3 (uuid.NAMESPACE_X500, str (companyKey)))


            # ['companyKey', 'dailyKey', 'dataSourceKey', 'count', 'lastUpdated',
            # 'day', 'month', 'year']

        dStat[ 'lastUpdated' ] = str (datetime.datetime.now().strftime(f))

        t = time.strftime ('%d/%m/%Y')

        dStat[ 'day' ] = t.split ('/')[ 0 ]

        dStat[ 'month' ] = t.split ('/')[ 1 ]

        dStat[ 'year' ] = t.split ('/')[ 2 ]

        return dStat

    def removeCompanyEmptyKeys(self, dicts):

        result = [ ]
        keys = [ ]
        for key, value in dicts.items ():
            if (value == ''):
                keys.append (key)
        for ind in keys:
            dicts.pop (ind, None)
        result.append (dicts)

        return result[ 0 ]

    def insertData(self, data, dataSource):

        t = time.time ()
        domain_for_key = data.get ('website')
        domain_for_key = ext.extract(domain_for_key)
        domain_for_key = domain_for_key.domain+'.'+domain_for_key.suffix

        companyKey = ''.join (str (ord (c.lower ())) for c in domain_for_key)

        _key = str (uuid.uuid3 (uuid.NAMESPACE_X500, str (companyKey)))

        # collection_name = ["company_data", "revenue", "funding", "fundedBy", "product",
        #                    "SocialAndOther", "soaTypeData", "dataSourceData", "dataSourceStats"]
        #
        # collections = [companyData, revenueData, fundingData, fundedByData, productData,
        #                sourceAndOtherData, soaTypeData, dataSourceData, dataSourceStats]
        # toUpdate = [ ]
        # toUpdateName = [ ]
        # toInsert = [ ]
        # toInsertName = [ ]
        # if not self.isCompanyUnique (companyKey, "company_data"):
        #     dataFromDB = self.database['company_data'][ companyKey ].getStore ()
        #     dataFromDB[ 'companyDataKey' ] = companyKey
        #     companyData = self.companyData (data=dataFromDB, nData=data, mode="update", dataSource=dataSource)
        #     companyData = {k: v if v is not None else '' for k, v in companyData.items ()}
        #     toUpdate.append (companyData)
        #     toUpdateName.append ('company_data')


        # else:
        if self.phoenixWrite.exists(value=companyKey, column='companyKey', table='company_data'):
            dataFromDB = self.phoenixWrite.retrieve_row(column='companyKey',
                                                        row_key=companyKey,
                                                        table_name='company_data')

            companyData = self.companyData(data=dataFromDB, nData=data, companyKey=companyKey, mode="update", dataSource=dataSource)
            self.phoenixWrite.upsert_one(columnsValuesDict=companyData, tableName='company_data')


        else:
            companyData = self.companyData(data=data, nData=data, companyKey=companyKey, mode="insert", dataSource=dataSource)
            self.phoenixWrite.upsert_one(columnsValuesDict=companyData, tableName='company_data')

        # if not self.isCompanyUnique (_key, "revenue"):
        #
        #     dataFromDB = self.database[ 'revenue' ][ _key ].getStore ()
        #     dataFromDB[ 'companyKey' ] = _key
        #     revenueData = self.revenueData (data=dataFromDB, nData=data, companyKey=companyKey, mode="update",
        #                                     dataSource=dataSource)
        #     revenueData = {k: v if v is not None else '' for k, v in revenueData.items ()}
        #     toUpdate.append (revenueData)
        #     toUpdateName.append ('revenue')



        # else:
        if self.phoenixWrite.exists (value=_key, column='companyKey', table='revenue'):
            dataFromDB = self.phoenixWrite.retrieve_row (column='companyKey',
                                                         row_key=_key,
                                                         table_name='revenue')

            revenueData = self.revenueData (data=dataFromDB, companyKey=_key,nData=data, mode="update", dataSource=dataSource)
            self.phoenixWrite.upsert_one (columnsValuesDict=revenueData, tableName='revenue')
        else:

            revenueData = self.revenueData (data=data, nData=data, companyKey=_key, mode="insert",
                                            dataSource=dataSource)
            self.phoenixWrite.upsert_one (columnsValuesDict=revenueData, tableName='revenue')

        # revenueData = {k: v if v is not None else '' for k, v in revenueData.items ()}
        # toInsert.append (revenueData)
        # toInsertName.append ('revenue')

        # if not self.isCompanyUnique (_key, "funding"):
        #
        #     dataFromDB = self.database[ 'funding' ][ _key ].getStore ()
        #     dataFromDB[ 'companyKey' ] = _key
        #     fundingData = self.fundingData (data=dataFromDB, nData=data, companyKey=companyKey, mode="update",
        #                                     dataSource=dataSource)
        #     fundingData = {k: v if v is not None else '' for k, v in fundingData.items ()}
        #     toUpdate.append (fundingData)
        #     toUpdateName.append ('funding')




        # else:
        if self.phoenixWrite.exists (value=_key, column='companyKey', table='funding'):
            dataFromDB = self.phoenixWrite.retrieve_row (column='companyKey',
                                                         row_key=_key,
                                                         table_name='funding')

            fundingData = self.fundingData (data=dataFromDB, companyKey=_key,nData=data, mode="update", dataSource=dataSource)
            self.phoenixWrite.upsert_one (columnsValuesDict=fundingData, tableName='funding')

        else:
            fundingData = self.fundingData (data=data, nData=data, companyKey=_key, mode="insert",
                                            dataSource=dataSource)
            self.phoenixWrite.upsert_one (columnsValuesDict=fundingData, tableName='funding')

        # fundingData = {k: v if v is not None else '' for k, v in fundingData.items ()}
        # toInsert.append (fundingData)
        # toInsertName.append ('funding')

        # if not self.isCompanyUnique (_key, "fundedBy"):
        #
        #     dataFromDB = self.database[ 'fundedBy' ][ _key ].getStore ()
        #     dataFromDB[ 'companyKey' ] = _key
        #     fundedByData = self.fundedByData (data=dataFromDB, nData=data, companyKey=companyKey, mode="update",
        #                                       dataSource=dataSource)
        #     fundedByData = {k: v if v is not None else '' for k, v in fundedByData.items ()}
        #     toUpdate.append (fundedByData)
        #     toUpdateName.append ('fundedBy')



        # else:
        # fundedByData = self.fundedByData (data=data, nData=data, companyKey=companyKey, mode="insert",
        #                                   dataSource=dataSource)
        # self.phoenixWrite.upsert_one (columnsValuesDict=fundedByData, tableName=tableName)

        # fundedByData = {k: v if v is not None else '' for k, v in fundedByData.items ()}
        # toInsert.append (fundedByData)
        # toInsertName.append ('fundedBy')

        # if not self.isCompanyUnique (_key, "product"):
        #
        #     dataFromDB = self.database[ 'product' ][ _key ].getStore ()
        #     dataFromDB[ 'companyKey' ] = _key
        #     productData = self.product (data=dataFromDB, nData=data, companyKey=companyKey, mode="update",
        #                                 dataSource=dataSource)
        #     productData = {k: v if v is not None else '' for k, v in productData.items ()}
        #     toUpdate.append (productData)
        #     toUpdateName.append ('product')
        if self.phoenixWrite.exists (value=_key, column='companyKey', table='product'):
            dataFromDB = self.phoenixWrite.retrieve_row (column='companyKey',
                                                         row_key=_key,
                                                         table_name='product')
            # print(dataFromDB)

            productData = self.product (data=dataFromDB, companyKey=_key,nData=data, mode="update", dataSource=dataSource)
            self.phoenixWrite.upsert_one (columnsValuesDict=productData, tableName='product')


        else:
            productData = self.product (data=data, nData=data, companyKey=_key, mode="insert",
                                        dataSource=dataSource)
            self.phoenixWrite.upsert_one (columnsValuesDict=productData, tableName='product')

        # productData = {k: v if v is not None else '' for k, v in productData.items ()}
        # toInsert.append (productData)
        # toInsertName.append ('product')

        # if not self.isCompanyUnique (_key, "SocialAndOther"):
        #
        #     dataFromDB = self.database[ 'SocialAndOther' ][ _key ].getStore ()
        #     dataFromDB[ 'companyKey' ] = _key
        #
        #     SocialAndOther = self.sourceAndOther (data=dataFromDB, nData=data, companyKey=companyKey, mode="update")
        #     SocialAndOther = {k: v if v is not None else '' for k, v in SocialAndOther.items ()}
        #     toUpdate.append (SocialAndOther)
        #     toUpdateName.append ('SocialAndOther')
        if self.phoenixWrite.exists (value=_key, column='companyKey', table='socialAndOtherCompany'):
            dataFromDB = self.phoenixWrite.retrieve_row (column='companyKey',
                                                         row_key=_key,
                                                         table_name='socialAndOtherCompany')

            SocialAndOther = self.sourceAndOther(data=dataFromDB,companyKey=_key, nData=data, mode="update")
            self.phoenixWrite.upsert_one (columnsValuesDict=SocialAndOther, tableName='socialAndOtherCompany')


        else:

            SocialAndOther = self.sourceAndOther (data=data, nData=data, companyKey=_key, mode="insert")
            self.phoenixWrite.upsert_one (columnsValuesDict=SocialAndOther, tableName='socialAndOtherCompany')

        # SocialAndOther = {k: v if v is not None else '' for k, v in SocialAndOther.items ()}
        # toInsert.append (SocialAndOther)
        # toInsertName.append ('SocialAndOther')

        # if not self.isCompanyUnique (_key, "soaTypeData"):
        #     dataFromDB = self.database[ 'soaTypeData' ][ _key ].getStore ()
        #     dataFromDB[ 'companyKey' ] = _key
        #     soaTypeData = self.soaTypeData (data=dataFromDB, nData=data, mode="update", companyKey=companyKey)
        #     soaTypeData = {k: v if v is not None else '' for k, v in soaTypeData.items ()}
        #     toUpdate.append (soaTypeData)
        #     toUpdateName.append ('soaTypeData')



        # else:

        # soaTypeData = self.soaTypeData (data=data, nData=data, mode="insert", companyKey=companyKey)
        # soaTypeData = {k: v if v is not None else '' for k, v in soaTypeData.items ()}
        # toInsert.append (soaTypeData)
        # toInsertName.append ('soaTypeData')

        # if not self.isCompanyUnique (_key, "dataSourceData"):
        #     dataFromDB = self.database[ 'dataSourceData' ][ _key ].getStore ()
        #     dataFromDB[ 'companyKey' ] = _key
        #     dataSourceData = self.dataSourceData (data=dataFromDB, companyKey=companyKey, mode="update",
        #                                           dataSource1=dataSource)
        #     dataSourceData = {k: v if v is not None else '' for k, v in dataSourceData.items ()}
        #     toUpdate.append (dataSourceData)
        #     toUpdateName.append ('dataSourceData')



        # else:
        if self.phoenixWrite.exists (value=_key, column='dataSourceKey', table='dataSource'):
            dataFromDB = self.phoenixWrite.retrieve_row (column='companyKey',
                                                         row_key=_key,
                                                         table_name='dataSource')

            dataSourceData = self.dataSourceData(data=dataFromDB, companyKey=_key, mode="update", dataSource1=dataSource)
            self.phoenixWrite.upsert_one (columnsValuesDict=dataSourceData, tableName='dataSource')
        else:
            dataSourceData = self.dataSourceData (data=data, companyKey=_key, mode="insert",
                                                  dataSource1=dataSource)
            self.phoenixWrite.upsert_one (columnsValuesDict=dataSourceData, tableName='dataSource')

        # dataSourceData = {k: v if v is not None else '' for k, v in dataSourceData.items ()}
        # toInsert.append (dataSourceData)
        # toInsertName.append ('dataSourceData')

        # if not self.isCompanyUnique (_key, "dataSourceStats"):
        #     dataFromDB = self.database[ 'dataSourceStats' ][ _key ].getStore ()
        #     dataFromDB[ 'companyKey' ] = _key
        #
        #     dataSourceStat = self.dataSourceStat (data=dataFromDB, companyKey=companyKey, mode="update")
        #     dataSourceStat = {k: v if v is not None else '' for k, v in dataSourceStat.items ()}
        #     toUpdate.append (dataSourceStat)
        #     toUpdateName.append ('dataSourceStats')



        # else:
        # dataSourceStat = self.dataSourceStat (data=data, companyKey=companyKey, mode="insert")
        # self.phoenixWrite.upsert_one (columnsValuesDict=dataSourceStat, tableName='')
        # dataSourceStat = {k: v if v is not None else '' for k, v in dataSourceStat.items ()}
        # toInsert.append (dataSourceStat)
        # toInsertName.append ('dataSourceStats')

        # if not self.isCompanyUnique (_key, "domain"):
        #     dataFromDB = self.database[ 'domain' ][ _key ].getStore ()
        #     dataFromDB[ 'companyKey' ] = _key
        #
        #     domain = self.domain (data=dataFromDB, nData=data, companyKey=companyKey, mode="update",
        #                           dataSource=dataSource)
        #     domain = {k: v if v is not None else '' for k, v in domain.items ()}
        #     toUpdate.append (domain)
        #     toUpdateName.append ('domain')

        if self.phoenixWrite.exists (value=_key, column='companyKey', table='domain'):
            dataFromDB = self.phoenixWrite.retrieve_row (column='companyKey',
                                                         row_key=_key,
                                                         table_name='domain')

            domain = self.domain(data=dataFromDB, nData=data,companyKey=_key, mode="update", dataSource=dataSource)
            self.phoenixWrite.upsert_one (columnsValuesDict=domain, tableName='domain')

        else:
            domain = self.domain (data=data, nData=data, companyKey=_key, mode="insert", dataSource=dataSource)
            self.phoenixWrite.upsert_one (columnsValuesDict=domain, tableName='domain')

        # domain = {k: v if v is not None else '' for k, v in domain.items ()}
        # toInsert.append (domain)
        # toInsertName.append ('domain')

        # self.dataToSEO = {'company_data': companyData, 'SocialAndOther': SocialAndOther}
        # if len (toUpdate):
        #     number_of_chunks = ceil (len (data) * 0.001)
        #     for i, _ in enumerate (toUpdateName):
        #         toUpdate[ i ] = self.removeCompanyEmptyKeys (toUpdate[ i ])
        #         self.objupdate.updateData (data=toUpdate[ i ], collection_name=toUpdateName[ i ])
        #
        # if len (toInsert):
        #     allInOne = [ ]
        #     number_of_chunks = ceil (len (data) * 0.001)
        #
        #     for i, _ in enumerate (toInsertName):
        #         allInOne.append (
        #             self.common.makeCompanyListOfData (collection_name=toInsertName[ i ], data=toInsert[ i ],
        #                                                chunks=number_of_chunks))
        #
        #     datasetForDatabase = self.common.dataToPool (allInOne)
        #     print ("time taken: ", time.time () - t)
        #     return datasetForDatabase

        print ("time taken: ", time.time () - t)
        return


# if __name__ == "__main__":
#     import json
#
#     with open ('testhbase.json') as f:
#         data = json.load (f)
#     # print(data)
#     obj = CompanyHbase ()
#     obj.insertData (data=data, dataSource="FC")

