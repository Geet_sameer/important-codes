import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import bcrypt
# from .db import *
#
# dbObj = dbStuff()
import pymongo as pymongo


def sendemail(email, url, otp=None, invite=False):
    for i in [email]:
        email = i
        fromaddr = "trioncube3@gmail.com"
        toaddrs = email
        # toaddrs.append(email)

        # print(toaddrs)

        # Create the container (outer) email message.
        # msg = MIMEText(url)   #url
        # url = "http://www.google.com"
        if otp is None and invite is False:
            bodyEmail = "body: " + str(url)
        elif invite:
            bodyEmail = "body: " + str(url) + " Your invite will expire in 3 days"
        else:
            bodyEmail = "body: " + str(url) + " otp = " + str(otp)

        msg = MIMEText(bodyEmail)
        msg['Subject'] = 'Email Verification'
        # me == the sender's email address
        # family = the list of all recipients' email addresses
        msg['From'] = fromaddr
        msg['To'] = ",".join(toaddrs)
        msg.preamble = 'OutBeast'
        # msg = "Welcome"
        # msg.attach(MIMEText(body,'plain'))



        password = 'crunchbase'
        # print("password accepted")
        # The actual mail send
        server = smtplib.SMTP(host='smtp.gmail.com:587')
        server.ehlo()
        server.starttls()
        server.login(fromaddr, password)

        # print("server logged in")

        text = msg.as_string()

        # print(text)

        # print("text converted to string")
        server.sendmail(fromaddr, toaddrs, text)
        print("Successfully sent email")

    return


def createAccount(key, data):
    """
    :type data: Dictionary
    :type key: String
    """
    dbObj.Insert(collection="account", key=key, data=data)
    return


def createUser(key, data):
    """
    :type data: Dictionary
    :type key: String
    """
    dbObj.Insert(collection="users", key=key, data=data)
    return


def createMapper(key1, key2):
    data = {'account_id': key2}
    dbObj.Insert(collection="mapper", key=key1, data=data)
    return


def hash_password(pw):
    pwhash = bcrypt.hashpw(pw.encode('utf8'), bcrypt.gensalt())
    return pwhash.decode('utf8')


def check_password(pw, hashed_pw):
    expected_hash = hashed_pw.encode('utf8')
    return bcrypt.checkpw(pw.encode('utf8'), expected_hash)

# def fun():
#     # time.sleep(2)
#     client = pymongo.MongoClient('178.63.85.80', 27017)
#     db = client['baseSystemDB']
#     deduct = 10
#     dSet = dict(
#         last_credit_usage=datetime.datetime.now()
#     )
#     dInc = dict(
#         credits_available=abs(deduct) * (-1)
#     )
#     try:
#         db['credits'].update_one(filter={'account_id': '1234321'}, update={'$set': dSet, '$inc': dInc})
#     except:
#         pass
#
#     print('method')
