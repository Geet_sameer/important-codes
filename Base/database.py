import phoenixdb

import tldextract as ext
import email_split as spl
from .publicdoms import publicE

class PhoenixWrite(object):

    def __init__(self):
        super(PhoenixWrite, self).__init__()
        database_url = 'http://pho.iscreed.com:8765'
        connection = phoenixdb.connect(database_url, autocommit = True)
        self.cursor = connection.cursor(cursor_factory=phoenixdb.cursor.DictCursor)

    def writeValues(self, d):
        values = ""
        columns = []
        for key,value in d.items():
            columns.append(key)
            if isinstance(value, str):
                values = values + "'{}',".format(value)
            elif value is None:
                values = values + str('Null') + ','
            else:
                values = values + str(value) + ','
        return dict(
            values = values[:-1],
            columns = columns
        )

    def upsert_one(self, columnsValuesDict, tableName = None, schemaName = 'CUBERSET'):
        q = self.writeValues(d=columnsValuesDict)
        if tableName is not None:
            query = "UPSERT INTO {table}({columns}) VALUES({values})".\
                format(table=tableName, columns=','.join(q.get('columns')), values=q.get('values'))
        else:
            query = False
        print(query)

        self.runQuery(query=query)

        return query

    def upsert_many(self, columns, valueList, tableName = None, schemaName = 'CUBERSET'):
        pass

    def exists(self, value = None, column = None, table = None, schema = 'CUBERSET'):
        if value and column and table:
            if isinstance(value, str):
                query = "SELECT {col} FROM {tab} WHERE {col} = '{val}'".format(col=column, tab=table, val=value)
            else:
                query = "SELECT {col} FROM {tab} WHERE {col} = {val}".format(col=column, tab=table, val=value)
            res = self.runQuery(query, select=True, fetchOne=True)

            if res:
                return True
            else:
                return False
        else:
            raise ValueError('Value, Column or Table not provided')

    def create(self, tableName = None, columnTypeDict = None, primaryKeysList = None, schemaName = 'CUBERSET', salt = 100):
        if tableName and columnTypeDict and primaryKeysList and schemaName:
            if not set(primaryKeysList).issubset(set(columnTypeDict.keys())):
                raise Exception('Primary keys {} should be present in column names {}'
                                .format(primaryKeysList, list(columnTypeDict.keys())))
            col = []
            for val in columnTypeDict.items():
                col.append("{} {}".format(val[0],val[1]))
            columns = ','.join(col)
            primaryKeys = ','.join(primaryKeysList)
            query = "CREATE TABLE IF NOT EXISTS {table}({columns} " \
                    "CONSTRAINT pk PRIMARY KEY ({primaryKeys})) SALT_BUCKETS = {salt}".format(
                     table=tableName, columns=columns, primaryKeys=primaryKeys, salt = salt
            )
            self.runQuery(query=query)
            return
        else:
            raise ValueError("tableName, columns and primaryKeys can't be Null")

    def delete(self, tab):
        query = "DROP TABLE CUBERSET.{t}".format(t=tab)
        self.runQuery(query=query)

    def runQuery(self, query, select = False, fetchOne = False):
        if query:
            self.cursor.execute(operation=query)
            if select:
                if fetchOne:
                    res = self.cursor.fetchone()
                else:
                    res = self.cursor.fetchall()
                return res
            else:
                return

    def extractFullDomain(self, domain):

        extract = ext.extract(str(domain))

        return extract.registered_domain

    def getDomainFromEmail(self, email):

        split = spl.email_split(str(email))

        return split.domain

    def getEmailType(self, email):

        EmailDomain = self.getDomainFromEmail(email)
        # EmailDomain = EmailDomain.split('.')
        # EmailDomain = EmailDomain[0]
        # lista = ['gmail', 'yahoo', 'hotmail', 'live', 'mail','outlook']
        if EmailDomain in publicE:
            return "Personal"
        else:
            return "Private"

    def getEmailExtension(self,email):

        EmailDomain = self.getDomainFromEmail(email)
        EmailDomain = EmailDomain.split('.')
        EmailDomain = '.'.join(EmailDomain[1:])
        return EmailDomain

    def retrieve_row(self, column, row_key, table_name, schema_name='CUBERSET'):
        query = "SELECT * FROM {table} where {col} = '{val}'".format(col=column, table=table_name, val=row_key)
        # print(query)
        self.cursor.execute(query)
        q_result = self.cursor.fetchone()
        # print(q_result)
        return q_result

    def nameBlock(self,peopleKey):

        query = "SELECT peop.fullName, pos.orgName, pos.title FROM PEOPLE_DATA peop join POSITION_DATA pos on peop.peopleKey = pos.peopleKey WHERE peop.peopleKey = '{peopleKey}'".format(peopleKey = peopleKey)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

    def social_profiles(self,peopleKey):

        query = "SELECT * FROM SOCIALANDOTHERPEOPLE WHERE PEOPLEKEY='{peopleKey}'".format(peopleKey = peopleKey)
        self.cursor.execute(query)
        r = self.cursor.fetchone(doc=True)
        r.pop('PEOPLEKEY')
        r.pop('SOCIALID')
        result = []
        for key, val in r.items():
            if val is not None:
                self.cursor.execute("SELECT url FROM {tab} WHERE SNSUUID='{val}'".format(tab=key.lower(), val=val))
                a = self.cursor.fetchone(doc=True)
                result.append(dict(name=key.lower(), url=a.get('URL')))

        return result

    def sourcesFound(self,peopleKey):

        query = "SELECT * FROM SOURCESFOUND WHERE peopleKey = ‘{peopleKey}’".format(peopleKey=peopleKey)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

    def totalSourcesFound(self,peopleKey):

        query = "SELECT COUNT(*) FROM SOURCESFOUND where peoplekey = ‘{peopleKey}‘".format(peopleKey=peopleKey)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

    def workHistory(self,peopleKey):

        query = "SELECT title,startDate,endDate,orgName FROM POSITION_DATA WHERE peopleKey = ‘{peopleKey}’".format(peopleKey=peopleKey)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

    def domainSearch(self,domainName):

        query = "SELECT p.fullName, e.email FROM PEOPLE_DATA p INNER JOIN EMAILPEOPLE e on p.peoplekey=e.peoplekey WHERE e.domain='{domainName}'".format(domainName = domainName)

        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

    def getpeopleKeyfromDomainName(self,domainName):

        query = "select peopleKey from emailpeople where domain = '{domainName}'".format(domainName=domainName)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result


if __name__ == "__main__":
    # print(len(ALL_TABLES.keys()))
    ob = PhoenixWrite()