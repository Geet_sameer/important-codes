from pyArango.connection import *
connection = Connection('http://178.63.85.80:8529', username='root', password='')
db = connection['sampleDB']



class dbStuff():
    def __init__(self):

        super(dbStuff, self).__init__()

        return

    def UpdateAtInfoGathering(self, collection, data, key):

        _collection = db[collection]
        _data = data

        doc = _collection[key]

        for key, value in _data.items():
            doc[key] = value

        doc.save()

        return

    def InsertAtRegister(self, collection, keyValue):

        _collection = db[collection]
        _keyValue = keyValue

        doc = _collection.createDocument()
        doc._key = _keyValue

        doc.save()

        return

    def Update(self, collection, data, key=None):

        _collection = db[collection]
        _data = data
        _key = key
        doc = _collection[_key]

        for key, value in _data.items():
            doc[key] = value

        doc.save()

        return

    def Insert(self, collection, data, key=None):

        _collection = db[collection]
        _data = data
        doc = _collection.createDocument()

        if (key is not None):
            doc._key = key

        for key, value in _data.items():
            doc[key] = value

        doc.save()

        return

    def Remove(self, collection, key):

        _collection = db[collection]
        _key = key

        doc = _collection[_key]
        doc.delete()

        return

    def MapperUserAccount(self, collection, data):

        _collection = db[collection]
        _data = data
        doc = _collection.createDocument()

        queryResult = []

        for key, value in _data.items():
            aql = "FOR x in " + key + " FILTER x._key == \"" + value + "\" RETURN x._key"
            queryResult.append(db.AQLQuery(aql, rawResults=True, batchSize=100)[0])

        doc._key = queryResult[0]
        doc['account_id'] = queryResult[1]

        doc.save()

        return

    # def checkCollection(self, key):
    #     """
    #
    #     :type key: string
    #     """
    #     result = 0
    #
    #     try:
    #         try:
    #             collection = db['invitation']
    #             doc = collection[key]
    #             result = 1
    #             return result
    #         except:
    #             collection = db['verification']
    #             doc = collection[key]
    #             result = -1
    #             return result
    #     except:
    #         return result

    def checkCollection(self, collection, key):

        try:
            _collection = db[collection]
            doc = _collection[key]
            return True
        except:
            return False


    def getDataThroughKey(self, collection, key, data):

        _collection = db[collection]
        _data = data
        _data_to_return = {}

        doc = _collection[key]

        try:
            for i in _data:
                _data_to_return[i] = doc[i]

            return _data_to_return
        except:
            return False