import requests
import re
import json
import time


# re_to_check_name = "^[a-zA-Z+]{0,10}(\s[a-zA-Z+]{0,10}){0,2}$"


# re_generic_for_scripts_and_aql = "(<.*?[/script]?.*?>)*(for .* in)*(insert {.*})*(update )*"

class Validation_For_Incoming_Parameters(object):
    """docstring for ValidationForIncomingParameters"""

    def __init__(self):
        super(Validation_For_Incoming_Parameters, self).__init__()
        # self.request = request
        # self.validate_input(self.request)
        return

    def validate_input(self, parameters):
        """
        Extract the parameters in request object that are needed to be validated
        The extracted information is a dictionary

        """
        # req = request
        to_be_validated = parameters

        # print(to_be_validated)

        result = self.check_parameters_accept(to_be_validated)

        return result

    def check_parameters_accept(self, data):
        """
        Here we check if the parameters that are passed through the url accepted or extra malacious
        parameters also included in the request url

        """
        list_of_accepted_parameters = ['username', 'password', 'fullname', 'key', 'email']

        # print(list_of_accepted_parameters)
        count = 0

        self.data = data

        parameters_to_validate = set(self.data.keys()).intersection(set(list_of_accepted_parameters))

        for key in parameters_to_validate:
            if (self.check_the_string(key, self.data[key])):
                count = count + 1

        if (count == len(parameters_to_validate)):
            # print("count yes:", count)
            return True
        else:
            return False

    def check_the_string(self, key, value):
        """
        Validating the values sent by the user

        """
        # global re_for_password_and_email
        # global re_for_scripts

        re_for_password_and_email = "[^\S]"  # No white spaces allowed
        re_for_scripts = "[<>'\"].*?[<>'\"]"  # No tags allowed with open and close brackets

        self.key = key
        self.value = value

        if (self.key != 'fullname'):
            if (re.search(re_for_password_and_email, self.value)):
                print("Coming here for having a space", self.value)
                return False
            else:
                print("coming here for not having a space ", self.value)
                return True
        else:
            if (re.search(re_for_scripts, self.value)):
                print("coming here for having a script tag", self.value)
                return False
            else:
                print("coming here for not having a script tag ", self.value)
                return True


if __name__ == "__main__":
    data = {'name': 'Yugantar '';!--"<XSS=&{()} Malhotra', 'password': 'yu<sjsj>yuu', 'email': 'hehehe@gmail.com',
            'new': 'rrr'}
    r = requests.post('http://httpbin.org/post', params=data)

    t2 = time.time()

    obj = Validation_For_Incoming_Parameters()
    val = obj.validate_input(r)

    print(val)
    print("Time taken is: ", time.time() - t2)