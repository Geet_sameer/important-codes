import gevent.monkey
import pyotp
gevent.monkey.patch_socket()
gevent.monkey.patch_select()
from .database import *
from datetime import datetime
from datetime import timedelta
from dateutil import parser
import uuid

from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    remember,
    forget,
)
from requests import Response
from pyramid.view import (
    view_config,
    view_defaults
)
from .common import *
from .validation import *
import gevent
from .BackGroundProcess import *

validate = Validation_For_Incoming_Parameters()
from .security import (
    USERS,
    check_password,
    # connection,
    # validate_token

)

import uuid

from datetime import (datetime, timedelta)

from strgen import StringGenerator as gen

from .common import *

from .db import *

dbObj = dbStuff()

userKey = ''

dbmethods = PhoenixWrite()


@view_defaults(renderer='home.pt')
class Views:
    def __init__(self, request):
        self.request = request
        self.logged_in = request.authenticated_userid

    @view_config(route_name='home')
    def home(self):
        return {'name': 'Home View'}

    @view_config(route_name='hello')
    def hello(self):
        return {'name': 'Hello View'}

    @view_config(route_name='login', renderer='login.pt')
    def login(self):
        request = self.request
        # import pdb;pdb.set_trace()
        login_url = request.route_url('login')
        referrer = request.url
        if referrer == login_url:
            referrer = '/'  # never use login form itself as came_from
        came_from = request.params.get('came_from', referrer)
        message = ''
        login = ''
        password = ''
        if 'form.submitted' in request.params:
            login = request.params['login']
            password = request.params['password']
            if check_password(password, USERS.get(login)):
                headers = remember(request, login)
                expire_timestamp = str(datetime.now() + timedelta(days=2))
                key = uuid.uuid4()
                """
                key: user_id
                value: '{"token": "<token>", "expires_at": "<expires_at>"}'
                """

                # Store login session to redis
                # connection.hset('login-system', key=key, value=expire_timestamp)
                return HTTPFound(location=came_from, headers=headers)
            message = 'Failed login'

        if 'form.submitted.forgot' in request.params:
            headers = remember(request, login)
            return HTTPFound(location='forgotPassword', headers=headers)

        return dict(
            name='Login',
            message=message,
            url=request.application_url + '/login',
            came_from=came_from,
            login=login,
            password=password,

        )

    @view_config(route_name='forgotPassword', renderer='forgotPassword.pt')
    def forgotPassword(self):
        request = self.request
        login_url = request.route_url('forgotPassword')
        referrer = request.url
        if referrer == login_url:
            referrer = '/'  # never use login form itself as came_from
        came_from = request.params.get('came_from', referrer)
        message = ''
        email = ''
        if 'form.submitted' in request.params:
            email = request.params['Email']
            headers = remember(request, email)

            # send password to the email from the database

            print("your entered email is", email)
            return HTTPFound(location=came_from, headers=headers)
        message = 'Failed'
        return dict(
            name='forgotPassword',
            message=" ",
            url=request.application_url + '/forgotPassword',
            came_from=came_from,
            Email=email

        )

    @view_config(route_name='logout')
    def logout(self):
        request = self.request
        headers = forget(request)
        url = request.route_url('home')
        return HTTPFound(location=url,
                         headers=headers)

    @view_config(route_name='register', renderer='register.pt')
    def register(self):

        request = self.request
        # print("request is",request)
        response = Response()
        # response = request.authenticated_userid
        # print("response is ", response)

        register_url = request.route_url('register')

        referrer = request.url

        if referrer == register_url:
            referrer = '/'

        came_from = request.params.get('came_from', referrer)

        email = " "

        if 'form.submit' in request.params:

            email = request.params['email']
            # print(email)
            # print(type(email))
            parameters = {'email': email}

            validate_input = validate.validate_input(parameters)

            if validate_input:
                # global dbObj

                # Create org Account
                # Create User Account

                verification_key = str(uuid.uuid4())

                url = "http://0.0.0.0:6543/userdetails/" + str(verification_key)
                # print("url is", url)
                # sendemail(email, url)
                import gevent
                from gevent import Greenlet
                g = gevent.Greenlet(sendemail, [email], url)
                # g = gevent.Greenlet(fun)
                g.start()
                # print("gevent")


                # bgProcess(sendemail(email=email, url=url))

                expiry_date = str(datetime.datetime.now() + timedelta(days=2))

                data_to_be_stored = {'email': email, 'expiry_date': expiry_date}

                dbObj.Insert(collection="verification", data=data_to_be_stored, key=verification_key)

                headers = remember(request, email)
                # response.set_cookie(email, value=verification_key)


                return HTTPFound(location=request.route_url('userdetails'), headers=headers)

        return dict(
            name="email",
            email=email,
            came_from=came_from,
            url=request.application_url + '/register',
            message=" "
        )

    # @view_config(route_name='AccountSettings', renderer='json')
    @view_config(route_name='AccountSettings', renderer='accountSettings.jinja2')
    def AccountSettings(self):
        request = self.request
        account_url = request.route_url('AccountSettings')
        # referrer = request.url
        # if referrer == account_url:
        #     referrer = '/'  # never use login form itself as came_from
        # came_from = request.params.get('came_from', referrer)

        if 'form.submitted.Update' in request.params:
            return HTTPFound(location=request.route_url('UpdateUserDetails'))

        if 'form.submitted.Team' in request.params:
            return HTTPFound(location=request.route_url('TeamManagement'))

        if 'form.submitted.Billing' in request.params:
            return HTTPFound(location=request.route_url('BillingSystem'))

        return dict(
            name="Account Settings",
            url=request.application_url + '/AccountSettings'
        )

    @view_config(route_name='UpdateUserDetails', renderer='json')
    # @view_config(route_name='UpdateUserDetails', renderer='UpdateUserDetails.jinja2')
    def UpdateUserDetails(self):
        request = self.request
        update_url = request.route_url('UpdateUserDetails')
        # userdetails = {}
        FirstName = request.POST.get('firstname')
        LastName = request.POST.get('lastname')
        Password = request.POST.get('password')
        Phone_Number = request.POST.get('phone')
        # userdetails['FirstName'] = FirstName
        data = dict(FirstName=FirstName, LastName=LastName, Password=Password, Phone_Number=Phone_Number)
        # print(FirstName)
        # return dict(data = data)
        # if 'form.submit' in request.params:
        #     # store the details in database
        #
        #     FirstName = request.params['firstname']
        #     userdetails['FirstName'] = FirstName
        #     LastName = request.params['lastname']
        #     userdetails['LastName'] = LastName
        #     Gender = request.params['gender']
        #     userdetails['Gender'] = Gender
        #     print(userdetails)

        return dict(names='UpdateUserDetails', url=request.application_url + '/UpdateUserDetails',
                    data=data)

    @view_config(route_name='TeamManagement', renderer='TeamManagement.jinja2')
    def TeamManagement(self):

        request = self.request
        team_url = request.route_url('TeamManagement')
        # referrer = request.url
        # if referrer == team_url:
        #     referrer = '/'  # never use login form itself as came_from
        # came_from = request.params.get('came_from', referrer)

        if 'form.submitted.InviteTeam' in request.params:
            return HTTPFound(location=request.route_url('InvitePage'))

        if 'form.submitted.EditTeam' in request.params:
            return HTTPFound(location=request.route_url('EditTeamMembers'))

        return dict(name='Team', url=request.application_url + '/TeamManagement')

    @view_config(route_name='EditTeamMembers', renderer='EditTeamMembers.jinja2')
    # @view_config(route_name='EditTeamMembers', renderer = 'json')
    def EditTeamMembers(self):

        request = self.request
        editTeam_url = request.route_url('EditTeamMembers')

        # Get the username

        # request.session.get('username') = username

        # Get the users under his/her account to display


        # options = request.POST.get('status')
        #
        # if options == 'active':
        #     status = 1               # No delete, only change the status in user mapping
        # elif options == 'disable':
        #     status = 0
        # elif options == 'deleted':
        #     status = -1
        # else:
        #     # change the usergroup
        #     pass

        # return dict(options=options)

        if request.params.get('form.submit') == 'Submit':
            username = request.params['username']
            usergroup = request.params['usergroup']

            options = request.params['editTeamlist']
            print(username)
            print(usergroup)
            print(options)

        return dict(
            url=request.application_url + '/EditTeamMembers'
        )

    # @view_config(route_name = 'people_data', renderer = 'json')
    # def people_data(self):
    #
    #     # Get email and convert it into people_key
    #
    #     # 1st block
    #     nameblock = dbmethods.nameBlock(peopleKey)
    #
    #     # 2nd block
    #     social_profiles = dbmethods.social_profiles(peopleKey)
    #
    #     # 3rd block
    #
    #     sourcesFound = dbmethods.sourcesFound(peopleKey)
    #
    #     # Total sources found i.e; count
    #
    #     sourcesCount = dbmethods.totalSourcesFound(peopleKey)
    #
    #     # work history
    #
    #     workHistory = dbmethods.workHistory(peopleKey)
    #
    #     data = dict(nameblock = nameblock, social_profiles = social_profiles, sourcesFound = sourcesFound, sourcesCount = sourcesCount,
    #                 workHistory = workHistory)
    #
    #     return data
    #
    # @view_config(route_name = 'domainSearch', renderer = 'json')
    # def domainSearch(self):
    #     # Get the domain name
    #
    #
    #     emails = dbmethods.domainSearch(domainName)
    #
    #     # Get peopleKey from the emailPeople using domain name
    #
    #     # select peopleKey from emailpeople where domain = 'microsoft.com'
    #
    #     peopleKey = dbmethods.getpeopleKeyfromDomainName(domainName)
    #
    #     sourcesCount = dbmethods.totalSourcesFound(peopleKey)
    #
    #     data = dict(emails = emails, sourcesCount = sourcesCount)
    #
    #     return data

    @view_config(route_name='campaign', renderer='json')
    def campaign(self):
        request = self.request
        Send_From_email = request.POST.get('sendfrom')
        # TODO verify the email
        # TODO import the contacts
        campaign_name = request.POST.get('campaignName')
        select_days = request.POST.get("days")
        stop_campaign = request.POST.get("options")
        Topic_head = request.POST.get("topicName")
        # Store the details in the Campaign table

        data = dict(Send_From=Send_From_email, campaign_name=campaign_name, select_days=select_days,stop_campaign=stop_campaign,Topic_head=Topic_head)
        return data

    @view_config(route_name='campaignInfo', renderer='json')
    def compaignInfo(self):
        request = self.request
        # Template
        # when the user selected the template, get the selected template from the Templates table
        # TODO Attachment
        series_name = request.POST.get('seriesName')
        whichDay = request.POST.get('day')
        subject = request.POST.get('subject')
        message = request.POST.get('message')
        status = request.POST.get('status')
        data = dict(subject=subject, message=message, series_name=series_name, whichDay=whichDay,status=status)
        return data

    @view_config(route_name='InvitePage', renderer='json')
    # @view_config(route_name='InvitePage', renderer='InvitePage.jinja2')
    def InvitePage(self):

        request = self.request
        invite_url = request.route_url('InvitePage')

        invites = request.POST.get('email')
        dataToStore = dict(invites=invites)
        # Send Invitaion links to all the emails
        if "form.submit" in request.params:
            key = str(uuid.uuid4())
            url = "http://0.0.0.0:6543/invitation/" + key
            email = request.params['email']
            emaillist = email.split(",")
            import gevent
            from gevent import Greenlet
            g = gevent.Greenlet(sendemail, emaillist, url)
            g.start()
            return HTTPFound(location='/')

        return dict(names='Invitation', url=request.application_url + '/InvitePage', invites=invites)

    @view_config(route_name='BillingSystem', renderer='BillingSystem.jinja2')
    def BillingSystem(self):
        request = self.request
        return {'name': 'Billing'}

    @view_config(route_name='userdetails', renderer='userdetails.pt')
    def hello(self):
        request = self.request
        print(request.POST.get('hi'))
        # response = Response()
        email_url = request.route_url('userdetails')
        # print("email_url is....", email_url)
        return {'name': 'Userdetails'}

    @view_config(route_name = 'verifyCampaignEmail', renderer = 'json')
    def verifyCampaignEmail(self):
        request = self.request
        email = request.POST.get('email')
        import pyotp
        otp = pyotp.random_base32()
        # store the OTP in the collection
        import gevent
        from gevent import Greenlet
        g = gevent.Greenlet(sendemail,[email], otp)
        g.start()

        # check whether the opt is matched or not
        if otp:
            verified = True
        else:
            verified = False

        data = dict(email=email)
        return data