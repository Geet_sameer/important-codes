import requests
from pyramid.view import (view_config, view_defaults, forbidden_view_config)


from pyramid.renderers import render_to_response

from .peopleInsertion import *
from .peopleUpdate import *
from .database import *
from .companyInsertion import *



@view_defaults(renderer='home.jinja2')
class seo:
    def __init__(self, request):
        self.request = request

    @view_config(route_name='home')
    def home(self):
        return {'message': 'HOME'}

    @view_config(route_name='fullcontact', renderer='fullContact.jinja2')
    def fullcontact(self):
        request = self.request

        if request.params.get('form.submit') == 'Submit':
            text = request.params['api']

            whichAPI = request.params['apicall']
            #key = '151471cd9a8084d3'
            key = '5b1da44dbd7df13b'
            # key = '1632fd99793cc5a3'
            headers = {
                'X-FullContact-APIKey': key,
            }
            params = (
                (whichAPI, text),
            )
            if whichAPI == "email":
                url = 'https://api.fullcontact.com/v2/person.json'
            elif whichAPI == "emailverification":
                url = 'http://api.fullcontact.com/v2/verification/email'
            else:
                url = 'https://api.fullcontact.com/v2/company/lookup.json'
            r = requests.get(url, headers=headers, params=params)
            # print(r.json())
            objPeopleInsert = People()
            objPeopleUpdate = PeopleUpdate()
            objCompany = CompanyHbase()
            dbStuff = PhoenixWrite()
            # objPeopleUpdate = UpdatePeople()

            if whichAPI == 'email':
                # get domain name
                # domainName = common.getDomainFromEmail(text)
                # #Increment Count
                # count = common.IncEmailDomainCount(domainName)
                # print("count of Domain Names" , count)
                if not dbStuff.exists(value = text, column ='email', table ='emailPeople'):
                    print("Entering into Insert")
                    print(r.json())
                    readDF = objPeopleInsert.insert(r.json(), text, 'FullContact')
                    # p = Pool(processes=2)
                    # result = p.map(objPeople.save_bulk_data, readDF)
                    # p.close()
                    # p.join()
                    # objPeople.SEOMethod()
                else:
                    print("Entering into Update")
                    print(r.json())
                    # pass
                    readDF = objPeopleUpdate.update(r.json(), text, 'FullContact')

            elif whichAPI == "emailverification":
                print("Email verification")
                print(r.json())
            else:
                print("Insertion company data")
                print(r.json())

                readDF = objCompany.insertData(r.json(),'FullContact')

                # print(r.json())



        return dict(
            url=request.application_url + '/apicall/fullcontact'
        )
