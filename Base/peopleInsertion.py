import json
import uuid
import time
from .database import *



class People:
    def __init__(self):
        super(People, self).__init__()
        return

    def insert(self, data, email, dataSource):

        # with open(data) as data_file:
        #     data = json.load(data_file)
        # print(data)

        dbStuff = PhoenixWrite()

        # Generate baseKey from email
        gen_basekey = ''.join(str(ord(c.lower())) for c in email)
        # gen_basekey = uuid.uuid3(uuid.NAMESPACE_X500, str(email))
        baseKey = str(gen_basekey)

        # Generate peopleKey from baseKey
        peopleKey = str(uuid.uuid3(uuid.NAMESPACE_X500, str(gen_basekey)))

        # Store baseKey and peopleKey in people_base

        peoplebasedict = dict(baseKey=baseKey, peopleKey=peopleKey,email = email)

        dbStuff.upsert_one(peoplebasedict, tableName='people_base')

        # This is for emailPeople table
        Extension = dbStuff.getEmailExtension(email)
        domain = dbStuff.getDomainFromEmail(email)
        emailtype = dbStuff.getEmailType(email)
        emailPeopledict = dict(peopleKey=peopleKey, email=email,type = emailtype,domain = domain,Email_Extension = Extension)
        dbStuff.upsert_one(emailPeopledict, tableName='emailPeople')

        # This is for dataSource table

        # _uuid = str(uuid.uuid4())
        dskey = str(peopleKey + "-" + dataSource)
        dataSourceDict = dict(dataSourceKey=peopleKey, uuid=dskey, sourceName=dataSource)
        dbStuff.upsert_one(dataSourceDict, tableName='dataSource')

        # This data is to insert in people_data table

        fullName = ''
        url = ''
        firstName = ''
        lastName = ''
        if data.get('contactInfo'):

            if data.get('contactInfo').get('websites'):
                _url = data.get('contactInfo').get('websites')
                for urldata in _url:
                    url = urldata.get('url')
            if data.get('contactInfo').get('fullName'):
                fullName = data.get('contactInfo').get('fullName')
            # print(fullName)
            if data.get('contactInfo').get('givenName'):
                firstName = data.get('contactInfo').get('givenName')
            # print(firstName)
            if data.get('contactInfo').get('familyName'):
                lastName = data.get('contactInfo').get('familyName')
                # print(lastName)
        else:
            pass

        aboutNote = ''
        socialProfiles = data.get('socialProfiles')
        if data.get('socialProfiles'):
            try:
                aboutNote = "".join([d['bio'] for d in socialProfiles if 'bio' in d])
                # print(aboutNote)
                aboutNote = aboutNote.replace('\r\n', '').replace('\'','')

            except:
                pass
        else:
            pass

        # Include peopleKey and uuid to the dictionary

        peopledict = dict(peopleKey=peopleKey, fullName=fullName, firstName=firstName, lastName=lastName,
                          aboutNote=aboutNote,website_url = url)
        dbStuff.upsert_one(peopledict, tableName='people_data')

        # This is for position_data table




        if data.get('organizations'):

            organizations = data.get('organizations')

            for com in organizations:
                # data['org'] = com
                # _uuid = str(uuid.uuid4())
                # peopleKey = uuid.uuid4()
                Orgname = com.get('name')
                # print("orgname",Orgname)
                startDate = com.get('startDate')
                # print("startdate",startDate)
                endDate = com.get('endDate')
                # print("enddate",endDate)
                title = com.get('title')
                # print("title",title)
                Currentvalue = com.get('current')
                # print("current",Currentvalue)
                posdict = dict(peopleKey=peopleKey, startDate=startDate, endDate=endDate,
                               orgName=Orgname,
                               title=title, currently=str(Currentvalue))
                # print(posdict)
                dbStuff.upsert_one(posdict, tableName='position_data')

                # dbStuff.close()

        else:
            posdict = dict(peopleKey = peopleKey)
            dbStuff.upsert_one(posdict, tableName='position_data')


        # This is for photos table



        if data.get('photos'):
            photos = data.get('photos')
            for pho in photos:
                # _uuid = str(uuid.uuid4())
                Type = pho.get('type')
                # print(Type)
                TypeId = pho.get('typeId')
                # print(TypeId)
                TypeName = pho.get('typeName')
                # print(TypeName)
                PicUrl = pho.get('url')
                # print(PicUrl)
                IsPrimary = pho.get('isPrimary')
                # print(IsPrimary)

                # Include peopleKey and uuid to this dict
                photokey = str(peopleKey + "-" + Type + "-" + TypeName + "-" + TypeId)
                photosdict = dict(photoskey=photokey, peopleKey=peopleKey, type=Type, typeId=TypeId, typeName=TypeName,
                                  picUrl=PicUrl,
                                  isPrimary=str(IsPrimary))
                # print(photosdict)
                dbStuff.upsert_one(photosdict, tableName='photos')
        else:
            photosdict = dict(peopleKey=peopleKey)
            dbStuff.upsert_one(photosdict, tableName='photos')



        # This is for Role table



        if data.get('organizations'):

            organizations = data.get('organizations')

            for role in organizations:
                # _uuid = str(uuid.uuid4())
                if role.get('title'):
                    RoleName = role.get('title')
                else:
                    RoleName = ''
                if role.get('name'):
                    RoleName = role.get('name')
                else:
                    RoleName = ''
                # print(RoleName)

                # Include peopleKey and RoleKey to this dict
                rkey = str(peopleKey + "-" + RoleName)
                roledict = dict(peopleKey=peopleKey, roleName=RoleName, roleKey= rkey)
                # print(roledict)
                dbStuff.upsert_one(roledict, tableName='role')
                # dbStuff.close()
                # print(d)
        else:
            roledict = dict(peopleKey=peopleKey)
            dbStuff.upsert_one(roledict, tableName='role')



        # This is for Demographics

        if data.get('demographics'):

            # _uuid = str(uuid.uuid4())

            if data.get('demographics').get('locationGeneral'):
                location = data.get('demographics').get('locationGeneral')

            else:
                location = ''

            print("location is-->", type(location))
            city = ''
            if data.get('demographics').get('locationDeduced'):
                # print(_city)
                if data.get('demographics').get('locationDeduced').get('city'):
                    city = data.get('demographics').get('locationDeduced').get('city')
                    city = str(city.get('name'))
                else:
                    city = ''

            else:
                city = ''
            print("city is-->", city)
            state = ''
            state_code = ''
            if data.get('demographics').get('locationDeduced'):
                if data.get('demographics').get('locationDeduced').get('state'):
                    _state = data.get('demographics').get('locationDeduced').get('state')
                    state = str(_state.get('name'))
                    state_code = str(_state.get('code'))
                else:
                    state = ''
                    state_code = ''

            else:
                state = ''
                state_code = ''

            print("state is-->", state)
            print("state_code is-->", state_code)

            country = ''
            country_code = ''
            if data.get('demographics').get('locationDeduced'):
                if data.get('demographics').get('locationDeduced').get('country'):
                    _country = data.get('demographics').get('locationDeduced').get('country')
                    country = str(_country.get('name'))
                    country_code = str(_country.get('code'))
                else:

                    country = ''
                    country_code = ''

            else:
                country = ''
                country_code = ''
            print("country is-->", country)
            print("country_code is-->", country_code)

            county = ''
            if data.get('demographics').get('locationDeduced'):
                if data.get('demographics').get('locationDeduced').get('county'):

                    _county = data.get('demographics').get('locationDeduced').get('county')
                    county = str(_county.get('name'))
                else:
                    county = ''

            else:
                county = ''

            print("county is-->", county)

            continent = ''
            if data.get('demographics').get('locationDeduced'):
                if data.get('demographics').get('locationDeduced').get('continent'):
                    _continent = data.get('demographics').get('locationDeduced').get('continent')
                    continent = str(_continent.get('name'))
                else:
                    continent = ''
            else:
                continent = ''
            print("continent is-->", continent)

            age = ''
            if data.get('demographics').get('age'):
                age = int(data.get('demographics').get('age'))
            else:
                age = ''
            print("age is-->", age)

            ageRange = ''
            if data.get('demographics').get('ageRange'):
                # if data.get('demographics').get('age-range'):
                ageRange = str(data.get('demographics').get('ageRange'))
            else:
                ageRange = ''
            print("ageRange is-->", ageRange)

            gender = ''
            if data.get('demographics').get('gender'):
                # if data.get('demographics').get('gender'):
                gender = str(data.get('demographics').get('gender'))
            else:
                gender = ''
            print("gender is-->", gender)

            # #Include peopleKey in this dict
            demographicskey = str(
                peopleKey + "-" + location + "-" + city + "-" + state + "-" + state_code + "-" + country_code
                + "-" + country + "-" + county + "-" + str(age) + "-" + ageRange + "-" + gender + "-" + continent)
            # print(demographicskey)

            demodict = dict(peopleKey=peopleKey, uuid=demographicskey, location=location, city=city, state=state,
                            state_code=state_code,
                            country_code=country_code, country=country, county=county,
                            age=age, ageRange=ageRange, gender=gender, continent=continent)
            # print(demodict)
            dbStuff.upsert_one(demodict, tableName='demographics')
            # dbStuff.close()
            # print(d)
        else:
            demodict = dict(peopleKey=peopleKey)
            dbStuff.upsert_one(demodict, tableName='demographics')


        # This is for currentCompany table



        if data.get('organizations'):

            organizations = data.get('organizations')

            for org in organizations:
                # _uuid = str(uuid.uuid4())
                companyName = org.get('name')
                peopleKeycurrent = str(peopleKey + "-" + companyName)
                # Include peopleKey and UUID in this dict
                companydict = dict(peopleKey = peopleKey,currentKey=peopleKeycurrent, companyName=companyName)
                #     print(companydict)
                dbStuff.upsert_one(companydict, tableName='currentCompany')
                # dbStuff.close()
                # print(d)
        else:
            companydict = dict(peopleKey=peopleKey)
            dbStuff.upsert_one(companydict, tableName='currentCompany')

        # This is for sourcesFound table

        # keywords = data.get('digitalFootprint').get('topics')
        # print(keywords)
        if data.get('digitalFootprint'):

            keywords = data.get('digitalFootprint').get('topics')
            for key in keywords:
                # _uuid = str(uuid.uuid4())

                sourceName = str(str(key.get('provider')) + "-" + str(key.get('value')))
                # print(sourceName)
                sourcefoundkey = str(peopleKey + "-" + sourceName)
                sourceDict = dict(peopleKey = peopleKey,sourceFoundkey = sourcefoundkey,sourceName = sourceName)
                # print(sourceDict)
                dbStuff.upsert_one(sourceDict, tableName='sourcesFound')


        else:
            sourceDict = dict(peopleKey=peopleKey)
            dbStuff.upsert_one(sourceDict, tableName='sourcesFound')






        # This is for socialAndOtherPeople



        # socialAccountList = ['angellist', 'linkedin', 'facebook', 'twitter', 'instagram', 'googleplus', 'pinterest',
        #                      'github', 'foursquare', 'klout', 'flickr', 'gravatar', 'aboutme', 'vimeo','googleplus','myspace',
        #                      'aboutme','wordpress','plancast']
        socialAccountList = ['amplify', 'aolchat', 'bebo', 'bitbucket', 'blippy', 'crunchbase', 'delicious', 'digg', 'disqus', 'dribble', 'flavorsme', 'friendster', 'gdgt', 'getsatisfaction', 'hackernews', 'hunch', 'hyves', 'identica', 'intensedebate', 'lastfm', 'livejournal', 'meetup', 'mixi', 'ohloh', 'other', 'picasa', 'plaxo', 'qik', 'reddit', 'reverbnation', 'shelfari', 'slideshare', 'soundcloud', 'stackoverflow', 'stumbleupon', 'tripit', 'weibo', 'wordpress', 'yelp', 'academiaedu', 'bandcamp', 'behance', 'blipfm', 'blogger', 'dandyid', 'deviantart', 'diigo', 'doyoubuzz', 'econsultancy', 'facebookchat', 'friendfeed', 'fullcontact', 'getglue', 'gist', 'googleprofile', 'gtalkchat', 'hi5', 'hypemachine', 'icqchat', 'imdb', 'itcchat', 'lanyrd', 'meadiciona', 'mixcloud', 'myspace', 'orkut', 'pandora', 'pinboard', 'plancast', 'plurk', 'quora', 'renren', 'scribd', 'skype', 'smugmug', 'stackexchange', 'steam', 'tagged', 'tumblr', 'typepad', 'vk', 'xing', 'youtube', 'angellist', 'linkedin', 'facebook', 'twitter', 'instagram', 'googleplus', 'pinterest', 'github', 'foursquare', 'klout', 'flickr', 'gravatar', 'aboutme', 'vimeo', 'googleplus', 'aboutme']

        if data.get('socialProfiles'):

            socialProfiles = data.get('socialProfiles')

            socialID = str(uuid.uuid4())


            # print(socialID)
            # peopleKeysocial = peopleKey + "-" + tableName
            data = {'socialID': socialID, 'peopleKey': peopleKey}
            for soc in socialProfiles:
                try:
                    tableName = soc.get('typeName').replace(".", "").lower()
                except:
                    tableName = ''
                # print(type(tableName))
                tableNameId = tableName + "-" + peopleKey
                # print(tableNameId)
                data[tableName] = tableNameId
                # d = dict(socialID = socialID, tableName = tableName, tableNameId = tableNameId)
                # print(soc.get('type-name').lower().replace(".",""))

                typesns = soc.get('type')
                if typesns.lower() in socialAccountList:

                    typeId = soc.get('typeId')
                    typeName = soc.get('typeName')
                    url = soc.get('url')
                    snsid = soc.get('id')

                    username = soc.get('username')

                    socialdict = dict(snsUUID=tableNameId, typesns=typesns, typeId=typeId, typeName=typeName, url=url,
                                      snsid=snsid, username=username)
                else:
                    print("coming to else block")
                    socialdict = dict(snsUUID=tableNameId)
                    # create a file and write the typesns in that file



                # socialdict = dict(snsUUID=tableNameId, typesns=typesns, typeId=typeId, typeName=typeName, url=url,
                #                   snsid=snsid, username=username)
                # print(socialdict)
                dbStuff.upsert_one(socialdict, tableName=tableName)
                # print(d)
            # print("data is",data)
            # This is to send for socialAndOtherPeople
            dbStuff.upsert_one(data, tableName='socialAndOtherPeople')
        # dbStuff.close()

        # print(data)



if __name__ == "__main__":
    obj = People()
    l = obj.insert()
