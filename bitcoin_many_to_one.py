#
from cryptos import *
import pprint

c = Bitcoin(testnet=True)
priv_1 = 'cVPFWHcwALpHHLbMUDpNfokh8UnYujgoNA3XPGjvGXb8QDBvzdyw'
addr_1 = 'mzCcYe6WzrEX1FpF2a18pqtCJU9zYE4xig'
priv_2 = 'cSMVNLukL4KopyFRdVNgNCzxVRtZegqiJcgxPHUUUFB8LQQbW4o2'
addr_2 = 'mnAJWx7kX4kAbjiiSVs73kDbzdAF9CapBY'
priv_3 = 'cQrWn6Ti4hqXuM4j6Rr2ywKmDUYrWW5HhfF5odbBZ4yUS1FzRiTQ'
addr_3 = 'n1VPiJsBQUZzqMqFofyjAqT74tPM1aMSqr'
destination = 'mnAJWx7kX4kAbjiiSVs73kDbzdAF9CapBY'
input_address_list = [addr_1,addr_2,addr_3]
priv_key_list = [priv_1,priv_2,priv_3]

# inputs = c.unspent(input_address_list[2])
# print(inputs)
amount_to_send = 288877448
tx_fee = 100000

# tx = c.preparesignedtx(privkey=priv,to=destination,value=amount_to_send,fee=tx_fee,change_addr=my_addr)
# transaction_id = c.pushtx(tx)
# print(transaction_id)
input_list_raw_1 = [{'output': '08d36284d926bf38aea8b2e58fc061a43f595ae5fa1c0fe0a06c22cbcf336eb9:0', 'value': 223134094}, 
					{'output': '10d2774866c8a726dfd1e1b0196ce3c3d056ecfb710881c5b34b81f9cdefa999:0', 'value': 64543354}]
P_KEYS = []

for i in input_list_raw_1:
	P_KEYS.append(priv_1)

input_list_raw_2 = [{'output': '524fdeb4ee8ef828a81c63a8d5d80e10ffcdafb52e25ec60e706ed271f05ef2a:1', 'value': 1000000}, 
					{'output': '897088afe3d82bd2ac5982684545182d8d7e82579385b0c6de54f5c3ee848347:1', 'value': 100000}, 
					{'output': '9b9ce64970b91a62dea4124bf27355e4aeb603c55be38fbd952b5f6fa1d52be0:1', 'value': 100000}]

for i in input_list_raw_2:
	P_KEYS.append(priv_2)

# input_list_raw_3 =[{'output': 'f5fd872998b4d52d2caf95069bb491c847d9406344b6e0932961f8939767ad50:0','value': 133571476}]
total_inputs = input_list_raw_1 + input_list_raw_2
print(len(total_inputs)) 
# make a dictionary
# input_dict = {} 
# input_list = []
# for i in input_address_list:
# 	inputs = c.unspent(i)
# 	for index,j in enumerate(inputs):
# 		input_list.append(j)
		# input_dict[priv_key_list[index]] = input_list
		

# pprint.pprint(input_list)
# pprint.pprint(input_dict)
withdraw_amount = amount_to_send - tx_fee;
# print(withdraw_amount)
outs = [{'value':withdraw_amount,'address':destination}]
# print(len(input_list))
tx = c.mktx(total_inputs,outs)
# print(tx)
# for index,j in enumerate(input_list):
# 	c.sign(tx,index,priv)      # can use signall method also

# tx_1 = c.signall(tx,priv_1)
# tx_2 = c.signall(tx,priv_2)
# tx_3 = c.signall(tx,priv_3)

# for index,j in enumerate(input_list_raw_1):
# 	first = c.sign(tx,index,priv_1)



# for index,j in enumerate(input_list_raw_2):
# 	second = c.sign(first,index,priv_2)

# for index,j in enumerate(input_list_raw_3):
# 	c.sign(tx,index,priv_3)


def f(tx, n, private_key=None):
	_tx = c.sign(tx, n, P_KEYS[n])
	# pprint.pprint(tx)
	n = n+1
	if n<len(P_KEYS):
		_tx_final = f(_tx, n, P_KEYS[n])
	else:
		return _tx

# print(P_KEYS)

# _tx1 = c.sign(tx,0,priv_1)
# _tx2 = c.sign(_tx1,1,priv_1)
# _tx3 = c.sign(_tx2,2,priv_1)
# _tx4 = c.sign(_tx3,3,priv_2)
# tx3 = c.sign(tx2,2,priv_3)

_tx4 = f(tx, 0)
# print(_tx4)
tx4 = serialize(tx)
print(tx4)
transaction_id = c.pushtx(tx4)
print(transaction_id)
# #-------------------------------------------------------------------------------------

# def test_var_args( privkey, *args, change_addr=None, segwit=False, addr=None):
#     print (privkey)
#     print(change_addr)
#     tv, fee = args[:-1], int(args[-1])
#     print(tv)
#     print(fee)
#     for arg in args:
#         print ("another arg through *argv :", arg)

# d1 = 'yasoob'
# v1 = 1
# d2 = 'python'
# v2 = 2
# x = f'{d1}:{v1}'
# y = f'{d2}:{v2}'
# send = ['adfadfdsaf1321312:100000', 'GCGUHPNOCTJVOC7YQUX6UG6L3S6PUPHNZJFEVSVSP56GJIC7OBCNXHUA:090909']
# # test_var_args('private_key',f'{d1}:{v1}',f'{d2}:{v2}','10000',change_addr = 'change_addr')
# test_var_args('private_key',*send,'10000',change_addr = 'change_addr')

# lis = []
# def make(address,amount):
# 	lis.append(address + ":" + amount)
# 	print(lis)
	
# make('adfadfdsaf1321312','100000')
# make('GCGUHPNOCTJVOC7YQUX6UG6L3S6PUPHNZJFEVSVSP56GJIC7OBCNXHUA','090909')

