from boilerpipe.extract import Extractor
import re

# def extractfromHTML(filename):
f = open("embassyit.html", 'r',encoding = 'utf-8')
data_file = f.read()
# print(type(data_file)) #str type

extractor1 = Extractor(extractor='KeepEverythingExtractor', html=data_file)
content = extractor1.getText()
print(content) #str type

#Email #^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$
email = re.findall(r'\w+@\w+\.\w+',content)
print(email)

#Phone numers

# phone_regex = re.findall(r'[\+]?\d?[\s-]?\(?(\d{3})\)?[\s-]?\d{3}[\s-]?\d{4}',content)
# phone_regex = re.findall(r'[\+]{1}.*\d$',content)
# phone_regex = re.findall(r'(?:[\+]?(\d{1})?[\s-]?\(?(\d{3})\)?[\s-]?)?(\d{3})[\s-]?(\d{4})[\s-]?',content)
#+1 972 200 5599
phone_regex = re.findall(r'[\+]{1}\(?\d.*',content)

print("phone number", phone_regex)

#google
google_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?google.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_file)
print(google_regex)

#Twitter
twitter_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?twitter.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_file)
print(twitter_regex)

#Linkedin
linkedin_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?linkedin.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_file)
print(linkedin_regex)

#FourSquare
foursquare_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?foursquare.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_file)
print(foursquare_regex)

#Facebook
Facebook_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?facebook.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_file)
print(Facebook_regex)

#Github
github_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?github.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_file)
print(github_regex)

#Instagram
Instagram_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?instagram.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_file)
print(Instagram_regex)

#google_plus
google_plus_regex = re.findall(r'(?:(?:http|https):\/\/)?(?:www.)?plus.google.(?:com|in)\/[A-z 0-9 \/ \. ^=:]*',data_file)
print(google_plus_regex)
