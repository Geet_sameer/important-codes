import tldextract
import uuid
import pandas as pd
import pymongo
import time
from pymongo.errors import DuplicateKeyError
import urllib.request
from datetime import datetime
from dateutil.parser import parse
import json
import ast
import yaml
import phoenixdb
import datetime
from database import *
import seolib as seo
import pprint


print("Start : %s" % time.ctime())
# time.sleep(5)

client = pymongo.MongoClient('127.0.0.1', 39898)
db = client['Mattermark']


company_data_jsons = list(db['companies_json'].find({'phoenix_status': {'$exists': False}},
                                                                               projection=dict(_id=False,
                                                                                               data=True,
                                                                                               link=True)).limit(1))
link = [i.get('link') for i in company_data_jsons]
db['companies_json'].update_many(filter={'link': {'$in': link}},
                                                            update={'$set': {'phoenix_status': 'crawling'}})


# This is to insert keywords for the company and key people of the company
# Before executing this file, make sure that datafox_phoenix_comp.py is done. (Mapping dependent on datafox)

print("retrieved", len(link))

def getPermalink(socialAccount):
	if 'https' in socialAccount:
	    https_query = "select reference_key from base_set where base_value like '%{val}%'".format(
	        val=socialAccount)
	    print(https_query)
	    https_result = dbStuff.singleQuery(query=https_query)
	    if https_result:
	        https_permalink = https_result.get('REFERENCE_KEY')
	        return https_permalink
	    else:
	        http = socialAccount.replace('https', 'http')
	        http_query = "select reference_key from base_set where base_value like '%{val}%'".format(val=http)
	        print(http_query)
	        http_result = dbStuff.singleQuery(query=http_query)
	        if http_result:
	            http_permalink = http_result.get('REFERENCE_KEY')
	            return http_permalink

	else:
	    http = socialAccount.replace('https', 'http')
	    http_query = "select reference_key from base_set where base_value like '%{val}%'".format(val=http)
	    print(http_query)
	    http_result = dbStuff.singleQuery(query=http_query)
	    if http_result:
	        http_permalink = http_result.get('REFERENCE_KEY')
	        return http_permalink

def get_unique_keywords(new_list,old_list):
	new_list = len(new_list)
	old_list = len(old_list)
	if(new_list > old_list):
		diff = set(new_list) - set(old_list)
	else:
		diff = set(old_list) - set(new_list)
	add_list = list(diff)
	return add_list

# print(data)

count = 0
dbStuff = PhoenixWrite()
perma_list = []
for data in company_data_jsons:

	col_link = data.get('link')
	json_data = data.get('data')
	data = json.loads(json_data)
	pprint.pprint(data)
	keywords = data.get('keywords')
	facebook = data.get('social').get('facebook.com')
	facebook_likes = data.get('social').get('facebook_likes')
	facebook_talking_about = data.get('social').get('facebook_talking_about')
	linkedin = data.get('social').get('linkedin.com')
	linkedin_follower_count = data.get('social').get('linkedin_follower_count')
	twitter = data.get('social').get('twitter.com')
	twitter_followers = data.get('social').get('twitter_followers')
	twitter_mentions = data.get('social').get('twitter_mentions')
	# GET THE DOMAIN AND CHECK FOR PERMALINK
	website = tldextract.extract(data.get('domain'))
	domain = '.'.join(website[1:3])
	# print(domain)
	dom_query = "select reference_key from base_set where base_value = '{val}'".format(val=domain)
	dom_result = dbStuff.runQuery(query=dom_query, select=True)
	# DOMAIN
	if dom_result:
		domain_permalink = dom_result.get('REFERENCE_KEY')
		# company_permalink = domain_permalink
		company_permalink = 'bluestem-brands'
		query = "select keywords from company_keywords where company_permalink = '{val}'".format(val=company_permalink)
		result = dbStuff.runQuery(query=query, select=True)
		if result and keywords:
			keys_from_db = [i.get('KEYWORDS') for i in result]
			add_list = get_unique_keywords(keys_from_db,keywords)
			for k in add_list:
				keywords_key_uuid = str(uuid.uuid4())
				keywords_dict = dict(keywords_key=keywords_key_uuid, company_permalink=company_permalink,keywords=k)
				dbStuff.upsert_one(keywords_dict, tableName='company_keywords')
		else:
			pass
	else:
		pass

	# SOCAIL LINKS
	twitter_permalink = ''
	linkedin_permalink = ''
	if len(dom_result) > 0:
	    pass
	else:
	    if linkedin:
	        linkedin_permalink = getPermalink(linkedin)
	        print('linkedin_permalink', linkedin_permalink)
	    if linkedin_permalink:
	        perma_list.append(linkedin_permalink)
	        print('linkedin is there')
	    else:
	        if twitter:
	            twitter_permalink = getPermalink(twitter)
	            perma_list.append(twitter_permalink)

	if len(perma_list) > 0:
		permalink = perma_list[0]
		company_permalink = permalink
		query = "select * from company_keywords where company_permalink = '{val}'".format(val=company_permalink)
		result = dbStuff.runQuery(query=query, select=True)
		if result and keywords:
			keys_from_db = [i.get('KEYWORDS') for i in result]
			add_list = get_unique_keywords(keys_from_db,keywords)
			for k in add_list:
				keywords_key_uuid = str(uuid.uuid4())
				keywords_dict = dict(keywords_key=keywords_key_uuid, company_permalink=company_permalink,keywords=k)
				dbStuff.upsert_one(keywords_dict, tableName='company_keywords')
		else:
			pass

	# Store other details in matt table
	# Get team and store it in matt_team table
	team = data.get('team').get('link')
	# Store team in matt_team table




	
