import pymongo

import tldextract
import uuid
import pandas as pd
import pymongo
import time
from pymongo.errors import DuplicateKeyError
import urllib.request
from datetime import datetime
from dateutil.parser import parse
import json
import ast
import yaml
import phoenixdb
import datetime
from database import *
import seolib as seo
import requests as req
import json
import ast
# from appscript import *
# safari = app("Safari")

client = pymongo.MongoClient('127.0.0.1', 39898)
db = client['elucify']
print("Start : %s" % time.ctime())
# for i in range(850000,860000):
# 	get_domain = 'https://www.elucify.com/lookup_company/?company_id=' + str(i)
# 	# get_json = 'https://www.elucify.com/lookup_contacts/?company_id='+str(i)+'&start=0&end=30&keyword=&_=1511481134822'
# 	db['all_links'].insert_one(document = {'url':get_domain,'c_id':str(i)})

company_data_jsons = list(db['all_links'].find({'phoenix_status': {'$exists': False}},
                                                                               projection=dict(_id=False,
                                                                                               url=True,
                                                                                               c_id=True)).limit(100))
link = [i.get('url') for i in company_data_jsons]
db['all_links'].update_many(filter={'url': {'$in': link}},
                                                            update={'$set': {'phoenix_status': 'crawling'}})

print("retrieved", len(link))


dbStuff = PhoenixWrite()
cookies = dict(
	# csrftoken='TUnnEuf8DL9R37HschgaU2t2PrhVhz5D',
	# elucifyClientId='2951501749638',
	sessionid='evugpuoebpnw7s37a9b06ul9h1864lbn'
) # We only need sessionid, not cookies or anything else
count = 0
for data in company_data_jsons:

	main_url = data.get('url')
	print(main_url)
	c_id = data.get('c_id')

	# second = req.get(url='https://www.elucify.com/lookup_contacts/?company_id=932294&start=0&end=30&keyword=&_=1511481134822',cookies=cookies)
	first = req.get(url = main_url,cookies=cookies)
	second_check_url = req.get(url ='https://www.elucify.com/lookup_contacts/?company_id='+c_id+'&start=0&end=30&keyword=&_=1511481134822',cookies=cookies)
	print(second_check_url)
	first_data = first.content
	first_data = json.dumps(first_data.decode('utf-8'))
	first_json_data = json.loads(ast.literal_eval(first_data))

	second_check = second_check_url.content
	second_check = json.dumps(second_check.decode('utf-8'))
	second_check_json_data = json.loads(ast.literal_eval(second_check))
	
	# if second_check_json_data.get('contacts'):
	# 	pass
	# else:
	# 	safari.make(new=k.document,with_properties={k.URL:"https://www.youtube.com/watch?v=RgKAFK5djSk&list=RDRgKAFK5djSk"})
	contacts_check_length = len(second_check_json_data.get('contacts'))
	print('length',contacts_check_length)
	if first_json_data.get('error') or contacts_check_length == 0:
		print('No emails')
		db['all_links'].update_one(filter={'url': main_url},update={'$set': {'phoenix_status': 'exception'}})
		pass
	else:
		if first_json_data.get('domain'):
			domain = first_json_data.get('domain')
		else:
			domain = ''
		if first_json_data.get('account_name'):
			company_name = first_json_data.get('account_name')
		else:
			company_name = ''
		if first_json_data.get('size_category'):
			company_size = first_json_data.get('size_category')
		else:
			company_size = ''
		if first_json_data.get('industry_groups'):
			industry_groups = first_json_data.get('industry_groups')
		else:
			industry_groups = ''
		if first_json_data.get('industry'):
			industry = first_json_data.get('industry')
		else:
			industry = ''

		if first_json_data.get('web_techs'):
			web_techs = first_json_data.get('web_techs')
		else:
			web_techs = ''

		if domain:
			# print(domain)
			start = 0
			end = 30
			elu_data = []
			try:
				while(True):
					# time.sleep(1)
					url = 'https://www.elucify.com/lookup_contacts/?company_id='+c_id+'&start='+str(start)+'&end='+str(end)+'&keyword=&_=1511481134822'
					print(url)
					second = req.get(url = 'https://www.elucify.com/lookup_contacts/?company_id='+c_id+'&start='+str(start)+'&end='+str(end)+'&keyword=&_=1511481134822',cookies=cookies)
					second_data = second.content
					second_data = json.dumps(second_data.decode('utf-8'))
					second_json_data = json.loads(ast.literal_eval(second_data))
					# print(second_json_data)
					if len(second_json_data.get('contacts')) > 0:
						
						contacts_length = len(second_json_data.get('contacts'))
						contacts = second_json_data.get('contacts')
						for i in contacts:
							email = i.get('email')
							linkedin_url = i.get('linkedin_url')
							name = i.get('name')
							title = i.get('title')
							confidence = i.get('confidence')
							main_data = dict(email = email,linkedin_url=linkedin_url,name=name,title=title,confidence=confidence)
							elu_data.append(main_data)
							# db['all_links_new'].insert_one(document = {'email':email,'linkedin_url':linkedin_url,'name':name,'title':title,})

					else:
						break
					start = end + 1;
					end = end + 30;
			except:
				print('DONE')
				pass
		else:
			pass
		# print('data',elu_data)
		# final_data = dict(c_id =c_id ,data=elu_data,domain=domain,company_name=company_name,industry_groups=industry_groups,industry=industry)
		# print(final_data)
		try:
			print('DATABASE')
			db['all_links_new'].insert_one(document={'c_id ':c_id ,'data':elu_data,'domain':domain,'company_name':company_name,'company_size':company_size,'industry_groups':industry_groups,'industry':industry,'web_techs':web_techs})
			db['all_links'].update_one(filter={'url': main_url},update={'$set': {'phoenix_status': 'inserted'}})
		except:
			print("exception",main_url)
			db['all_links'].update_one(filter={'url': main_url},update={'$set': {'phoenix_status': 'exception'}})
			# db['url_exceptions'].insert_one(document={'url': url})
	count = count + 1
	print('count',count)


print("End : %s" % time.ctime())


# i = 0
# start = 0
# end = 30
# while(i < 10):
# 	link = 'https://www.elucify.com/lookup_contacts/?company_id=844409&start='+str(start)+'&end='+str(end)+'&keyword=&_=1511481134822'
# 	start = end + 1
# 	end = end + 30
# 	i = i + 1
# 	print(link)