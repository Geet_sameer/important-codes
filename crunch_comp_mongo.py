import tldextract
import uuid
import pandas as pd
import pymongo
import time
from pymongo.errors import DuplicateKeyError
import urllib.request
from datetime import datetime
from dateutil.parser import parse
import json
import ast
import yaml
import boto
from boto.s3.key import Key
from boto.s3.connection import S3Connection
conn = S3Connection('AKIAJ2WSZQIHD7BQZKLA', 'qDyZ4451CEF1LlkcCPlyRfIU/Wp2K/fXYCTFu/ZV')
client = pymongo.MongoClient('127.0.0.1', 39898)
dbinhouse = client['inhouse']
db = client['CrunchbaseLinks']
print("Start : %s" % time.ctime())
time.sleep( 5 )
# cd /opt/crunchbase/crunchbase_json_mongo
# client = pymongo.MongoClient('127.0.0.1', 39898)
# db = client['CrunchbaseLinks']


company_data_jsons = list(db['Crunchbase_organization_Json_Unique_26Nov'].find({'db_status':{'$exists':False}},projection = dict(_id=False,json_data = True,link = True)).limit(2000))
link = [i.get('link') for i in company_data_jsons]
db['Crunchbase_organization_Json_Unique_26Nov'].update_many(filter={'link':{'$in':link}}, update={'$set':{'db_status':'crawling'}})

# for i in company_data_jsons:
# 	print(i.get('json_data'))

print("retrieved", len(link))
# with open('charwell.json',encoding = 'utf-8') as data_file:
# 	data = json.load(data_file)
	# print(data)

count = 0
for data in company_data_jsons:
	col_link = data.get('link')
	# print(link)
	data = data.get('json_data')
	# col_link = data.get('link')
	data = json.loads(data)
	# print(data.get('cards'))
	if data.get('properties'):
		if data.get('properties').get('identifier'):
			c_key = data.get('properties').get('identifier').get('uuid')

	if data.get('cards').get('overview_fields2'):
		if data.get('cards').get('overview_fields2').get('website'):
			if data.get('cards').get('overview_fields2').get('website').get('value'):
				website = data.get('cards').get('overview_fields2').get('website').get('value')
				ext = tldextract.extract(website)
				domain = '.'.join(ext[1:3])
				company_key_ascii = ''.join(str(ord(c.lower())) for c in domain)
				# c_key = data.get('properties').get('identifier').get('uuid')
				company_domain_key = str(uuid.uuid3(uuid.NAMESPACE_X500, str(company_key_ascii)))
		else:
			website = ''
			ext = ''
			domain = ''
			# c_key = ''
			company_key_ascii = ''
			company_domain_key = ''
	else:
		website = ''
		ext = ''
		domain = ''
		# c_key = ''
		company_key_ascii = ''
		company_domain_key = ''

	# FIRST BLOCK DONE I.E; PROPERTIES           
	if data.get('properties'):
		# print(data.get('properties'))
		if data.get('properties').get('identifier'):
			try:
				company_name = data.get('properties').get('identifier').get('value')
			except:
				company_name = ''
			try:
				permalink = data.get('properties').get('identifier').get('permalink')
			except:
				permalink = ''
			# company_logo = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_thumb,h_120,w_120,f_jpg,g_faces/' + str(data.get('properties').get('identifier').get('image_id'))
			try:	
				logo = data.get('properties').get('identifier').get('image_id').split('/')[1]
				main_logo = data.get('properties').get('identifier').get('image_id')
				image_id = str(uuid.uuid4()) + '.jpg'
				download_image = urllib.request.urlretrieve('https://crunchbase-production-res.cloudinary.com/image/upload/c_lpad,h_120,w_120,f_jpg/'+main_logo,image_id)
				our_logo = download_image[0]
				comp_logo = str(uuid.uuid4()) + '.' + logo.split('.')[1]
				logo_extension = logo.split('.')[1]
			except:
				logo_extension = ''
				logo = ''
				comp_logo = ''
				download_image = ''
				our_logo = ''
			# print("c_key ", c_key)
			bucket = conn.get_bucket('comp_logo')
			possible_key = bucket.get_key(c_key)
			if possible_key:
				pass
			else:
				k = Key(bucket)
				k.key = c_key
				k.set_contents_from_string(our_logo)
		else:
			company_name = ''
			logo_extension = ''
			comp_logo = ''
			logo = ''
			comp_log = ''
			download_image = ''
		
		if data.get('properties').get('short_description'):
			about_info = data.get('properties').get('short_description')
		else:
			about_info = ''
		# if data.get('properties').get('title'):
		# 	company_title = data.get('properties').get('title')
		# else:
		# 	company_title = ''
		# person_image = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_thumb,h_120,w_120,f_jpg,g_faces/' + str(data.get('properties').get('identifier').get('image_id'))
		# company_extra_roles = data.get('properties').get('facet_ids')
		# print(extra_roles)
		# print(company_extra_roles)
		# db['company'].insert_one(document={'company_name':company_Name,'company_image': company_image,'about_info':about_info})

	# COMPANY LOCATIONS                         
	if data.get('cards').get('overview_image_description'):
		try:
			company_locations = [i.get('value') for i in data.get('cards').get('overview_image_description').get('location_identifiers')]
			# db['company'].insert_one(document = {'company_locations':company_locations})
			try:
				state = company_locations[0].replace(' ','_').lower() 
			except:
				state = ''
			try:
				city = company_locations[1].replace(' ','_').lower() 
			except:
				city = ''
			try:
				country = company_locations[2].replace(' ','_').lower() 
			except:
				country = ''
			try:
				continent = company_locations[3].replace(' ','_').lower() 
			except:
				continent = ''
			# print(city)
		except:
			state = ''
			city = ''
			country = ''
			continent = ''


	# Company company_categories               
	cat_list = []
	if data.get('cards').get('overview_fields').get('categories'):
		categories = data.get('cards').get('overview_fields').get('categories')
		for cat in categories:
			company_category = cat_list.append(cat.get('permalink'))
		# db['company'].insert_one(document = {'company_categories':cat_list})
	# print(company_locations)

	# FOUNDERS
	
	founders_list = []
	if data.get('cards').get('overview_fields').get('founder_identifiers'):
		founders = data.get('cards').get('overview_fields').get('founder_identifiers')
		for cat in founders:
			company_founders = founders_list.append(cat.get('permalink'))
	# print(founders_list)

	# SOCIAL ACCOUNTS
	if data.get('cards').get('overview_fields2'):
		if data.get('cards').get('overview_fields2').get('facebook'):
			facebook = data.get('cards').get('overview_fields2').get('facebook').get('value')
		else:
			facebook = ''
		# print(facebook)
		if data.get('cards').get('overview_fields2').get('linkedin'):
			linkedin = data.get('cards').get('overview_fields2').get('linkedin').get('value')
		else:
			linkedin = ''
		# print(linkedin)
		if data.get('cards').get('overview_fields2').get('twitter'):
			twitter = data.get('cards').get('overview_fields2').get('twitter').get('value')
		else:
			twitter = ''
		# print(twitter)
		if data.get('cards').get('overview_fields2').get('website'):
			website = data.get('cards').get('overview_fields2').get('website').get('value')
		else:
			website = ''
		# print(website)

		if data.get('cards').get('overview_fields2').get('phone_number'):
			phone_number = data.get('cards').get('overview_fields2').get('phone_number')
		else:
			phone_number = ''


		if data.get('cards').get('overview_fields2').get('contact_email'):
			contact_email = data.get('cards').get('overview_fields2').get('contact_email')
		else:
			contact_email = ''
	else:
		facebook = ''
		linkedin = ''
		twitter = ''
		phone_number = ''
		contact_email = ''

	# print(contact_email)

	# overview_fields 
	if data.get('cards').get('overview_fields'):
		if data.get('cards').get('overview_fields').get('founded_on'):
			founded_on = data.get('cards').get('overview_fields').get('founded_on').get('value')
			# founded_on_date = [d.to_pydatetime() for d in pd.to_datetime(founded_on)]
			if founded_on:
				founded_on_date = datetime.strptime(founded_on, '%Y-%m-%d')
			else:
				founded_on_date = ''		
			
		else:
			founded_on = ''
			founded_on_date = ''
		# print(founded_on_date)
		if data.get('cards').get('overview_fields').get('operating_status'):
			operating_status = data.get('cards').get('overview_fields').get('operating_status')
		else:
			operating_status = ''
		# print(operating_status)
		if data.get('cards').get('overview_fields').get('num_employees_enum'):
			total_employees = data.get('cards').get('overview_fields').get('num_employees_enum').replace('c_0','').replace('_0','-').replace('c_','').replace('_max','+')
		else:
			total_employees = ''
		# print(total_employees)
		if data.get('cards').get('overview_fields').get('last_funding_type'):
			last_funding_type = data.get('cards').get('overview_fields').get('last_funding_type')
		else:
			last_funding_type = ''
		# print(last_funding_type)
		if data.get('cards').get('overview_fields').get('funding_stage'):
			funding_stage = data.get('cards').get('overview_fields').get('funding_stage')
		else:
			funding_stage = ''
	else:
		funding_stage = ''
		last_funding_type = ''
		total_employees = ''
		operating_status = ''
		founded_on = ''
		founded_on_data = ''



	# IPO FIELDS
	if data.get('cards').get('ipo_fields'):
		if data.get('cards').get('ipo_fields').get('ipo_amount_raised'):
			ipo_amount_raised_usd = data.get('cards').get('ipo_fields').get('ipo_amount_raised').get('value_usd')
			currency = data.get('cards').get('ipo_fields').get('ipo_amount_raised').get('currency')
		else:
			ipo_amount_raised_usd = ''
			currency = ''
		# print(ipo_amount_raised)
		# print(currency)
		if data.get('cards').get('ipo_fields').get('went_public_on'):
			went_public_on = data.get('cards').get('ipo_fields').get('went_public_on')
			# went_public_on_date = [d.to_pydatetime() for d in pd.to_datetime(went_public_on)]
			if went_public_on:
				went_public_on_date = datetime.strptime(went_public_on, '%Y-%m-%d')
			else:
				went_public_on_date = ''
		else:
			went_public_on = ''
			went_public_on_date = ''
		# print(went_public_on)

		if data.get('cards').get('ipo_fields').get('stock_link'):
			stock_link_label = data.get('cards').get('ipo_fields').get('stock_link').get('label')
			stock_link_value = data.get('cards').get('ipo_fields').get('stock_link').get('value')
		else:
			stock_link_label = ''
			stock_link_value = ''
		# print(stock_link_label)
		# print(value)

		if data.get('cards').get('ipo_fields').get('ipo_valuation'):
			ipo_valuation_usd = data.get('cards').get('ipo_fields').get('ipo_valuation').get('value_usd')
			currency = data.get('cards').get('ipo_fields').get('ipo_valuation').get('currency')
		else:
			ipo_valuation_usd = ''
			currency = ''
		# print(ipo_valuation)
		# print(currency)

		if data.get('cards').get('ipo_fields').get('ipo_share_price'):
			ipo_share_price_usd = data.get('cards').get('ipo_fields').get('ipo_share_price').get('value_usd')
			currency = data.get('cards').get('ipo_fields').get('ipo_share_price').get('currency')
		else:
			ipo_share_price_usd = ''
			currency = ''
	else:
		ipo_amount_raised_usd = ''
		ipo_share_price_usd = ''
		ipo_valuation_usd = ''
		stock_link_label = ''
		currency = ''
		value = ''
		went_public_on_date = ''
		went_public_on = ''
		stock_link_value = ''


	# IF DOMAIN NOT PRESENT , CHECK FROM CONTACT_EMAIL
	try:
		if company_domain_key:
			pass
		else:
			domain = contact_email.split('@')[1]
			company_key_ascii = ''.join(str(ord(c.lower())) for c in domain)
			company_domain_key = str(uuid.uuid3(uuid.NAMESPACE_X500, str(company_key_ascii)))
	except:
		pass

	if data.get('cards').get('overview_investor_fields'):
		investor_type = data.get('cards').get('overview_investor_fields').get('investor_type')
	else:
		investor_type = ''

	if data.get('properties'):
		company_other_roles = data.get('properties').get('facet_ids')
	else:
		company_other_roles = ''

	sub_orgs = []
	if data.get('cards').get('sub_organizations_image_list'):
		orgs = data.get('cards').get('sub_organizations_image_list')
		for org in orgs:
			comp_sub_orgs = sub_orgs.append(org.get('ownee_identifier').get('permalink'))

	exits_list = []
	if data.get('cards').get('exits_image_list'):
		exits = data.get('cards').get('exits_image_list')
		for ex in exits:
			comp_exits = exits_list.append(ex.get('identifier').get('permalink'))


	if data.get('cards').get('owner_identifier'):
		company_owned_by = data.get('cards').get('owner_identifier').get('permalink')
	else:
		company_owned_by = ''

	if data.get('cards').get('overview_company_fields'):
		company_type = data.get('cards').get('overview_company_fields').get('company_type')
	else:
		company_type = ''

	

	# INCLUDE THIS
	# db['company_domains'].insert_one(document = {'company_name':company_name,'company_permalink':permalink,'domain':domain,'company_key':company_key,'company_logo':company_logo})


	# INCLUDE THIS
	try:
		dbinhouse['company_data'].insert_one(document = {'c_key':c_key,'company_domain_key':company_domain_key,'company_name':company_name,'company_founders':founders_list,'permalink':permalink,'domain':domain,'crunch_logo':logo,'company_logo':comp_logo,'our_logo':our_logo,'about_company':about_info, 
			'company_categories':cat_list,'state':state,'city':city,'country':country,'continent':continent,'company_facebook_link':facebook,'company_linkedin_link':linkedin,
			'company_twitter_link':twitter,'company_website':website,'company_email':contact_email,'company_phone_number':phone_number,'founded_on':founded_on_date,
			'operating_status':operating_status,'total_employees':total_employees,'last_funding_type':last_funding_type,'funding_stage':funding_stage,
			'ipo_amount_raised_usd':ipo_amount_raised_usd,'went_public_on':went_public_on_date,'stock_symbol':stock_link_label,'stock_link_value':stock_link_value,'ipo_valuation_usd':ipo_valuation_usd,
			'ipo_share_price_usd':ipo_share_price_usd,'investor_type':investor_type,'company_other_roles':company_other_roles,'sub_organizations':sub_orgs,'exits':exits_list,'company_owned_by':company_owned_by,'company_type':company_type})
		dbinhouse['img_table'].insert_one(document = {'c_key':c_key,'unique':comp_logo,'crunch_logo':logo,'ext':logo_extension,'source':'crunchbase','status':'enable','host_provider':'S3','bucket_name':'comp_logo','bucket_key':c_key,'company_name':company_name,'our_logo':our_logo})
		db['Crunchbase_organization_Json_Unique_26Nov'].update_one(filter = {'link':col_link},update = {'$set':{'db_status':'inserted'}})

	except Exception as e:
		print('link got Exception',col_link)
		dbinhouse['comp_exceptions'].insert_one(document = {'link':col_link})
		db['Crunchbase_organization_Json_Unique_26Nov'].update_one(filter={'link': col_link}, update={'$unset': {'db_status': ''}})
	
	# db['Crunchbase_organization_Json_Unique_26Nov'].update_one(filter = {'link':col_link},update = {'$set':{'db_status':'inserted'}})

	# count = count + 1
	# print(count)

print("End : %s" % time.ctime())






