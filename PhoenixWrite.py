import phoenixdb
# from queries import moreSocial


class PhoenixWrite(object):

    def __init__(self):
        super(PhoenixWrite, self).__init__()
        database_url = 'http://devmike.trioncube.com:8765'
        connection = phoenixdb.connect(database_url, autocommit = True)
        self.cursor = connection.cursor(cursor_factory=phoenixdb.cursor.DictCursor)

    def writeValues(self, d):
        values = ""
        columns = []
        for key,value in d.items():
            columns.append(key)
            if isinstance(value, str):
                values = values + "'{}',".format(value)
            elif value is None:
                values = values + str('Null') + ','
            else:
                values = values + str(value) + ','
        return dict(
            values = values[:-1],
            columns = columns
        )

    def upsert_one(self, columnsValuesDict, tableName = None, schemaName = 'CUBERSET'):
        q = self.writeValues(d=columnsValuesDict)
        if tableName is not None:
            query = "UPSERT INTO {table}({columns}) VALUES({values})".\
                format(table=tableName, columns=','.join(q.get('columns')), values=q.get('values'))
        else:
            query = False

        # self.runQuery(query=query)

        return query

    def upsert_many(self, columns, valueList, tableName = None, schemaName = 'CUBERSET'):
        pass

    def exists(self, value = None, column = None, table = None, schema = 'CUBERSET'):
        if value and column and table:
            if isinstance(value, str):
                query = "SELECT {col} FROM {tab} WHERE {col} = '{val}'".format(col=column, tab=table, val=value)
            else:
                query = "SELECT {col} FROM {tab} WHERE {col} = {val}".format(col=column, tab=table, val=value)
            res = self.runQuery(query, select=True, fetchOne=True)

            if res:
                return True
            else:
                return False
        else:
            raise ValueError('Value, Column or Table not provided')

    def create(self, tableName = None, columnTypeDict = None, primaryKeysList = None, schemaName = 'CUBERSET', salt = 100):
        if tableName and columnTypeDict and primaryKeysList and schemaName:
            if not set(primaryKeysList).issubset(set(columnTypeDict.keys())):
                raise Exception('Primary keys {} should be present in column names {}'
                                .format(primaryKeysList, list(columnTypeDict.keys())))
            col = []
            for val in columnTypeDict.items():
                col.append("{} {}".format(val[0],val[1]))
            columns = ','.join(col)
            primaryKeys = ','.join(primaryKeysList)
            query = "CREATE TABLE IF NOT EXISTS {table}({columns} " \
                    "CONSTRAINT pk PRIMARY KEY ({primaryKeys})) SALT_BUCKETS = {salt}".format(
                     table=tableName, columns=columns, primaryKeys=primaryKeys, salt = salt
            )
            self.runQuery(query=query)
            return
        else:
            raise ValueError("tableName, columns and primaryKeys can't be Null")

    def delete(self, tab):
        query = "DROP TABLE CUBERSET.{t}".format(t=tab)
        self.runQuery(query=query)

    def runQuery(self, query, select = False, fetchOne = False):
        if query:
            # print(query)
            self.cursor.execute(operation=query)
            if select:
                if fetchOne:
                    res = self.cursor.fetchone()
                else:
                    res = self.cursor.fetchall()
                return res
            else:
                return

if __name__ == "__main__":
    # print(len(ALL_TABLES.keys()))
    ob = PhoenixWrite()
    # for i in ALL_TABLES.values():
    #     # print(i.get('table'))
    #     try:
    #         ob.delete(tab=i.get('table'))
    #     except:
    #         pass
    #     print(i.get('table'))

    # print(ob.exists(column='firstName', table='people_data', value='Bill'))
    # for idx,i in enumerate(ALL_TABLES):
    #     ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),
    #               primaryKeysList=i.get('primary'))

    # # ADD FUNDING TABLE, INVESTORS TABLE AND ACQUISITIONS TABLE FOR COMPANY

    # i = dict(
    #     table='base_set',
    #     primary=['base_key'],
    #     cols=dict(
    #         base_key='VARCHAR',
    #         base_type='VARCHAR',
    #         base_value='VARCHAR',
    #         created_date='DATE',
    #         updated_date='DATE',
    #         email='VARCHAR',
    #         username = 'VARCHAR',
    #         hostname = 'VARCHAR',
    #         email_head = 'VARCHAR',
    #         domain='VARCHAR',
    #         sources_found = 'VARCHAR',
    #         twitter='VARCHAR',
    #         linkedin='VARCHAR',
    #         facebook='VARCHAR',
    #         phone_no = 'VARCHAR',
    #         data_source ='VARCHAR',
    #         data_type = 'VARCHAR',
    #         reference_key = 'VARCHAR'
    #     )
    # )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))
    # #
    # #
    # i = dict(
    #     table='company_data',
    #     primary=['company_permalink'],
    #     cols=dict(
    #         company_permalink = 'VARCHAR',
    #         company_key='VARCHAR',
    #         company_name='VARCHAR',
    #         domain='VARCHAR',
    #         company_logo='VARCHAR',
    #         company_description='VARCHAR',
    #         location='VARCHAR',
    #         website='VARCHAR',
    #         company_email='VARCHAR',
    #         founded_on='DATE',
    #         total_employees='VARCHAR',
    #         phone_no='VARCHAR',
    #         operating_status='VARCHAR',
    #         company_type='VARCHAR',
    #         company_twitter='VARCHAR',
    #         company_facebook='VARCHAR',
    #         company_linkedin='VARCHAR',
    #         alexa_ranking='INTEGER',
    #         investor_type = 'VARCHAR',
    
    #         company_owned_by = 'VARCHAR',
    #         company_registration_location='VARCHAR',
    #         company_registration_name='VARCHAR',
    #         company_identification_number='VARCHAR',
    #         company_registered_date='DATE',
    #         whois_registered_info='VARCHAR',
    #         last_funding_type='VARCHAR',
    #         funding_stage='VARCHAR',
    #         ipo_amount_raised_usd='BIGINT',
    #         went_public_on='DATE',
    #         stock_symbol = 'VARCHAR',
    #         stock_link_value = 'VARCHAR',
    #         ipo_valuation_usd = 'BIGINT'
    #     )
    # )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))
    # i = dict(
    #     table = 'company_sub_organizations',
    #     primary = ['company_permalink'],
    #     cols = dict(
    #         company_permalink = 'VARCHAR',
    #         sub_orgs = 'VARCHAR'
    #     )
    # )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'), primaryKeysList=i.get('primary'))
    # i = dict(
    #     table = 'company_other_roles',
    #     primary = ['company_permalink'],
    #     cols = dict(
    #         company_permalink = 'VARCHAR',
    #         other_roles = 'VARCHAR'
    #     )
    # )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'), primaryKeysList=i.get('primary'))


    # i = dict(
    #     table = 'company_investor_type',
    #     primary = ['company_permalink'],
    #     cols = dict(
    #         company_permalink = 'VARCHAR',
    #         investor_type = 'VARCHAR'
    #         )
    #     )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'), primaryKeysList=i.get('primary'))
    
    # i = dict(
    #     table = 'people_data',
    #     primary = ['people_permalink'],
    #     cols = dict(
    #         people_permalink = 'VARCHAR',
    #         people_key = 'VARCHAR',
    #         people_name = 'VARCHAR',
    #         people_logo = 'VARCHAR',
    #         people_description = 'VARCHAR',
    #         people_email = 'VARCHAR',
    #         people_facebook = 'VARCHAR',
    #         people_twitter = 'VARCHAR',
    #         people_linkedin = 'VARCHAR',
    #         people_github = 'VARCHAR',
    #         website = 'VARCHAR',
    #         gender = 'VARCHAR',
    #         investor_type = 'VARCHAR[]',
    #         investor_stage = 'VARCHAR[]',
    #         other_roles = 'VARCHAR[]'
            
    #         )
    
    #     )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))
    
    # i = dict(
    #     table = 'role',
    #     primary = ['role_key'],
    #     cols = dict(
    #         role_key = 'VARCHAR',
    #         people_key = 'VARCHAR',
    #         people_permalink = 'VARCHAR',
    #         current_positon = 'VARCHAR',
    #         current_company = 'VARCHAR',
    #         start_date = 'DATE',
    #         end_date = 'DATE'
    #         )
    #     )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))

    # i = dict(
    #     table = 'current_employees',
    #     primary = ['current_employees_key'],
    #     cols = dict(
    #         current_employees_key = 'VARCHAR',
    #         company_key = 'VARCHAR',
    #         company_permalink = 'VARCHAR',
    #         domain = 'VARCHAR',
    #         people_name = 'VARCHAR',
    #         people_permalink = 'VARCHAR',
    #         primary_role = 'VARCHAR',
    #         start_date = 'DATE',
    #         end_date = 'DATE',
    #         title = 'VARCHAR'
    
    #         )
    #     )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))
    
    # i = dict(
    # table = 'past_employees',
    # primary = ['past_employees_key'],
    # cols = dict(
    #     past_employees_key = 'VARCHAR',
    #     company_key = 'VARCHAR',
    #     company_permalink = 'VARCHAR',
    #     unique_key = 'VARCHAR',
    #     domain = 'VARCHAR',
    #     people_name = 'VARCHAR',
    #     people_permalink = 'VARCHAR',
    #     primary_role = 'VARCHAR',
    #     start_date = 'DATE',
    #     end_date = 'DATE',
    #     title = 'VARCHAR'
    
    #     )
    # )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))
    
    # i = dict(
    #     table = 'demographics',
    #     primary = ['demographics_key'],
    #     cols = dict(
    #         demographics_key = 'VARCHAR',
    #         location = 'VARCHAR',
    #         state = 'VARCHAR',
    #         city = 'VARCHAR',
    #         country = 'VARCHAR',
    #         continent = 'VARCHAR',
    #         data_type = 'VARCHAR',
    #         people_key = 'VARCHAR',
    #         company_key = 'VARCHAR',
    #         people_permalink = 'VARCHAR',
    #         company_permalink = 'VARCHAR',
    #         age_range = 'VARCHAR',
    #         age = 'INTEGER',
    #         gender = 'VARCHAR'
    #         )
    
    #     )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))
    
    # i = dict(
    #     table = 'image_table',
    #     primary = ['image_key'],
    #     cols = dict(
    #         image_key = 'VARCHAR',
    #         people_key = 'VARCHAR',
    #         company_key = 'VARCHAR',
    #         people_permalink = 'VARCHAR',
    #         company_permalink = 'VARCHAR',
    #         data_type = 'VARCHAR',
    #         extension = 'VARCHAR',
    #         source = 'VARCHAR',
    #         status = 'VARCHAR',
    #         host_provider = 'VARCHAR',
    #         bucket_name = 'VARCHAR',
    #         bucket_key = 'VARCHAR',
    #         type = 'VARCHAR',
    #         our_logo = 'VARCHAR'
    
    #         )
    #     )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))
    
    # i = dict(
    #     table = 'education',
    #     primary = ['education_key'],
    #     cols = dict(
    #         education_key = 'VARCHAR',
    #         people_key = 'VARCHAR',
    #         people_permalink = 'VARCHAR',
    #         company_key = 'VARCHAR',
    #         education_institute = 'VARCHAR',
    #         education_institute_permalink = 'VARCHAR',
    #         degree = 'VARCHAR',
    #         subject = 'VARCHAR',
    #         title = 'VARCHAR',
    #         completed_on = 'DATE'
    
    
    #         )
    #     )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))
    
    # i = dict(
    #     table = 'sources_found',
    #     primary = ['source_key'],
    #     cols = dict(
    #         source_key = 'VARCHAR',
    #         people_key = 'VARCHAR',
    #         company_key = 'VARCHAR',
    #         people_permalink = 'VARCHAR',
    #         company_permalink = 'VARCHAR',
    #         source_url = 'VARCHAR',
    #         host_name = 'VARCHAR',
    #         title = 'VARCHAR'
    
    #         )
    #     )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))
    
    i = dict(
        table = 'product',
        primary = ['product_key'],
        cols = dict(
        product_key = 'VARCHAR',
        company_permalink = 'VARCHAR',
        product_name = 'VARCHAR',
        product_features = 'VARCHAR[]',
        product_feature_name = 'VARCHAR',
        deployment = 'VARCHAR[]',
        domain = 'VARCHAR',
        about_note = 'VARCHAR',
        launched = 'DATE',
        blog_url = 'VARCHAR'
        ))
    ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))
    
    # i = dict(
    #     table = 'categories',
    #     primary = ['category_key'],
    #     cols = dict(
    #     category_key = 'VARCHAR',
    #     company_key = 'VARCHAR',
    #     company_permalink = 'VARCHAR',
    #     categories = 'VARCHAR'
    #     )
    #     )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))
    # #
    # i = dict(
    #     table = 'founders',
    #     primary = ['founders_key'],
    #     cols = dict(
    #     founders_key = 'VARCHAR',
    #     company_permalink = 'VARCHAR',
    #     people_key = 'VARCHAR',
    #     people_permalink = 'VARCHAR',
    #     people_name = 'VARCHAR'
    #     )
    #     )
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))

    #
#     socialAndOtherCompany = dict(
#     table='emailPeople',
#     primary=['peopleKey','domain'],
#     cols=dict(
#         peopleKey='VARCHAR',
#         email='VARCHAR',
#         email_extension='VARCHAR',
#         domain='VARCHAR',
#         type='VARCHAR',
#     )
# # )
#     for i in ALL_TABLES.values():
#         try:
    # print("len is :", len(moreSocial))
    # for ind,i in enumerate(moreSocial):
    # ob.create(tableName=i.get('table'), columnTypeDict=i.get('cols'),primaryKeysList=i.get('primary'))

        # query1 = "ALTER TABLE {tab} DROP COLUMN varsrname".format(tab=i.get('table'))
        # query2 = "ALTER TABLE {tab} ADD username VARCHAR".format(tab=i.get('table'))
        # query2 = "ALTER TABLE SOCIALANDOTHERCOMPANY ADD {tab} VARCHAR".format(tab=i.get('table'))
        # ob.runQuery(query=query1)
        # ob.runQuery(query=query2)
        # print(i.get('table'), ind + 1)
        # print(i.get('table'),ind)
#             print(i.get('table'))
#         except:
#             print("Falied --> ",i.get('table'))
    # print(ob.upsert_one(tableName='helloTable', columnsValuesDict=d))