import tldextract
import uuid
import pandas as pd
import pymongo
import time
from pymongo.errors import DuplicateKeyError
import urllib.request
from datetime import datetime
from dateutil.parser import parse
import json
import ast
import yaml
import phoenixdb
import datetime
from database import *
import seolib as seo
import pprint



print("Start : %s" % time.ctime())
# time.sleep(5)

client = pymongo.MongoClient('127.0.0.1', 39898)
db = client['sifter']


company_data_jsons = list(db['all_links_companies'].find({'phoenix_status': {'$exists': False}},
                                                                               projection=dict(_id=False,
                                                                                               content=True,
                                                                                               url=True)).limit(1))
link = [i.get('url') for i in company_data_jsons]
db['all_links_companies'].update_many(filter={'url': {'$in': link}},
                                                            update={'$set': {'phoenix_status': 'crawling'}})


# Get the permalink with domain
# for tech stack of the company first we have to execute this file.

print("retrieved", len(link))

dbStuff = PhoenixWrite()
for data in company_data_jsons:
	col_link = data.get('url')
	json_data = data.get('content')
	# pprint.pprint(json_data)

	# Get domain and get company_permalink from db
	if json_data.get('content').get('company').get('domain'):
		website = tldextract.extract(json_data.get('content').get('company').get('domain'))
		domain = '.'.join(website[1:3])
	# print(domain)
	products = json_data.get('content').get('products')
	pprint.pprint(products)
	

	dom_query = "select reference_key from base_set where base_value = '{val}'".format(val=domain)
	dom_result = dbStuff.runQuery(query=dom_query, select=True)
	if dom_result and products:
		# domain is there
		# if both domain and products are there, then only if block executes...
		domain_permalink = dom_result.get('REFERENCE_KEY')
		for key,value in products.items():
			stack_name = key
			stack_desc = value.get('description')
			# Store in tech_stack table

