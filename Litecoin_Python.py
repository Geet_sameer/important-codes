from cryptos import *

# Have to figure out the transaction size and also the fee based on transaction size
# Transaction fee calculation  ---> size = (no.of.inputs * 148 + no.of.outputs * 34) + 10
# Then call this api to get the various transaction fees per byte---> https://bitcoinfees.earn.com/api/v1/fees/recommended
# { "fastestFee": 40, "halfHourFee": 20, "hourFee": 10 }
# fee = size * halfHourFee
l = Litecoin(testnet=True)
priv = '107e9aaba95b1bcba732cb269588fb8762ba4f6018e0526818832bbe139b1317'
my_addr = 'msjK2Az65fzqzUDA6yB6QeNi42ssUAxG93'
destination = 'myZVSymfEZgW84YCairtHM7crcMgsaEAG3'
amount_to_send = 1000000
inputs = l.unspent(my_addr)
# inputs[0]['segwit']=True
print(inputs)
input_list = []
total_unspent_value = 0
tx_fee = 10000

for i in inputs:
    if (i.get('value') <= amount_to_send or i.get('value') >= amount_to_send):
        total_unspent_value += i.get('value')
        input_list.append(i)
        if (total_unspent_value > amount_to_send):
            break
print('unspent', total_unspent_value)
print('input_list', input_list)
get_amount = total_unspent_value - amount_to_send
withdraw_amount = amount_to_send - tx_fee;
print(get_amount)
print(withdraw_amount)
#
outs = [{'value': withdraw_amount, 'address': destination}, {'value': get_amount, 'address': my_addr}]
#
tx = l.mktx(input_list, outs)
print(tx)
sign = l.sign(tx,0,priv)
# for index, j in enumerate(input_list):
#     c.sign(tx, index, priv)  # can use signall method also
tx4 = serialize(tx)
print(tx4)
transaction_id = l.pushtx(tx4)
print(transaction_id)
