from cryptos import *
# Have to figure out the transaction size and also the fee based on transaction size
# Transaction fee calculation  ---> size = (no.of.inputs * 148 + no.of.outputs * 34) + 10 
# Then call this api to get the various transaction fees per byte---> https://bitcoinfees.earn.com/api/v1/fees/recommended
# { "fastestFee": 40, "halfHourFee": 20, "hourFee": 10 }
# fee = size * halfHourFee
c = Bitcoin(testnet=True)
priv = 'cVPFWHcwALpHHLbMUDpNfokh8UnYujgoNA3XPGjvGXb8QDBvzdyw'
btc_addr = 'mzCcYe6WzrEX1FpF2a18pqtCJU9zYE4xig'
destination = 'muiPT6YFkAALUATXR5b9dLPgh9VBnqUy23'
# print(c.unspent(btc_addr))
amount_to_send = 213244094
inputs = c.unspent(btc_addr)
input_list = []
total_unspent_value = 0
tx_fee = 10000
for i in inputs:
	if (i.get('value') <= amount_to_send or i.get('value') >= amount_to_send):
		total_unspent_value += i.get('value')
		input_list.append(i)
		if (total_unspent_value > amount_to_send):
			break
print('unspent',total_unspent_value)
print('input_list', input_list)
get_amount = total_unspent_value - amount_to_send
withdraw_amount = amount_to_send - tx_fee;
# print(get_amount)
# print(withdraw_amount)

# can use this also
# tx = c.preparesignedtx(privkey=priv,to=destination,value=amount_to_send,fee=tx_fee,change_addr=btc_addr,addr=btc_addr)

outs = [{'value':withdraw_amount,'address':destination},{'value':get_amount,'address':btc_addr}]
tx = c.mktx(input_list,outs)
# print(tx)
for index,j in enumerate(input_list):
	c.sign(tx,index,priv)     # can use signall method also
tx4 = serialize(tx)
transaction_id = c.pushtx(tx4)

# outs = [{'value':amount_to_send,'address':'muiPT6YFkAALUATXR5b9dLPgh9VBnqUy23'}]

# tx = c.mktx(inputs,outs)
# print(tx)

# tx2 = c.sign(tx,0,priv)

# print(tx2)