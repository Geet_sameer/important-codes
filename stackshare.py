import tldextract
import uuid
import pandas as pd
import pymongo
import time
from pymongo.errors import DuplicateKeyError
import urllib.request
from datetime import datetime
from dateutil.parser import parse
import json
import ast
import yaml
import phoenixdb
import datetime
from database import *
import seolib as seo
import pprint

print("Start : %s" % time.ctime())
# time.sleep(5)

client = pymongo.MongoClient('127.0.0.1', 39898)
db = client['stackshare']


company_data_jsons = list(db['all_links_companies'].find({'phoenix_status': {'$exists': False}},
                                                                               projection=dict(_id=False)).limit(1))
link = [i.get('url') for i in company_data_jsons]
db['all_links_companies'].update_many(filter={'url': {'$in': link}},
                                                            update={'$set': {'phoenix_status': 'crawling'}})	


# Get the permalink with domain

# Make sure sif.py is executed before running this file...mapping only after sif.py is done.

print("retrieved", len(link))
dbStuff = PhoenixWrite()

for data in company_data_jsons:
	# pprint.pprint(data)
	keywords = data.get('keywords')
	tech_stack = data.get('tech_stack')
	domain = data.get('domain')
	dom_query = "select reference_key from base_set where base_value = '{val}'".format(val=domain)
	dom_result = dbStuff.runQuery(query=dom_query, select=True)
	if dom_result:
		# domain is there    		
		# get the tech_stack from table and check with the new tech_stack
		# insert only the unique tech_stack.....for reference : go to "get_unique_keywords" method in matt_comp_phoenix.py 
		# insert tech_stack in tech_stack table
		# same process for keywords also.... 




