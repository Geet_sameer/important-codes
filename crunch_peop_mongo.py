import tldextract
import uuid
import pandas as pd
import pymongo
import time
from pymongo.errors import DuplicateKeyError
import urllib.request
from datetime import datetime
from dateutil.parser import parse
import json
import ast
import yaml
import boto
from boto.s3.key import Key
from boto.s3.connection import S3Connection
conn = S3Connection('AKIAJ2WSZQIHD7BQZKLA', 'qDyZ4451CEF1LlkcCPlyRfIU/Wp2K/fXYCTFu/ZV')

# cd /opt/crunchbase/crunchbase_json_mongo

client = pymongo.MongoClient('127.0.0.1', 39898)
db = client['inhouse']

poeple_data_jsons = list(db['Crunchbase_People_Json_Unique1'].find(projection = dict(_id=False,json_data = True)).limit(1))

# with open('justin-waldron.json',encoding = 'utf-8') as data_file:
# 	data = json.load(data_file)
	
for data in poeple_data_jsons:
	# people_key = str(uuid.uuid4()) + str(uuid.uuid4())



	if data.get('properties'):
		if data.get('properties').get('identifier'):
			p_key = data.get('properties').get('identifier').get('uuid')
	# print(p_key)

	# FIRST BLOCK DONE I.E; PROPERTIES
	if data.get('properties'):
		# print(data.get('properties'))
		if data.get('properties').get('identifier'):
			# p_key = data.get('properties').get('identifier').get('uuid')
			people_name = data.get('properties').get('identifier').get('value')
			people_name_permalink = data.get('properties').get('identifier').get('permalink')
			# person_image = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_thumb,h_120,w_120,f_jpg,g_faces/' + str(data.get('properties').get('identifier').get('image_id'))
		else:
			people_name = ''
			# person_image = ''
			permalink = ''
		if data.get('properties').get('short_description'):
			about_info = data.get('properties').get('short_description')
		else:
			about_info = ''
		# if data.get('properties').get('title'):
		# 	current_company = data.get('properties').get('title').strip().split('@')[-1]
		# 	pos = data.get('properties').get('title')
		# 	current_position=pos[pos.find("-")+1:pos.find("@")]
			
		# else:
		# 	current_company = ''
		# 	current_position = ''
		# person_image = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_thumb,h_120,w_120,f_jpg,g_faces/' + str(data.get('properties').get('identifier').get('image_id'))
		if data.get('properties').get('facet_ids'):
			extra_roles = data.get('properties').get('facet_ids')
		else:
			extra_roles = ''

		if data.get('properties').get('identifier'):
			try:
				crunch_image = data.get('properties').get('identifier').get('image_id').split('/')[1]
				main_image = data.get('properties').get('identifier').get('image_id')
				image_id = str(uuid.uuid4()) + '.jpg'
				download_image = urllib.request.urlretrieve('https://crunchbase-production-res.cloudinary.com/image/upload/c_lpad,h_120,w_120,f_jpg/'+main_image,image_id)
				our_logo = download_image[0]
				image_extension = logo.split('.')[1]
			except:
				crunch_image = ''
				main_image = ''
				image_id = ''
				download_image = ''
				our_logo = ''
				image_extension = ''
			bucket = conn.get_bucket('peop_logo')
			possible_key = bucket.get_key(p_key)
			if possible_key:
				pass
			else:
				k = Key(bucket)
				k.key = p_key
				k.set_contents_from_string(our_logo)
	# print(current_position)


	# PERSON IMAGE


	# SOCIAL ACCOUNTS
	if data.get('cards').get('overview_fields2'):
		if data.get('cards').get('overview_fields2').get('facebook'):
			facebook = data.get('cards').get('overview_fields2').get('facebook').get('value')
			# facebook_ascii = ''.join(str(ord(c.lower())) for c in facebook)
		else:
			facebook = ''
		# print(facebook)
		if data.get('cards').get('overview_fields2').get('linkedin'):
			linkedin = data.get('cards').get('overview_fields2').get('linkedin').get('value')
			# linkedin_ascii = ''.join(str(ord(c.lower())) for c in linkedin)
			# people_linkedin_key = str(uuid.uuid3(uuid.NAMESPACE_X500, str(linkedin_ascii)))
		else:
			linkedin = ''
		# print(linkedin)
		if data.get('cards').get('overview_fields2').get('twitter'):
			twitter = data.get('cards').get('overview_fields2').get('twitter').get('value')
			# twitter_ascii = ''.join(str(ord(c.lower())) for c in twitter)
			# people_twitter_key = str(uuid.uuid3(uuid.NAMESPACE_X500, str(twitter_ascii)))
		else:
			twitter = ''
		# print(twitter)
		if data.get('cards').get('overview_fields2').get('website'):
			website = data.get('cards').get('overview_fields2').get('website').get('value')
		else:
			website = ''
		# print(website)
	else:
		facebook = ''
		linkedin = ''
		twitter = ''
		website = ''

	# DEMOGRAPHICS
	if data.get('cards').get('overview_fields'):
		if data.get('cards').get('overview_fields').get('gender'):
			gender = data.get('cards').get('overview_fields').get('gender')
		else:
			gender = ''
	else:
		gender = ''
	# print(gender)

	# LOCATIONS
	if data.get('cards').get('overview_fields'):
		try:
			locations = [i.get('value') for i in data.get('cards').get('overview_fields').get('location_identifiers')]
		except:
			locations = ''
		try:
			state = locations[0].replace(' ','_').lower()
		except:
			state = ''
		try:
			city = locations[1].replace(' ','_').lower()
		except:
			city = ''
		try:
			country = locations[2].replace(' ','_').lower()
		except:
			country = ''
		try:
			continent = locations[3].replace(' ','_').lower()
		except:
			continent = ''
	else:
		state = ''
		city = ''
		country = ''
		continent = ''
		
		

		

	# overview_description
	if data.get('cards').get('overview_description'):
		overview_description = data.get('cards').get('overview_description').get('description')
	else:
		overview_description = ''
	# print(overview_description)

	if data.get('cards'):
		if data.get('cards').get('investor_overview_fields'):
			investor_stage = data.get('cards').get('investor_overview_fields').get('investor_stage')
			investor_type = data.get('cards').get('investor_overview_fields').get('investor_type')
		else:
			investor_type = ''
			investor_stage = ''
	# print(investor_stage)

		# overview_image_description
	if data.get('cards').get('overview_image_description'):
		# fullName = data.get('cards').get('overview_image_description').get('name')
		# print(fullName)
		# if data.get('cards').get('overview_image_description').get('identifier'):
		# 	photo = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_thumb,h_120,w_120,f_jpg,g_faces/' + str(data.get('cards').get('overview_image_description').get('identifier').get('image_id'))
		# else:
		# 	photo = ''
		# print(photo)
		if data.get('cards').get('overview_image_description').get('primary_job_title'):
			current_position = data.get('cards').get('overview_image_description').get('primary_job_title')
		else:
			current_position = ''
		# print(primary_job_title)
		if data.get('cards').get('overview_image_description').get('primary_organization'):
			current_company = data.get('cards').get('overview_image_description').get('primary_organization').get('value')
			current_company_permalink = data.get('cards').get('overview_image_description').get('primary_organization').get('permalink')
			# primary_organization_image = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_lpad,h_25,w_25,f_jpg/' + str(data.get('cards').get('overview_image_description').get('primary_organization').get('image_id'))
		else:
			current_company = ''
			current_company_permalink = ''
		# print(primary_organization)
		# print(primary_organization_image)
	current_company_list = list(db['company_data'].find(filter = {'permalink':current_company_permalink}))
	for cc in current_company_list:
		if (current_company_permalink == cc.get('permalink')):
			db['people_current_company'].insert_one(document = {'company_name':cc.get('company_name'),'company_permalink':cc.get('permalink'),
				'domain':cc.get('domain'),'company_key':cc.get('c_key'),'people_key':p_key,'people_name':people_name,'people_name_permalink':people_name_permalink,
				'company_logo':cc.get('our_logo'),'people_facebook':facebook,'company_facebook':cc.get('company_facebook_link'),'people_linkedin':linkedin,
				'company_linkedin':cc.get('company_linkedin_link'),'people_twitter':twitter,'company_twitter':cc.get('company_twitter_link')})


	# current_board_and_advisory_roles
	if data.get('cards').get('current_board_and_advisory_roles_image_list'):
		board_and_advisory_roles = data.get('cards').get('current_board_and_advisory_roles_image_list')
		# print(data.get('cards').get('current_board_and_advisory_roles_image_list'))
		for ba in board_and_advisory_roles:
			if ba.get('organization_identifier'):
				current_board_organization = ba.get('organization_identifier').get('value')
				current_board_organization_permalink = ba.get('organization_identifier').get('permalink')
			else:
				current_board_organization = ''
				current_board_organization_permalink = ''
			# print("org",organization)
			if ba.get('identifier'):
				current_board_title = ba.get('identifier').get('value')
			else:
				current_board_title = ''
			# print("tit",title)
			# if ba.get('organization_identifier'):
			# 	organization_image = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_lpad,h_100,w_100,f_jpg/' + str(ba.get('organization_identifier').get('image_id'))
			# else:
			# 	organization_image = ''
			
			if ba.get('started_on'):
				current_board_started_on = ba.get('started_on').get('value')
				if current_board_started_on:
					current_board_started_on_date = datetime.strptime(current_board_started_on, '%Y-%m-%d')
				else:
					current_board_started_on_date = ''

			else:
				current_board_started_on = ''
				current_board_started_on_date = ''
			# print("started_on", started_on)
			if ba.get('title'):
				current_board_primary_role = ba.get('title')
			else:
				current_board_primary_role = ''
			# print("primary",primary_role)
	# print(primary_role)

			c_dom = list(db['company_data'].find(filter = {'permalink':current_board_organization_permalink}))
			# c_dom_doc = [i for i in c_dom]
			for i in c_dom:
				if (current_board_organization_permalink == i.get('permalink')):
					# STORE SOCIAL LINKS OF COMPANY AND PEOPLE
					# STORE COMPANY DOMAIN, P_KEY, C_KEY, PERMALINK(PEOPLE,COMPANY)
					db['current_bm'].insert_one(document = {'company_name':i.get('company_name'),'company_permalink':i.get('permalink'),
					'domain':i.get('domain'),'company_key':i.get('c_key'),'people_key':p_key,'people_name':people_name,'people_name_permalink':people_name_permalink,'primary_role':current_board_primary_role,
					'started_on':current_board_started_on_date,'company_logo':i.get('our_logo'),'people_facebook':facebook,'company_facebook':i.get('company_facebook_link'),
					'people_linkedin':linkedin,'title':current_board_title,'company_linkedin':i.get('company_linkedin_link'),'people_twitter':twitter,'company_twitter':i.get('company_twitter_link')})

		# if domain not found in company_domains collection, then ... # PENDING

		# Same logic for current and past employees , current and past board_managers
		# Same logic for current and past current and past jobs list

	if data.get('cards').get('past_board_and_advisory_roles_list'):
		past_board_and_advisory_roles = data.get('cards').get('past_board_and_advisory_roles_list')
		for pba in past_board_and_advisory_roles:

			if pba.get('organization_identifier'):
				past_board_organization = pba.get('organization_identifier').get('value')
				past_board_organization_permalink = pba.get('organization_identifier').get('permalink')
			else:
				past_board_organization = ''
				past_board_organization_permalink = ''

			if pba.get('identifier'):
				past_board_title = pba.get('identifier').get('value')
			else:
				past_board_title = ''

			if pba.get('started_on'):
				past_board_started_on = pba.get('started_on').get('value')
				if past_board_started_on:
					past_board_started_on_date = datetime.strptime(past_board_started_on, '%Y-%m-%d')
				else:
					past_board_started_on_date = ''

			else:
				past_board_started_on = ''
				past_board_started_on_date = ''

			if pba.get('ended_on'):
				past_board_ended_on = pba.get('ended_on').get('value')
				if past_board_ended_on:
					past_board_ended_on_date = datetime.strptime(past_board_ended_on, '%Y-%m-%d')
				else:
					past_board_ended_on_date = ''

			else:
				past_board_ended_on = ''
				past_board_ended_on_date = ''

			if ba.get('title'):
				past_board_primary_role = ba.get('title')
			else:
				past_board_primary_role = ''

			pba_dom = list(db['company_data'].find(filter = {'permalink':past_board_organization_permalink}))
			for pb in pba_dom:
				if (past_board_organization_permalink == pb.get('permalink')):
					db['past_bm'].insert_one(document = {'company_name':pb.get('company_name'),'company_permalink':pb.get('permalink'),
					'domain':pb.get('domain'),'company_key':pb.get('c_key'),'people_key':p_key,'people_name':people_name,'people_name_permalink':people_name_permalink,'primary_role':past_board_primary_role,
					'started_on':past_board_started_on_date,'ended_on':past_board_ended_on_date,'company_logo':pb.get('our_logo'),'people_facebook':facebook,'company_facebook':pb.get('company_facebook_link'),
					'people_linkedin':linkedin,'title':past_board_title,'company_linkedin':pb.get('company_linkedin_link'),'people_twitter':twitter,'company_twitter':pb.get('company_twitter_link')})

	if data.get('cards').get('current_jobs_image_list'):
		current_jobs_image = data.get('cards').get('current_jobs_image_list')
		for cj in current_jobs_image:
			if cj.get('organization_identifier'):
				current_job = cj.get('organization_identifier').get('value')
				current_job_organization_permalink = cj.get('organization_identifier').get('permalink')
			else:
				current_job = ''
				current_job_organization_permalink = ''

			if cj.get('identifier'):
				current_job_title = cj.get('identifier').get('value')
			else:
				current_job_title = ''

			if cj.get('started_on'):
				current_job_started_on = cj.get('started_on').get('value')
				if current_job_started_on:
					current_job_started_on_date = datetime.strptime(current_job_started_on, '%Y-%m-%d')
				else:
					current_job_started_on_date = ''

			else:
				current_job_started_on = ''
				current_job_started_on_date = ''

			if cj.get('title'):
				current_job_primary_role = cj.get('title')
			else:
				current_job_primary_role = ''

			cj_dom = list(db['company_data'].find(filter = {'permalink':current_job_organization_permalink}))
			for cj in cj_dom:
				if(current_job_organization_permalink == cj.get('permalink')):
					db['current_job'].insert_one(document = {'company_name':cj.get('company_name'),'company_permalink':cj.get('permalink'),
					'domain':cj.get('domain'),'company_key':cj.get('c_key'),'people_key':p_key,'people_name':people_name,'people_name_permalink':people_name_permalink,'primary_role':current_job_primary_role,
					'started_on':current_job_started_on_date,'ended_on':current_job_ended_on_date,'company_logo':cj.get('our_logo'),'people_facebook':facebook,'company_facebook':cj.get('company_facebook_link'),
					'people_linkedin':linkedin,'title':current_job_title,'company_linkedin':cj.get('company_linkedin_link'),'people_twitter':twitter,'company_twitter':cj.get('company_twitter_link')})

	if data.get('cards').get('past_jobs_list'):
		past_jobs_image = data.get('cards').get('past_jobs_list')
		for pj in past_jobs_image:
			if pj.get('organization_identifier'):
				past_job = pj.get('organization_identifier').get('value')
				past_job_organization_permalink = pj.get('organization_identifier').get('permalink')
			else:
				past_job = ''
				past_job_organization_permalink = ''

			if pj.get('identifier'):
				past_job_title = pj.get('identifier').get('value')
			else:
				past_job_title = ''

			if pj.get('started_on'):
				past_job_started_on = pj.get('started_on').get('value')
				if past_job_started_on:
					past_job_started_on_date = datetime.strptime(past_job_started_on, '%Y-%m-%d')
				else:
					past_job_started_on_date = ''

			else:
				past_job_started_on = ''
				past_job_started_on_date = ''

			if pj.get('ended_on'):
				past_job_ended_on = pj.get('ended_on').get('value')
				if past_job_ended_on:
					past_job_ended_on_date = datetime.strptime(past_job_ended_on, '%Y-%m-%d')
				else:
					past_job_ended_on_date = ''

			else:
				past_job_ended_on = ''
				past_job_ended_on_date = ''

			if pj.get('title'):
				past_job_primary_role = pj.get('title')
			else:
				past_job_primary_role = ''

			pj_dom = list(db['company_data'].find(filter = {'permalink':past_job_organization_permalink}))
			for pj in pj_dom:
				if(current_job_organization_permalink == pj.get('permalink')):
					db['current_job'].insert_one(document = {'company_name':pj.get('company_name'),'company_permalink':pj.get('permalink'),
					'domain':pj.get('domain'),'company_key':pj.get('c_key'),'people_key':p_key,'people_name':people_name,'people_name_permalink':people_name_permalink,'primary_role':past_job_primary_role,
					'started_on':past_job_started_on_date,'ended_on':past_job_ended_on_date,'company_logo':pj.get('our_logo'),'people_facebook':facebook,'company_facebook':pj.get('company_facebook_link'),
					'people_linkedin':linkedin,'title':past_job_title,'company_linkedin':pj.get('company_linkedin_link'),'people_twitter':twitter,'company_twitter':pj.get('company_twitter_link')})




	


	db['people_data'].insert_one(document = {'people_key':p_key,'people_name':people_name,'people_image':photo,
		'people_name_permalink':people_name_permalink,'about_info':about_info,'current_company':current_company,'current_position':current_position,'current_company_permalink':current_company_permalink,'extra_roles':extra_roles,
		'people_facebook_link':facebook,'people_linkedin_link':linkedin,'people_twitter_link':twitter,'website':website,'gender':gender,'state':state,'city':city,'country':country,'continent':continent,
		'overview_description':overview_description,'investor_type':investor_type,'investor_stage':investor_stage})


	# GET FROM COMPANY_DATA...FOLLOW THE ABOVE LOGIC
	# education_image_list
	if data.get('cards').get('education_image_list'):
		education = data.get('cards').get('education_image_list')
		for edu in education:
			if edu.get('school_identifier'):
				education_institute = edu.get('school_identifier').get('value')
				education_permalink = edu.get('school_identifier').get('permalink')
				# main_logo = edu.get('school_identifier').get('image_id')
				# education_institute_image = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_lpad,h_100,w_100,f_jpg/' + str(edu.get('school_identifier').get('image_id'))

			else:
				education_institute = ''
				education_permalink = ''
				# education_institute_image = ''
			# print(education_institute)
			# print(education_institute_image)
			if edu.get('subject'):
				subject = edu.get('subject')
			else:
				subject = ''
			# print(subject)
			if edu.get('identifier'):
				title = edu.get('identifier').get('value')
			else:
				title = ''
			# print(title)
			if edu.get('completed_on'):
				completed_on = edu.get('completed_on').get('value')
				if completed_on:
					completed_on_date = datetime.strptime(completed_on, '%Y-%m-%d')
				else:
					completed_on_date = ''
				# completed_on_date = [d.to_datetime() for d in pd.to_datetime(completed_on)]
			else:
				completed_on = ''
				completed_on_date = ''
			if edu.get('type_name'):
				degree = edu.get('type_name')
			else:
				degree = ''
			# print(degree)
			# CHECK WHETHER THE ORG IS IN COMPANY_DATA OR NOT
			c_edu = list(db['company_data'].find(filter = {'permalink':education_permalink}))
			for i in c_edu:
				if(education_permalink == i.get('permalink')):
					# STORE SOCIAL LINKS OF COMPANY AND PEOPLE
					# STORE COMPANY DOMAIN, P_KEY, C_KEY, PERMALINK(PEOPLE,COMPANY)
					document = {'people_key':p_key,'people_name':people_name,'company_key':i.get('c_key'),'education_institute':i.get('company_name'),
					'education_institute_image':i.get('our_logo'),'company_domain':i.get('domain'),'subject':subject,'title':title,'completed_on':completed_on_date,'degree':degree,
					'people_permalink':people_name_permalink,'company_permalink':i.get('permalink'),'people_facebook':facebook,'company_facebook':i.get('company_facebook_link'),
					'people_linkedin':linkedin,'company_linkedin':i.get('company_linkedin_link'),'people_twitter':twitter,'company_twitter':i.get('company_twitter_link')}
			# print(document)

			db['people_education'].insert_one(document = document)

		# store the above if-block in person_education collection with key as the people_key




