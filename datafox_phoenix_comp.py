import tldextract
import uuid
import pandas as pd
import pymongo
import time
from pymongo.errors import DuplicateKeyError
import urllib.request
from datetime import datetime
from dateutil.parser import parse
import json
import ast
import yaml
import phoenixdb
import datetime
from database import *
import seolib as seo

print("Start : %s" % time.ctime())
# time.sleep(5)

dbStuff = PhoenixWrite()

with open('bluestem_datafox.json', encoding='utf-8') as data_file:
    data = json.load(data_file)
    perma_list = []
    if data.get('website'):
        website = data.get('website')
        ext = tldextract.extract(website)
        domain = '.'.join(ext[1:3])

    if data.get('keywords'):
        keywords = data.get('keywords')
    else:
        keywords = ''

    if data.get('twitter'):
        twitter = data.get('twitter')
        twitter_username = twitter.rsplit('/', 1)[-1]
    else:
        twitter = ''
        twitter_username = ''
    if data.get('linkedin'):
        linkedin = data.get('linkedin')
        linkedin_username = linkedin.rsplit('/', 1)[-1]
    else:
        linkedin = ''
        linkedin_username = ''
    if data.get('crunchbase'):
        crunchbase = data.get('crunchbase')
        crunchbase_username = crunchbase.rsplit('/', 1)[-1]
    else:
        crunchbase = ''
        crunchbase_username = ''

    if crunchbase_username:
        print('crunchbase is there and found permalink', crunchbase_username)
        company_permalink = crunchbase_username
        query = "select * from company_keywords where company_permalink = '{val}'".format(val=company_permalink)
        result = dbStuff.runQuery(query=query, select=True)
        if result:
            pass
        else:
            if keywords:
                for k in keywords:
                    keywords_key_uuid = str(uuid.uuid4())
                    keywords_dict = dict(keywords_key=keywords_key_uuid, company_permalink=crunchbase_username,
                                         keywords=k)
                    dbStuff.upsert_one(keywords_dict, tableName='company_keywords')
            else:
                pass
    else:

        dom_query = "select reference_key from base_set where base_value = '{val}'".format(val=domain)
        print('else here')
        dom_result = dbStuff.runQuery(query=dom_query, select=True)
        if dom_result:
            domain_permalink = dom_result.get('REFERENCE_KEY')
            perma_list.append(domain_permalink)
            company_permalink = domain_permalink
            query = "select * from company_keywords where company_permalink = '{val}'".format(val=company_permalink)
            result = dbStuff.runQuery(query=query, select=True)
            if result:
                pass
            else:
                if keywords:
                    for k in keywords:
                        keywords_key_uuid = str(uuid.uuid4())
                        keywords_dict = dict(keywords_key=keywords_key_uuid, company_permalink=crunchbase_username,
                                             keywords=k)
                        dbStuff.upsert_one(keywords_dict, tableName='company_keywords')
                else:
                    pass
        else:
            pass


        def getPermalink(socialAccount):
            if 'https' in socialAccount:
                https_query = "select reference_key from base_set where base_value like '%{val}%'".format(
                    val=socialAccount)
                print(https_query)
                https_result = dbStuff.singleQuery(query=https_query)
                if https_result:
                    https_permalink = https_result.get('REFERENCE_KEY')
                    return https_permalink
                else:
                    http = socialAccount.replace('https', 'http')
                    http_query = "select reference_key from base_set where base_value like '%{val}%'".format(val=http)
                    print(http_query)
                    http_result = dbStuff.singleQuery(query=http_query)
                    if http_result:
                        http_permalink = http_result.get('REFERENCE_KEY')
                        return http_permalink

            else:
                http = socialAccount.replace('https', 'http')
                http_query = "select reference_key from base_set where base_value like '%{val}%'".format(val=http)
                print(http_query)
                http_result = dbStuff.singleQuery(query=http_query)
                if http_result:
                    http_permalink = http_result.get('REFERENCE_KEY')
                    return http_permalink


        twitter_permalink = ''
        linkedin_permalink = ''
        if len(dom_result) > 0:
            print('domain is there')
        else:
            if linkedin:
                linkedin_permalink = getPermalink(linkedin)
                print('linkedin_permalink', linkedin_permalink)
            if linkedin_permalink:
                perma_list.append(linkedin_permalink)
                print('linkedin is there')
            else:
                if twitter:
                    twitter_permalink = getPermalink(twitter)
                    perma_list.append(twitter_permalink)

        if len(perma_list) > 0:
            permalink = perma_list[0]
            company_permalink = permalink
            query = "select * from company_keywords where company_permalink = '{val}'".format(val=company_permalink)
            result = dbStuff.runQuery(query=query, select=True)
            if result:
                pass
            else:
                if keywords:
                    for k in keywords:
                        keywords_key_uuid = str(uuid.uuid4())
                        keywords_dict = dict(keywords_key=keywords_key_uuid, company_permalink=company_permalink,
                                             keywords=k)
                        dbStuff.upsert_one(keywords_dict, tableName='company_keywords')
        else:
            pass



