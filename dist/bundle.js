/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ 3:
/***/ (function(module, exports) {



// chrome.browserAction.onClicked.addListener(count_requests)
// THIS METHOD IS TO CHECK THE REQUESTS

// visited_domains = []
function count_requests(callback){
  var queryInfo = {
    active : true,
    currentWindow: true
  };
  chrome.tabs.query(queryInfo, function(tabs){
        
        var tab = tabs[0];
        var geturl = tab.url;
        var r = /:\/\/(.[^/]+)/;
        var res = geturl.match(r)[1];
        var url = res.replace("www.", "");
        currentURL = url
        Totalrequests = 100;
       
    
        
        // var set = localStorage.setItem('url', JSON.stringify(url));
        // var get = JSON.parse(localStorage.getItem('url'))
        if( currentURL === JSON.parse(localStorage.getItem('url'))){
          console.log("if")
            console.log(JSON.parse(localStorage.getItem('url')))
          
            callback(Totalrequests);
        }
        else{
            console.log(JSON.stringify(url));
            localStorage.setItem('url', JSON.stringify(url));
            console.log("else")
            console.log(localStorage.setItem('url', JSON.stringify(url)))
       
            Totalrequests = Totalrequests - 1;
            callback(Totalrequests);
        }
  });
}





/**
 * Get the current URL.
 *
 * @param {function(string)} callback - called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var requests = 100;
    var tab = tabs[0];

    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    var geturl = tab.url;

    // window.alert(geturl)
    // console.log(geturl)
    // tab.url is only available if the "activeTab" permission is declared.
    // If you want to see the URL of other tabs (e.g. after removing active:true
    // from |queryInfo|), then the "tabs" permission is required to see their
    // "url" properties.
    console.assert(typeof geturl == 'string', 'tab.url should be a string');
    var r = /:\/\/(.[^/]+)/;

    var res = geturl.match(r)[1];
    var url = res.replace("www.", "");
    // currentURL = url


    var criteria = 'search';
    var check = geturl.includes(criteria);
    // console.log(check)
    if(url === 'linkedin.com' && check === true){
        // window.alert(geturl)
        var raw_html = '';
        function makeHttpObject() {
            try {return new XMLHttpRequest();}
            catch (error) {}
            try {return new ActiveXObject("Msxml2.XMLHTTP");}
            catch (error) {}
            try {return new ActiveXObject("Microsoft.XMLHTTP");}
            catch (error) {}

            throw new Error("Could not create HTTP request object.");
          }
          var request = makeHttpObject();
          request.open("GET", geturl, true);
          request.send(null);
          request.onreadystatechange = function() {
            if (request.readyState == 4)
              raw_html = request.responseText;
            console.log(raw_html)
              
          };

    }
    else{
      callback(url);
    }

    // callback(url);
  });


}

/**
 * @param {string} searchTerm - Search term for Google Image search.
 * @param {function(string,number,number)} callback - Called when an image has
 *   been found. The callback gets the URL, width and height of the image.
 * @param {function(string)} errorCallback - Called when the image is not found.
 *   The callback gets a string that describes the failure reason.
 */
function getImageUrl(searchTerm, callback, errorCallback) {
  // Google image search - 100 searches per day.
  // https://developers.google.com/image-search/
  var searchUrl = 'https://ajax.googleapis.com/ajax/services/search/images' +
    '?v=1.0&q=' + encodeURIComponent(searchTerm);
  var x = new XMLHttpRequest();
  x.open('GET', searchUrl);
  // The Google image search API responds with JSON, so let Chrome parse it.
  x.responseType = 'json';
  x.onload = function() {
    // Parse and process the response from Google Image Search.
    var response = x.response;
    if (!response || !response.responseData || !response.responseData.results ||
        response.responseData.results.length === 0) {
      errorCallback('No response from Google Image search!');
      return;
    }
    var firstResult = response.responseData.results[0];
    // Take the thumbnail instead of the full image to get an approximately
    // consistent image size.
    var imageUrl = firstResult.tbUrl;
    var width = parseInt(firstResult.tbWidth);
    var height = parseInt(firstResult.tbHeight);
    console.assert(
        typeof imageUrl == 'string' && !isNaN(width) && !isNaN(height),
        'Unexpected respose from the Google Image Search API!');
    callback(imageUrl, width, height);
  };
  x.onerror = function() {
    errorCallback('Network error.');
  };
  x.send();
}



function getEmails(searchTerm,callback,errorCallback){
  // window.alert("getEmails function")
  // var searchUrl = 'http://0.0.0.0:6552/domainSearchview?domain=' + searchTerm + '&startRange=0' 
  var searchUrl = 'http://0.0.0.0:6564/domainSearchviewforChrome?domain=gmail.com&startRange=0';
  var searchUrl = 'http://0.0.0.0:6564/domainSearchviewforChrome?domain='+ searchTerm + '&startRange=0';
  // var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
  fetch(searchUrl).then(j=>j.json()).then(d=>{
    // console.log(d)
    var finalresult = JSON.stringify(d);
    // console.log(finalresult)
    
    callback(JSON.stringify(d));
  
  })
  /*var xhr = new XMLHttpRequest();
  xhr.open('GET', searchUrl,true);
  xhr.responseType = 'json';
  // window.alert(xhr.responseText);
  xhr.onreadystatechange = function() {
        if (xhr.readyState>3 && xhr.status==200) success(xhr.responseText);
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    callback(xhr.responseText);
    xhr.send();
    return xhr;
  // x.onload = function(){
  //   var response = x.response;

  // }
  // var result = response.responseData.results[0];
  // callback(result);
  // x.send();*/
}





function renderStatusRequests(statusText){
  document.getElementById('requests').textContent = statusText;
}

function renderStatus(statusText) {
  document.getElementById('status').textContent = statusText;

  
}
function renderStatus1(statusText){
  document.getElementById('emails').textContent = statusText;
}
function renderStatus1(statusText){
  document.getElementById('pageDetails').textContent = statusText;
}

document.addEventListener('DOMContentLoaded', function() {

  $(function() {
  $('#s').click(function() {
     chrome.tabs.create({url: 'https://www.outbeast.com/'});
  });
  });
   $(function() {
  $('#U').click(function() {
     chrome.tabs.create({url: 'https://www.outbeast.com/'});
  });
  });

   count_requests(function(url){
      renderStatusRequests(Totalrequests);
    });

  getCurrentTabUrl(function(url) {

    renderStatus(url);


   
    getEmails(url,function(url){
      renderStatus1(url);
    
    // getEmails(url, function(imageUrl, width, height) {

    //   renderStatus('Search term: ' + url + '\n' +
    //       'Google image search result: ' + imageUrl);
    //   var imageResult = document.getElementById('image-result');
    //   // Explicitly set the width/height to minimize the number of reflows. For
    //   // a single image, this does not matter, but if you're going to embed
    //   // multiple external images in your page, then the absence of width/height
    //   // attributes causes the popup to resize multiple times.
    //   imageResult.width = width;
    //   imageResult.height = height;
    //   imageResult.src = imageUrl;
    //   imageResult.hidden = false;

    }, function(errorMessage) {
      renderStatus('Cannot display image. ' + errorMessage);

    });
    
  });
  });




/***/ })

/******/ });