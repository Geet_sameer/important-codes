from pyramid.view import (notfound_view_config,
                          forbidden_view_config,
                          view_config, view_defaults)

import phoenixdb
import uuid
import seolib as seo
from bs4 import BeautifulSoup
import urllib

import json
from multiprocessing import Pool
from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPNotFound, HTTPFound
import datetime

# @view_defaults(renderer='templates/newView.pt')
class OutbeastPhoenix():
    def __init__(self, request):
        database_url = 'http://devmike.trioncube.com:8765'
        self.request = request
        self.conn = phoenixdb.connect(database_url, autocommit=True)
        self.cursor = self.conn.cursor(cursor_factory=phoenixdb.cursor.DictCursor)

    def datetime_handler(self,obj):
        return obj.isoformat() if hasattr(obj, 'isoformat') else obj

    def getPeopleKeyFromEmail(self, emailid):
        query = "SELECT peopleKey FROM emailpeople WHERE email = '{emailid}' LIMIT 10".format(
            emailid=emailid)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

    # TRIONDATA
    def getPermalink(self,value):
        query = "SELECT reference_key as permalink from base_set WHERE base_value = '{value}'".format(value=value)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

    def getPermalinkforSocial(self,value):
        query = "SELECT data_type from base_set WHERE base_value like '%{value}%'".format(value=value)
        print(query)
        self.cursor.execute(query)
        query_result = self.cursor.fetchone()
        data_type = query_result.get('DATA_TYPE')
        if data_type == 'company':
            company_query = "SELECT reference_key as permalink,data_type from base_set where data_type = '{data_type}' and base_value like '%{value}%'".format(data_type=data_type,value=value)
            self.cursor.execute(company_query)
            result = self.cursor.fetchone()
        else:
            people_query = "SELECT reference_key as permalink,data_type from base_set where data_type = '{data_type}' and base_value like '%{value}%'".format(data_type=data_type,value=value)
            self.cursor.execute(people_query)
            result = self.cursor.fetchone()

        return result

    # TRIONDATA
    def getPermalinkfromPeople(self,value):
        query = "SELECT people_permalink as permalink from people_data where people_permalink = '{value}'".format(value=value)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

    # TRIONDATA
    def getCompany_data(self,perma):
        query = "SELECT * FROM company_data where company_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

    def getPeople_data(self,perma):
        query = "SELECT * FROM people_data where people_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)

        result = self.cursor.fetchone()
        return result

    # TRIONDATA
    def getCategoriesfromCompany(self,perma):
        query = "SELECT categories from categories where company_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    # TRIONDATA
    def getFoundersfromCompany(self,perma):
        query = "SELECT people_name from founders where company_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    # TRIONDATA
    def getCompanyDemographics(self,perma):
        query = "SELECT state,city,country,continent from demographics where company_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

    def getPeopleDemographics(self,perma):
        query = "SELECT state,city,country,continent from demographics where people_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result


    # TRIONDATA
    def getCurrentEmployees(self,perma):
        query = "SELECT people_name,primary_role,start_date,title from current_employees where company_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    # TRIONDATA

    def getPastEmployees(self, perma):
        query = "SELECT people_name,primary_role,start_date,end_date,title from past_employees where company_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    def getCompany_Sub_Organizations(self,perma):
        query = "SELECT company_name from company_sub_organizations WHERE company_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    def get_Company_Investor_Type(self,perma):
        query = "SELECT investor_type from company_investor_type WHERE company_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    def get_Company_Investor_Stage(self,perma):
        query = "SELECT investor_stage from company_investor_stage WHERE company_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    def get_Company_Other_Roles(self,perma):
        query = "SELECT other_roles from company_other_roles WHERE company_permalink = '{value}'".format(
            value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    def getRole(self,perma):
        query = "SELECT current_company_name,CURRENT_POSITON from role where people_permalink = '{value}'".format(value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    def getPeopleInvestorType(self,perma):
        query = "SELECT investor_type from people_investor_type where people_permalink = '{value}'".format(
            value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    def getPeopleInvestorStage(self,perma):
        query = "SELECT investor_stage from people_investor_stage where people_permalink = '{value}'".format(
            value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    def getPeopleOtherRole(self,perma):
        query = "SELECT other_roles from people_other_roles where people_permalink = '{value}'".format(
            value=perma)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    # def create_token(self):
    #     return "{0}-{1}".format(str(uuid.uuid4()), str(uuid.uuid4()))
    #
    # def nameBlock(self, peopleKey,offset, limit=10):
    #
    #     query = "SELECT pos.orgName as orgName, pos.title as Title FROM PEOPLE_DATA peop join POSITION_DATA pos on peop.peopleKey = pos.peopleKey WHERE peop.peopleKey = '{peopleKey}' LIMIT 10 OFFSET {off}".format(
    #         peopleKey=peopleKey,off=offset,lim=limit)
    #     self.cursor.execute(query)
    #     result = self.cursor.fetchall()
    #     return result
    #
    # def photos(self,peopleKey,offset,limit = 10):
    #     query = "SELECT type,typeid,typename,picurl from PHOTOS where PEOPLEKEY like '%{peopleKey}' LIMIT 10 OFFSET {off}".format(peopleKey=peopleKey,off=offset,lim=limit)
    #     self.cursor.execute(query)
    #     result = self.cursor.fetchall()
    #     print(result)
    #     return result

    # def social_profiles(self, peopleKey,offset, limit=10):
    #
    #     query = "SELECT * FROM SOCIALANDOTHERPEOPLE WHERE PEOPLEKEY like'%{peopleKey}' LIMIT 10 OFFSET {off}".format(peopleKey=peopleKey,off=offset,lim=limit)
    #     self.cursor.execute(query)
    #     r = self.cursor.fetchone()
    #     print(r)
    #     if r:
    #         r.pop('PEOPLEKEY')
    #         r.pop('SOCIALID')
    #         result = []
    #         for key, val in r.items():
    #             if val is not None:
    #                 self.cursor.execute("SELECT url FROM {tab} WHERE SNSUUID='{val}'".format(tab=key.lower(), val=val))
    #                 a = self.cursor.fetchone()
    #                 result.append(dict(social_profile=key.lower(), url=a.get('URL')))
    #
    #         return result
    #
    # def sourcesFound(self, peopleKey,offset, limit=10):
    #
    #     query = "SELECT sourceName FROM SOURCESFOUND WHERE peopleKey like '%{peopleKey}' LIMIT 10 OFFSET {off}".format(peopleKey=peopleKey,off=offset,lim=limit)
    #     self.cursor.execute(query)
    #     result = self.cursor.fetchall()
    #     return result

    # def totalSourcesFound(self, peopleKey,limit=10):
    #
    #     query = "SELECT COUNT(*) as Sources FROM SOURCESFOUND WHERE peoplekey like '%{peopleKey}' LIMIT 10".format(peopleKey=peopleKey,lim=limit)
    #
    #     self.cursor.execute(query)
    #     result = self.cursor.fetchone()
    #     print("sources--->", result)
    #
    #     return result
    #
    # def workHistory(self, peopleKey,offset, limit=10):
    #
    #     query = "SELECT title,startDate,endDate,orgName FROM POSITION_DATA WHERE peopleKey = '{peopleKey}' LIMIT 10 OFFSET {off}".format(
    #         peopleKey=peopleKey,off=offset,lim=limit)
    #     self.cursor.execute(query)
    #     result = self.cursor.fetchall()
    #
    #     return result

    # def demographics(self,peopleKey, offset, limit = 10):
    #     query = "SELECT location,city,city_code,state,state_code,country,country_code,continent,county,age,gender,agerange from DEMOGRAPHICS where peopleKey like '%{peopleKey}' LIMIT 10 OFFSET {off}".format(
    #         peopleKey=peopleKey,off=offset,lim=limit)
    #     self.cursor.execute(query)
    #     result = self.cursor.fetchall()
    #
    #     return result
    #
    # def domainSearch(self, domainName, offset, limit=10):
    #
    #     query = "SELECT e.email as email FROM PEOPLE_DATA p INNER JOIN EMAILPEOPLE e on p.peoplekey=e.peoplekey WHERE e.domain='{domainName}' LIMIT {lim} OFFSET {off}" \
    #         .format(domainName=domainName, off=offset, lim=limit)
    #
    #     self.cursor.execute(query)
    #     result = self.cursor.fetchall()
    #     return result

    # def domainSearchChrome(self,domainName, offset, limit = 10):
    #     query = "SELECT peodt.fullname as fullname, pos.title as title, ep.email as email from PEOPLE_DATA peodt INNER JOIN POSITION_DATA pos on peodt.peoplekey = pos.peoplekey INNER JOIN EMAILPEOPLE ep on ep.peoplekey = pos.peoplekey WHERE ep.domain = '{domainName}' LIMIT {lim} OFFSET {off}".format(domainName=domainName,off=offset,lim=limit)
    #     self.cursor.execute(query)
    #     result = self.cursor.fetchone()
    #     return result

    # def domainSearchCount(self,domainName):
    #     query = "SELECT COUNT(*) as TotalEmails from EMAILPEOPLE where domain = '{domainName}'".format(domainName=domainName)
    #     self.cursor.execute(query)
    #     result = self.cursor.fetchall()
    #     return result
    #
    # def getFullName(self,peopleKey):
    #
    #     query = "SELECT fullName,firstName,lastName from PEOPLE_DATA WHERE peopleKey = '{peopleKey}'".format(peopleKey=peopleKey)
    #     self.cursor.execute(query)
    #     result = self.cursor.fetchall()
    #     return result



    # @view_config(route_name='domainSearchview', renderer='json')      # MODIFIED AND ADD
    # @view_config(route_name='getMoreDomain', renderer='json')
    # def domainSearchview(self):
    #     request = self.request
    #
    #     domain = request.params.get('domain')
    #
    #     key = self.create_token()
    #
    #     startRange = request.params.get('startRange')
    #
    #     contactInfo = self.domainSearch(domainName=domain, offset=startRange)
    #
    #     # peopleKey = self.getPeopleKeyFromDomainName(domainName=domain).get('peopleKey')
    #     # print(peopleKey)
    #
    #     # TotalEmails = self.domainSearchCount(domain)
    #
    #     for contact in contactInfo:
    #         email = contact.get('EMAIL')
    #         peopleKey = self.getPeopleKeyFromEmail(email).get('PEOPLEKEY')
    #         contact['sourceCount'] = self.totalSourcesFound(peopleKey).get('SOURCES')
    #         social_profiles = self.social_profiles(peopleKey, offset=startRange)
    #         contact['social_profiles'] = social_profiles
    #
    #
    #
    #     return dict(
    #         status=True,
    #         result=contactInfo,
    #         count = self.domainSearchCount(domain)
    #
    #     )

    # @view_config(route_name='getMoreDomain', renderer='json')
    # def getMoreDomain(self):
    #     result = self.domainSearchview()
    #     return result

    def checkemptyDictionary(self,data,list_of_parameters):
        for params in list_of_parameters:
            for i in data:
                if i.get(params) is None:
                    del i[params]
        data = data
        all_empty = True
        for i in data:
            if i:
                all_empty = False
        return all_empty,data

    def checkdict(self,mydict):    # METHOD FOR CHROME EXTENSION

        b = []
        email = set()
        email_ind = dict()
        ind = 0
        for i in mydict:
            em = i.get('EMAIL')
            if em not in email:
                email.add(em)
                i['TITLE'] = [i.get('TITLE')] if i.get('TITLE') is not None else list()
                b.append(i)
                email_ind[em] = ind
            else:
                newInd = email_ind.get(em)
                d = b[newInd]
                d['TITLE'].append(i['TITLE']) if i['TITLE'] is not None else d['TITLE']
                b[newInd] = d
            ind = ind + 1

        return b

    @view_config(route_name='domainSearchviewforChrome', renderer='json')
    def domainSearchviewforChrome(self):
        request = self.request
        result = {}
        countdict = {}
        domain = request.params.get('domain')
        startRange = request.params.get('startRange')
        Info = self.domainSearchChrome(domainName=domain, offset=startRange)
        # print(Info)
        # parameters = ['FULLNAME', 'TITLE','EMAIL']
        # dominfo = self.checkemptyDictionary(Info, parameters)
        # if dominfo[0] is False:
        #     result['details']=dominfo[1]
        # else:
        #     pass
        countdict['Total_Emails'] = self.domainSearchCount(domainName=domain).get('TOTALEMAILS')


        for sources in Info:
            email = sources.get('EMAIL')
            peopleKey = self.getPeopleKeyFromEmail(email).get('PEOPLEKEY')
            sources['sourceCount'] = self.totalSourcesFound(peopleKey)
            # social_profiles = self.social_profiles(peopleKey, offset=startRange)
            # Info['social_profiles'] = social_profiles

        finaldata = self.checkdict(Info)
        # print(finaldata)



        return finaldata,countdict
    '''

    def verify_email(self,email):
        from validate_email import validate_email
        exists = validate_email(email, verify=True)
        return dict(_id=email, exists=exists)

    def toSMTPPool(self,gen):
        p = Pool(processes=len(gen))
        email_exists = p.map(self.verify_email, gen)
        p.close()
        p.join()
        return email_exists

    def create_email(self,pattern, domain):
        email = pattern.replace(' ', '') + '@' + domain
        return email.encode('utf-8')

    def verify(self,first_name,last_name,domain):
        request = self.request
        first_name = first_name
        last_name = last_name
        domain = domain
        if not first_name:
            first_name = ' '
        if not last_name:
            last_name = ' '
        if not domain:
            return 'Domain cannot be empty'

        emails = [last_name + first_name[0], last_name, first_name + last_name[0], last_name + '-' + first_name,
                  first_name + '.' + last_name, first_name + '-' + last_name, first_name + '.' + last_name[0],
                  last_name + '.' + first_name[0], last_name + first_name, first_name[0] + last_name,
                  first_name + '-' + last_name[0], first_name + '_' + last_name[0], first_name + '_' + last_name,
                  last_name + '_' + first_name, first_name[0] + '.' + last_name, first_name + last_name,
                  last_name[0] + first_name, last_name + '_' + first_name[0], last_name + '.' + first_name, first_name,
                  first_name[0] + '_' + last_name]
        generated_email = list(map(lambda x: self.create_email(x, domain), emails))


        # print(len(email_exists_in_db))
        generated_email_not_in_db = []
        email_exists = []
        # email_exists_in_db_copy = list(email_exists_in_db)
        for _, i_doc in enumerate(generated_email):
            if not i_doc.get('exists'):
                generated_email_not_in_db.append(i_doc.get('_id'))
            else:
                email_exists.append(i_doc.get('_id'))
                # print(i_doc)
                # del email_exists_in_db_copy[index]

        if generated_email_not_in_db:
            email_exists_SMTP = self.toSMTPPool(generated_email_not_in_db)

        return dict(response=list(set(email_exists)))
        
    '''

    # @view_config(route_name = 'crunchbase',renderer = 'json')
    # def crunchbase(self):
    #     request = self.request
    #     html_lines = request.params.get('raw_html')
    #     # print("html lines", html_lines)
    #     from bs4 import BeautifulSoup
    #     import re
    #     soup = BeautifulSoup(html_lines, 'html.parser')
    #     data = str(soup)
    #     # print("data is",data)
    #     # criteria = re.findall(r'<!DOCTYPE html> <html> <head> <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">',data,re.DOTALL)
    #     # print("criteria",criteria)
    #
    #     permalinks_company = re.findall(r'{"properties":{"identifier":{"entity_def_id":"organization","permalink":(.*?),"image_id":', data,re.DOTALL)
    #
    #
    #     extract_links = []
    #     for i in permalinks_company:
    #         url = 'https://crunchbase.com/v4/data/entities/organizations/' + i.replace('\"','') + '?field_ids=[%22identifier%22,%22layout_id%22,%22facet_ids%22,%22title%22,%22short_description%22]&layout_mode=view'
    #
    #         extract_links.append(url)
    #     if len(extract_links) <= 0:
    #         permalinks_people = re.findall(r'{"properties":{"identifier":{"entity_def_id":"person","permalink":(.*?),"image_id":', data, re.DOTALL)
    #         for i in permalinks_people:
    #             url = 'https://crunchbase.com/v4/data/entities/people/' + i.replace('\"','') + '?field_ids=[%22identifier%22,%22layout_id%22,%22facet_ids%22,%22title%22,%22short_description%22]&layout_mode=view'
    #             extract_links.append(url)
    #
    #     # save the data into database along with raw html
    #     print("extract_links",extract_links)
    #     x = dict(success = 'success')
    #     return x

    @view_config(route_name = 'peopleSearchviewforChrome',renderer = 'json')
    def peopleSearchviewforChrome(self):
        request = self.request
        person_name = request.params.get('name')
        split_names = person_name.split(" ")

        first_name = split_names[0]
        last_name = split_names[-1]
        if(first_name == last_name):
            first_name = last_name
            last_name = ''
        else:
            pass
        position = request.params.get('position')
        company_name = request.params.get('company')
        cname = company_name.lower().strip()
        print("company name:",cname)
        search = 'https://autocomplete.clearbit.com/v1/companies/suggest?query=:' + ''.join(cname.split(' '))
        print("search is", search)
        response = urllib.request.urlopen(search)
        print("response is", response)
        jsonData = json.loads(response.read())
        data = [item.get('domain') for item in jsonData]
        print(data)
        print("length is",len(data))
        if len(data) > 0:
            domain = data[0]
        else:
            domain = ''


        get_Email_from_domain = 'http://88.198.107.39:6543/findemail?first_name='+ first_name + '&last_name=' + last_name + '+&domain=' + domain
        print(get_Email_from_domain)
        response1 = urllib.request.urlopen(get_Email_from_domain)
        jsonData1 = json.loads(response1.read())
        info = dict(name=person_name,position=position,Email=jsonData1)
        return info

    @view_config(route_name='peopleEmailSearch',renderer='json')                # MODIFIED AND ADD
    def peopleEmailSearch(self):
        request = self.request
        data = {}

        email = request.POST.get('email')
        startRange = request.POST.get('startRange')

        gen_basekey = ''.join(str(ord(c.lower())) for c in email)
        peopleKey = str(uuid.uuid3(uuid.NAMESPACE_X500, str(gen_basekey)))


        fullName = self.getFullName(peopleKey)
        data['fullName'] = fullName

        positions = self.nameBlock(peopleKey,offset=startRange)
        parameters = ['ORGNAME','TITLE']
        info = self.checkemptyDictionary(positions,parameters)
        if info[0] is False:
            data['positions']=info[1]
        else:
            pass

        photos = self.photos(peopleKey,offset = startRange)
        data['photos'] = photos

        demographics = self.demographics(peopleKey,offset = startRange)
        data['demographics'] = demographics

        social_profiles = self.social_profiles(peopleKey,offset=startRange)
        data['social_profiles'] = social_profiles

        sourcesFound = self.sourcesFound(peopleKey,offset=startRange)
        data['sourcesfound'] = sourcesFound

        workHistory = self.workHistory(peopleKey,offset=startRange)
        parameters = ['TITLE','STARTDATE','ENDDATE','ORGNAME']
        workinfo = self.checkemptyDictionary(workHistory, parameters)
        if workinfo[0] is False:
            data['workHistory'] = workinfo[1]
        else:
            pass

        sourcesCount = self.totalSourcesFound(peopleKey)
        data['sourcesCount'] = sourcesCount

        return data

    @view_config(route_name='companyInfo', renderer='json')
    def companyInfo(self):
        request = self.request
        company_website = request.POST.get('website')
        #company_website = tldx.extract(str(company_website))
        #domain = company_website.domain+'.'+company_website.suffix
        domain = ''.join(str(ord(c.lower())) for c in company_website)
        companyKey = str(uuid.uuid3(uuid.NAMESPACE_X500,domain))
        # _key = str(uuid.uuid3(uuid.NAMESPACE_X500,companyKey))
        # print(companyKey)
        query_to_company_data = "SELECT companyName, companyType FROM company_data where companyKey='{key}'".format(key=domain)
        query_to_email = "SELECT email FROM emailCompany where companyKey='{key}'".format(key=companyKey)
        query_to_phone = "SELECT phone FROM phoneCompany where companyKey='{key}'".format(key=companyKey)
        query_to_domain = "SELECT domain FROM domain where companyKey='{key}'".format(key=companyKey)
        query_to_address = "SELECT street, city, state, country FROM addressCompany where companyKey='{key}'".format(key=companyKey)
        alexa_ranking = str(seo.get_alexa('http://www.'+company_website))
        database_url = 'http://pho.iscreed.com:8765'
        data_to_return = {}
        ph_connection = phoenixdb.connect(database_url, autocommit=True)
        cursor = ph_connection.cursor(cursor_factory=phoenixdb.cursor.DictCursor)
        cursor.execute(query_to_company_data)
        cdata = cursor.fetchone()
        cursor.execute(query_to_address)
        address_data = cursor.fetchone()
        # print(address_data)
        cursor.execute(query_to_domain)
        domain_data = cursor.fetchone()
        cursor.execute(query_to_email)
        email_data = cursor.fetchone()
        cursor.execute(query_to_phone)
        phone_data = cursor.fetchone()
        if cdata:
            data_to_return.update(cdata)
        if domain_data:
            data_to_return.update(domain_data)
        if address_data:
            data_to_return.update(address_data)
        if email_data:
            data_to_return.update(email_data)
        if phone_data:
            data_to_return.update(phone_data)
        data_to_return['alexaRanking'] = alexa_ranking

        cursor.execute("SELECT * FROM SOCIALANDOTHERCOMPANY WHERE companyKey='{key}'".format(key=companyKey))
        r = cursor.fetchone ()
        d = {}
        if r:
            r.pop ('COMPANYKEY')
            r.pop ('SOCIALID')
            for key, val in r.items ():
                if val is not None:

                    cursor.execute ("SELECT url FROM {tab} WHERE SNSUUID='{val}'".format (tab=key.lower (), val=val))
                    a = cursor.fetchone ()
                    print(a)
                    d[key] = a.get('URL')
        data_to_return.update(dict(social_profiles=d))
        query = "SELECT p.fullName, e.email FROM PEOPLE_DATA p INNER JOIN EMAILPEOPLE e on p.peoplekey=e.peoplekey " \
                "WHERE e.domain='{domainName}'".format(domainName=company_website)
        cursor.execute(query)
        result = cursor.fetchall()
        data_to_return.update(dict(contacts=result))
        data_to_return = {k:v for k,v in data_to_return.items() if v is not None and len(v) != 0}
        return data_to_return

    def companySocial(self,permalink):                         # TRIONDATA
        request = self.request
        data = {}

        # Get permalink from base_set using domain
        # permalink_from_db = self.getPermalink(permalink)
        # permalink = permalink_from_db.get('PERMALINK')
        # Get company data from company_data using permalink
        company_data = self.getCompany_data(permalink)
        converting_datetostr = json.dumps(company_data, default=self.datetime_handler)
        data['company_data'] = json.loads(converting_datetostr)
        # Get categories
        data['categories'] = self.getCategoriesfromCompany(permalink)
        # Get founders
        data['Founders'] = self.getFoundersfromCompany(permalink)
        # Get demographics
        data['demographics'] = self.getCompanyDemographics(permalink)
        # Get Current_employees
        current_team_db = self.getCurrentEmployees(permalink)
        current_team = json.dumps(current_team_db, default=self.datetime_handler)
        data['current_team'] = json.loads(current_team)
        # Get Past_employees
        past_team_db = self.getPastEmployees(permalink)
        past_team = json.dumps(past_team_db, default=self.datetime_handler)
        data['past_team'] = json.loads(past_team)
        return data

    @view_config(route_name='domainCrunch',renderer = 'json')
    def domainCrunch(self):                         # TRIONDATA
        request = self.request
        data = {}
        domain = request.POST.get('domain')
        # Get permalink from base_set using domain
        permalink_from_db = self.getPermalink(domain)
        permalink = permalink_from_db.get('PERMALINK')
        # Get company data from company_data using permalink
        company_data = self.getCompany_data(permalink)
        converting_datetostr = json.dumps(company_data, default=self.datetime_handler)
        data['company_data'] = json.loads(converting_datetostr)
        # Get categories
        data['categories'] = self.getCategoriesfromCompany(permalink)
        # Get founders
        data['Founders'] = self.getFoundersfromCompany(permalink)
        # Get demographics
        data['demographics'] = self.getCompanyDemographics(permalink)
        # Get company_sub_organizations
        data['sub_organizations'] = self.getCompany_Sub_Organizations(permalink)
        # Get company_investor_type
        data['investor_type'] = self.get_Company_Investor_Type(permalink)
        # Get company_investor_stage
        data['investor_stage'] = self.get_Company_Investor_Stage(permalink)
        # Get company_other_roles
        data['other_activities'] = self.get_Company_Other_Roles(permalink)
        # Get Current_employees
        current_team_db = self.getCurrentEmployees(permalink)
        current_team = json.dumps(current_team_db, default=self.datetime_handler)
        data['current_team'] = json.loads(current_team)
        # Get Past_employees
        past_team_db = self.getPastEmployees(permalink)
        past_team = json.dumps(past_team_db, default=self.datetime_handler)
        data['past_team'] = json.loads(past_team)
        return data

    @view_config(route_name='peopleNameCrunch', renderer='json')
    def peopleNameCrunch(self):
        request = self.request
        data = {}
        people_name = request.POST.get('people_permalink')
        # Get permalink from people_data using people_name
        permalink_from_db = self.getPermalinkfromPeople(people_name)
        permalink = permalink_from_db.get('PERMALINK')
        # Get people data from people_data using permalink
        people_data = self.getPeople_data(permalink)
        data['people_data'] = people_data
        # Get demographics
        data['demographics'] = self.getPeopleDemographics(permalink)
        # Get role
        data['current_job'] = self.getRole(permalink)
        # Get investor_type
        data['investor_type'] = self.getPeopleInvestorType(permalink)
        # Get investor_stage
        data['investor_stage'] = self.getPeopleInvestorStage(permalink)
        # Get other roles
        data['other_roles'] = self.getPeopleOtherRole(permalink)
        return data
    def peopleSocial(self,permalink):
        data ={}
        people_data = self.getPeople_data(permalink)
        data['people_data'] = people_data
        # Get demographics
        data['demographics'] = self.getPeopleDemographics(permalink)
        # Get role
        data['current_job'] = self.getRole(permalink)
        # Get investor_type
        data['investor_type'] = self.getPeopleInvestorType(permalink)
        # Get investor_stage
        data['investor_stage'] = self.getPeopleInvestorStage(permalink)
        # Get other roles
        data['other_roles'] = self.getPeopleOtherRole(permalink)
        return data


    @view_config(route_name='socailCrunch',renderer = 'json')
    def socailCrunch(self):
        request = self.request
        data = {}
        social = request.POST.get('social')
        # Get permalink from people_data using people_name
        permalink_from_db = self.getPermalinkforSocial(social)
        permalink = permalink_from_db.get('PERMALINK')
        data_type = permalink_from_db.get('DATA_TYPE')
        if data_type == 'company':
            result = self.companySocial(permalink)
        else:
            result = self.peopleSocial(permalink)
        return result




