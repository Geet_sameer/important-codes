var StellarSdk = require('stellar-sdk');
StellarSdk.Network.useTestNetwork();
var server = new StellarSdk.Server('https://horizon-testnet.stellar.org');
// Transaction will hold a built transaction we can resubmit if the result is unknown.
var transaction;

fifth_public_addr = 'GCOVPMBVL276VMZHMGYUXQFDNYCXTGHKSERMXCEFYFCNLKA5Z2T3QQCR';
fifth_private_key = 'SBUVP7UQD5U3UEQPLXXKUHC6SC32BQ3WP4Q37GBBZKB2VJJU74HLPL7L';

sixth_public_addr = 'GCIWEKEKY7O4IYDUWWIIX2Z5IC7U64X7FZGH4FWPLBLU5WJJIJ5HVFNS';
sixth_private_key = 'SBS4AUQAY3N5JYITIE4SKMPLMLLCPRWHHEEBPDV7PRS5FJJVNGFTBFR4';
var sourceKeys = StellarSdk.Keypair
  .fromSecret(fifth_private_key);

var second_signer = StellarSdk.Keypair
  .fromSecret(sixth_private_key);
var destinationId = 'GA2C5RFPE6GCKMY3US5PAB6UZLKIGSPIUKSLRB6Q723BM2OARMDUYEJ5';

// First, check to make sure that the destination account exists.
// You could skip this, but if the account does not exist, you will be charged
// the transaction fee when the transaction fails.
server.loadAccount(destinationId)
  // If the account is not found, surface a nicer error message for logging.
  .catch(StellarSdk.NotFoundError, function (error) {
    throw new Error('The destination account does not exist!');
  })
  // If there was no error, load up-to-date information on your account.
  .then(function() {
    return server.loadAccount(sourceKeys.publicKey());
  })
  .then(function(sourceAccount) {
    // Start building the transaction.
    transaction = new StellarSdk.TransactionBuilder(sourceAccount)
      .addOperation(StellarSdk.Operation.payment({
        destination: destinationId,
        // Because Stellar allows transaction in many currencies, you must
        // specify the asset type. The special "native" asset represents Lumens.
        asset: StellarSdk.Asset.native(),
        amount: "1"
      }))
      // A memo allows you to add your own metadata to a transaction. It's
      // optional and does not affect how Stellar treats the transaction.
      .addMemo(StellarSdk.Memo.text('Test Transaction'))
      .build();
    // Sign the transaction to prove you are actually the person sending it.
    transaction.sign(sourceKeys);
    transaction.sign(second_signer)
    console.log(transaction)
    // And finally, send it off to Stellar!
    return server.submitTransaction(transaction);
  })
  .then(function(result) {
    console.log('Success! Results:', result);
  })
  .catch(function(error) {
    console.error('Something went wrong!', error);
    // If the result is unknown (no response body, timeout etc.) we simply resubmit
    // already built transaction:
    // server.submitTransaction(transaction);
  });