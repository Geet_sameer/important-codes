var IOTA = require('iota.lib.js')
//transfer function working in nodes
//http://node06.iotatoken.nl:14265


var iota = new IOTA({
    'host': 'http://node.deviceproof.org',
    'port': 14265
});

iota.api.getNodeInfo(function(error, success) {
    if (error) {
        console.error(error);
    } else {
        console.log(success);
    }
});

console.log(iota.version);

var seed = 'TC9MGJASLUUTVHKOLDVTFY9WLGJIOFCH9JNYDJZCOWVELSTHB9THXIGCBNRSD9FMWZCTHEVELHASIFDJQ';

// iota.api.getNewAddress(seed, {index: 0 , total: 1, security: 3, checksum: true}, function(e, address) {
//     console.log(address)
//     // Save generated address in database
// });
var sendAddress = 'ZGZFZIAOMUPXDYNXJEAGMUSZDMBJGAPKMFTVQGAZDRMT9NMTUFPHTLVGPUBXCIRD9BRXOBJUUVRMXNBTWNKTFDTRQX';
var messageTag = 'HELLOWORLD';
var messageToSend = 'For testing purpose only';
var messageTrytes = iota.utils.toTrytes(messageToSend);
var transfer = [{
    'address': sendAddress,
    'value': 0,
    'message': messageTrytes,
    'tag':messageTag
}]
// use for specific transactions and also remainders
var options = [
        {
            address: 'XB9IBINADVMP9K9FEIIR9AYEOFUU9DP9EBCKOTPSDVSNRRNVSJOPTFUHSKSLPDJLEHUBOVEIOJFPDCZS9',     // for sending
            balance: 1500,
            keyIndex: 0,
            security: 3
        }, {
            address: 'W9AZFNWZZZNTAQIOOGYZHKYJHSVMALVTWJSSZDDRVEIXXWPNWEALONZLPQPTCDZRZLHNIHSUKZRSZAZ9W',    // change address
            balance: 8500,
            keyIndex: 7,
            security: 2
        }
    ]

iota.api.sendTransfer(seed, 4, 14, transfer, function(e, bundle) {
    if (e) throw e;
    console.log("Successfully sent your transfer: ", bundle);
})