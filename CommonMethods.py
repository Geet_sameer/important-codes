import re
from itertools import islice, groupby, count
import itertools
import linecache
from boilerpipe.extract import Extractor
import re
import usaddress
import unicodedata
from bs4 import BeautifulSoup
from bs4.element import Comment
import urllib

# from geotext import GeoText
from multiprocessing import Pool

class HtmlDataExtraction:
    Data_List = {}

    def __init__(self):
        super(HtmlDataExtraction, self).__init__()
        # self.filename = filename
        return

    def ExtractionProcess(self,filename):
        f = open(filename, "r", encoding='utf-8')
        html_lines = f.read()
        print(type(html_lines))
        # print(lines)
        # Data_List = []

        # html_text = self.html_to_text(html_lines)
        html_text = self.text_from_html(html_lines)
        # print((html_text))
        # print(type(usaddress.parse(html_text)))

        phone_regex = self.Phone_Number_Extraction(html_text)
        # print(phone_regex)
        # self.Data_List.append(phone_regex)
        self.Data_List['phone_regex'] = phone_regex

        # Getting Email from the Email_Extraction Method
        Email_data = self.Email_Extraction(html_lines)
        # print(Other_data)
        # self.Data_List.append(Email_data)
        self.Data_List['Email_data'] = Email_data

        # Social Data from the html page
        SocialData = self.Social_Data_Extraction(html_lines)
        # print(SocialData)
        # self.Data_List.append(SocialData)
        self.Data_List['SocialData'] = SocialData

        # Get title tag
        Title = self.Extract_Title_Tag(html_lines)
        # print(Title)
        # self.Data_List.append(Title)
        self.Data_List['Title'] = Title

        # Get Address
        # Address = self.Extract_Address(html_text)
        # Address = Address.replace("\n",",").replace("'","")
        # self.Data_List['Address'] = Address
        # Data_List.append(Address)
        print(self.Data_List)


        f.close()

        # Writing to a text file
        # text_file = open("rec_1.txt","a",encoding = 'utf-8')
        # text_file.write(str(Data_List) + '\n')
        # text_file.close()






        return self.Data_List

    def Extract_Address(self, htmlContent):
        address = GeoText(htmlContent)
        country = address.countries
        print(country)
        cities = address.cities
        print(cities)
        more_address = usaddress.parse(htmlContent)
        Address_line = ''
        # print(more_address)
        for i in more_address:

            x, y = i
            if y == 'AddressNumber':                    #Sometimes gives unrelated data
                Address_line = Address_line + x
            # print(Address_line)
            if y == 'StreetNamePreDirectional':
                Address_line = Address_line + x
                # print(Address_line)
            if y == 'StreetName':                       #Sometimes gives unrelated data
                Address_line = Address_line + x
                # print(Address_line)
            if y == 'StreetNamePostType':                #Sometimes gives unrelated data
                Address_line = Address_line + x
            # print(Address_line)


            if y == 'PlaceName':
                Address_line = Address_line + x
                # print(Address_line)
            if y == 'StateName':
                Address_line = Address_line + x + ","
                # print(Address_line)
            if y == 'ZipCode':
                Address_line = Address_line + x
                # print(Address_line)
        Address_line = Address_line + (str(country).replace('[',"").replace("]","") )#+  str(cities)
        # print(Address_line)
        # Address_line = Address_line + str(cities)
        Address_line = Address_line.replace("[", " ").replace("Finished"," ").replace("]", " ").replace(" ","")
        return Address_line




    def Extract_Title_Tag(self, htmlContent):
        data_lines = ''
        data_lines = ''.join(e for e in htmlContent)
        Title = re.findall(r'<title>(.*?)</title>', data_lines)
        # Title = str(Title).decode('unicode_escape').encode('ascii','ignore')
        # Title_tag = []
        # Title_tag.append({'Title':Title})
        # print(Title)
        return Title

    def Phone_Number_Extraction(self, htmlContent):
        phone_regex = re.findall(r'([\w\-\.]+@(\w[\w\-]+\.[\w\-]+))+', htmlContent)
        # phone_regex = re.findall(r'([\+\(]?\d+?[\s\-\.]?\(?\d+\)?[\s\-\.]\d{3}[\s\-\.]?\d{4})', htmlContent)
        # phone_regex1 = re.findall(r'^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$',htmlContent)
        # PhoneNumber = []
        # PhoneNumber.append({'Phone': phone_regex})
        # self.Data_List.append({'Phone': phone_regex})
        return phone_regex

    def Email_Extraction(self, htmlContent):
        data_lines = ''
        data_lines = ''.join(e for e in htmlContent)
        email = re.findall(r'([\w\-\.]+@(\w[\w\-]+\.[\w\-]+))+',data_lines)
        # email = re.findall(r'\w+@\w+\.(?:com|in)', data_lines)
        # print("email is ", email)
        # EmailId = []
        # EmailId.append({'Email' : email})
        # return self.Data_List['Email']
        return email

    def Social_Data_Extraction(self, data_list):
        data_lines = ''
        data_lines = ''.join(e for e in data_list)
        # print(data_lines)
        Extracted_Data = {}
        

       

        twitter_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?twitter.(?:com|in)\/([A-z 0-9 \. ^=:]+))+',data_lines)
        # print(twitter_regex)
        # Extracted_Data.append({'Twitter_account' : twitter_regex})
        Extracted_Data['Twitter_link'] = twitter_regex

        # Linkedin
        linkedin_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?linkedin.(?:com|in)\/([A-z 0-9 \. ^=:]+))+',data_lines)
        # print(linkedin_regex)
        # Extracted_Data.append({'Linkedin_account' : linkedin_regex})
        Extracted_Data['Linkedid_link'] = linkedin_regex

        # FourSquare
        foursquare_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?foursquare.(?:com|in)\/([A-z 0-9 \. ^=:]+))+',data_lines)
        # print(foursquare_regex)
        # Extracted_Data.append({'Foursquare_account' : foursquare_regex})
        Extracted_Data['Foursquare_link'] = foursquare_regex

        # Facebook
        Facebook_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?facebook.(?:com|in)\/([A-z 0-9 \. ^=:]+))+',data_lines)
        # print(Facebook_regex)
        # Extracted_Data.append({'FaceBook_account' : Facebook_regex})
        Extracted_Data['Facebook_link'] = Facebook_regex

        # Github
        github_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?github.(?:com|in)\/([A-z 0-9 \. ^=:]+))+',data_lines)
        # print(github_regex)
        # Extracted_Data.append({'GitHub_account' : github_regex})
        Extracted_Data['Github_link'] = github_regex

        # Instagram
        Instagram_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?instagram.(?:com|in)\/([A-z 0-9 \. ^=:]+))+',data_lines)
        # print(Instagram_regex)
        # Extracted_Data.append({'Instagram_account' : Instagram_regex})
        Extracted_Data['Instagram_link'] = Instagram_regex

        # google_plus
        google_plus_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?plus.google.(?:com|in)\/([A-z 0-9 \. ^=:]+))+',data_lines)
        # print(google_plus_regex)
        # Extracted_Data.append({'GooglePlus_account' : google_plus_regex})
        Extracted_Data['GooglePlus_link'] = google_plus_regex
        # print("Extracted data" , Extracted_Data)
        return Extracted_Data

    def html_to_text(self, html_lines):
        extractor1 = Extractor(extractor='KeepEverythingExtractor', html=html_lines)
        content = extractor1.getText()
        # print(content)
        return content

    def tag_visible(self,element):
        if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]','body','html','div']:
            return False
        if isinstance(element, Comment):
            return False
        return True


    def text_from_html(self,body):
        soup = BeautifulSoup(body, 'html.parser')
        texts = soup.findAll(text=True)
        visible_texts = filter(self.tag_visible, texts)  
        data = u" ".join(t.strip() for t in visible_texts)
        print((data))
        return data


if __name__ == "__main__":
    obj = HtmlDataExtraction()
    l = obj.ExtractionProcess('embassyit.html')
    # f = open("ex.html", "r", encoding='utf-8')
    # html_lines = f.read()
    # filename = "ex.html"
    # pool = Pool(processes = 3)
    # pool.map(l,filename)
