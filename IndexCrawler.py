
from warcreader import WarcFile
from gzip import GzipFile
import re
from boilerpipe.extract import Extractor
import usaddress
# from geotext import GeoText
import unicodedata
# import nltk
# from nltk.corpus import stopwords
# from nltk import word_tokenize, pos_tag, ne_chunk
import time
import numpy as np
import multiprocessing
from multiprocessing import Pool
from multiprocessing import Process
import glob, os


def Extraction(filename):
	warc_gzip = GzipFile(filename, 'rb')
	warc_file = WarcFile(warc_gzip)
	count = 0
	# Data = {}
	Email_collection = {}
	Phone_collection = {}
	Social_collection = {}
	facebook = {}
	twitter = {}
	linkedin = {}
	foursquare = {}
	github = {}
	Instagram = {}
	Total_Data = {}
	# process_id = os.getpid()
	# print("Number of Processes running\n")
	# print("process id", os.getpid())
	# file = str(process_id) + '.txt'
	# f = open(file, 'a')
	for webpage in warc_file:
	    body = webpage.payload          #ENTIRE HTML DATA


	    # print(type(body))
	    # #-----------------------_Email and Domain -------------------------------#
	    # email = re.findall(r'([\w\-\.]+@(\w[\w\-]+\.[\w\-]+))+',body)
	    # Email_collection['email_Dom'] = email 
	   
	    #-----------------------    END   ----------------------------------------#
	    
	    #------------------------ URI -------------------------------------------#
	    # print("uri",webpage.uri)
	    # uri = str(webpage.uri).decode('unicode_escape').encode('ascii','ignore')
	    # # # print(uri)
	    # Email_collection['SourceUrl'] = uri
	    # Phone_collection['SourceUrl'] = webpage.uri
	    # Social_collection['SourceUrl'] = webpage.uri
	    #--------------------------- END -----------------------------------#

	    #----------------------------PHONE_NUMBER----------------------------------------#


	    #Matches the following
	 	# 123-456-7890
		# (123) 456-7890
		# 123 456 7890
		# 123.456.7890
		# +91 (123) 456-7890
		# 555.123.4565
		# +1-(800)-545-2468
		# 2-(800)-545-2468
		# 3-800-545-2468
		# 555-123-3456
		# 555 222 3342
		# (234) 234 2442
		# (243)-234-2342
		# 1234567890
		# 123.456.7890
		# 123.4567
		# 123-4567
		# 1234567900
		# 12345678900
		# print(phone)
	    # phone_regex = re.findall(r'\+[-\(\s\d]+?(?=\s*[+<])', body)
	    # phone_regex = re.findall(r'([\+\(]?\d+[\s\-\.]\(?\d{3}\)?[\s\-\.]\d{3}[\s\-\.]?\d{4})', body)
	     
	    
	    # Phone_collection['phone'] = phone_regex
	    # print(phone_regex)
	    # print(len(phone_regex))
	    
	    # Phoone_collection['Phone'] = phone_regex

	    # print("phone ", phone_regex)
	    #---------------------------  TITLE --------------------------------------#
	    # Title = re.findall(r'<title>(.*?)</title>',body)
	    # Title = str(Title).decode('unicode_escape').encode('ascii','ignore')
	    # Email_collection['Title'] = Title
	    # Phone_collection['Title'] = Title
	    # Social_collection['Title'] = Title
	    # # print("Title is",Title)
	    #---------------------------- END ----------------------------------------#
			
		#----------------------------- Socail_Data --------------------------------------------------------#	 
		
		
	    # Facebook_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?facebook.(?:com|in)\/([^v2.3,sharer.php,\d+,pages,plugins,share,tr,badges,feeds][A-z 0-9 \. ^=:]+))+',body)
	    #Total_Data.append(Facebook_regex)
	    # facebook['facebook_link'] = Facebook_regex
	    # twitter_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?twitter.(?:com|in)\/([^share,intent,javascripts][^widgets.js][A-z 0-9 \/ \. ^=:]+))+', body)
	    # Total_Data.append(twitter_regex)
	    # twitter['twitter_link'] = twitter_regex
	    # linkedin_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?linkedin.(?:com|in)\/([^shareArticle][A-z 0-9 \/ \. ^=:]+))+',body)
	    # Total_Data.append(linkedin_regex)
	    # linkedin['linkedin_link'] = linkedin_regex
	    # foursquare_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?foursquare.(?:com|in)\/([A-z 0-9 \/ \. ^=:]+))+',body)
	    # Total_Data.append(foursquare_regex)
	    # foursquare['foursquare_link'] = foursquare_regex
	    # github_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?github.(?:com|in)\/([A-z 0-9 \/ \. ^=:]+))+',body)
	    # Total_Data.append(github_regex)
	    # github['github_link'] = github_regex
	    # Instagram_regex = re.findall(r'((?:(?:http|https):\/\/)?(?:www.)?instagram.(?:com|in)\/([A-z 0-9 \/ \. ^=:]+))+',body)
	    # Total_Data.append(Instagram_regex)
	    # Instagram['Insta_link'] = Instagram_regex

	    # Social_collection['Facebook'] = Facebook_regex
	    # Social_collection['Twitter'] = twitter_regex
	    # Social_collection['linkedin'] = linkedin_regex
	    # Social_collection['foursquare'] = foursquare
	    # Social_collection['Github'] = github_regex
	    # Social_collection['Instagram'] = Instagram_regex
	    # print("Fb", len(Facebook_regex))
	    # print("Fb", Facebook_regex)
	    #---------------------------- END ----------------------------------------#

	    #Using this try-catch for extraction of phone numbers

	    try:
	    	data = Text_from_HTML(body)
	    	# print(data)
	    	phone_regex = re.findall(r'([\+\(]?\d+?[\s\-\.]?\(?\d+\)?[\s\-\.]\d{3}[\s\-\.]?\d{4})', data)
	    	# print(phone_regex)
	    	Phone_collection['Phone_Number'] = phone_regex
	    	print(Phone_collection)
	    	# Get_Data = Extract_Address(data)
	    	# pure_data = str(Get_Data).decode('unicode_escape').encode('ascii','ignore')
	    	# print("Address is ", pure_data)
	    	# print(type(pure_data))
	    except Exception:
	    	# print("came into exception")
	    	continue

	    count = count + 1
	    # Email_collection['count'] = count
	    print("count",count)

	    # Total_Data['Email'] = Email_collection
	    # Total_Data['Social'] = Social_collection
	    # Total_Data['Phone'] = Phone_collection
	    
	    # print(Email_collection)
	    # f.write(str(Data) + '\n')
	    # print(Social_collection)
	    # print(Total_Data)

# def NLP_preprocesses(document):
# 	stop = stopwords.words('english')
# 	document = ' '.join([i for i in document.split() if i not in stop])
# 	sentences = nltk.sent_tokenize(document)
# 	sentences = [nltk.word_tokenize(sent) for sent in sentences]
# 	sentences = [nltk.pos_tag(sent) for sent in sentences]
# 	return sentences

# def extract_names(document):
# 	names = []
# 	sentences = NLP_preprocesses(document)
# 	for tagged_sentence in sentences:
# 	    for chunk in nltk.ne_chunk(tagged_sentence):
# 	        if type(chunk) == nltk.tree.Tree:
# 	            if chunk.label() == 'PERSON':
# 	                names.append(' '.join([c[0] for c in chunk]))
# 	print("Names", names)
# 	return names

# def NER(document):
 
# 	# sentences = "Mark and John are working at Google."
 
# 	data = ne_chunk(pos_tag(word_tokenize(document)))
# 	return data

def Text_from_HTML(htmlContent):
	extractor1 = Extractor(extractor='KeepEverythingExtractor', html=htmlContent)
	content = extractor1.getText()
	# print(content)
	return content

def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]','body','html','div']:
        return False
    if isinstance(element, Comment):
        return False
    return True


def text_from_html(body):
    soup = BeautifulSoup(body, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(tag_visible, texts)  
    data = u" ".join(t.strip() for t in visible_texts)
    print((data))
    return data
# def Extract_Address(htmlContent):
# 	address = GeoText(htmlContent)
# 	country = address.countries
# 	# print(country)
# 	cities = address.cities
# 	# print(cities)
# 	more_address = usaddress.parse(htmlContent)
# 	Address_line = ''
# 	# print(more_address)
# 	for i in more_address:

# 	    x, y = i
# 	    # if y == 'AddressNumber':                    #Sometimes gives unrelated data
# 	    #     Address_line = Address_line + x
# 	    # print(Address_line)
# 	    # if y == 'StreetNamePreDirectional':
# 	    #     Address_line = Address_line + x
# 	    #     # print(Address_line)
# 	    # if y == 'StreetName':                       #Sometimes gives unrelated data
# 	    #     Address_line = Address_line + x
# 	        # print(Address_line)
# 	    # if y == 'StreetNamePostType':                #Sometimes gives unrelated data
# 	    #     Address_line = Address_line + x
# 	    # print(Address_line)


# 	    if y == 'PlaceName':
# 	        Address_line = Address_line + x
# 	        # print(Address_line)
# 	    if y == 'StateName':
# 	        Address_line = Address_line + x + ","
# 	        # print(Address_line)
# 	    if y == 'ZipCode':
# 	        Address_line = Address_line + x
# 	        # print(Address_line)
# 	Address_line = Address_line + (str(country).replace('[',"").replace("]","") ) +  str(cities)
# 	# print(Address_line)
# 	# Address_line = Address_line + str(cities)
# 	Address_line = Address_line.replace("[", " ").replace("Finished"," ").replace("]", " ").replace(" ","")
# 	return Address_line

if __name__ == "__main__":
	t = time.time()
	Extraction('index.warc.gz')
	# first = 'index.warc.gz'
	# os.chdir("/opt/datac/cw")
	
	# lis = []
	# for file in glob.glob("*.gz"):
	# 	lis.append(file)
	# first = lis[0]
	# second = lis[1]
	# third = lis[2]
	# fourth = lis[3]
	# # # print(first)
	# # print(second)
	# p1 = Process(target = Extraction, args=(first,))
	# p2 = Process(target = Extraction, args=(second,))
	# p3 = Process(target = Extraction, args=(third,))
	# p4 = Process(target = Extraction, args=(fourth,))

	# p1.start()
	# p2.start()
	# p3.start()
	# p4.start()

	# p1.join()
	# p2.join()
	# p3.join()
	# p4.join()

	

	print("Time taken for all the processes: ", time.time() - t)
	
	#extract_names(string)
